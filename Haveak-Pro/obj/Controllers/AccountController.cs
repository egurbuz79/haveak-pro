﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Haveak_Pro.Models;


namespace Haveak_Pro.Controllers
{
    [System.Web.Mvc.OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class AccountController : Controller
    {
        //
        // GET: /Account/
        HaveakEntities db = new HaveakEntities();
        public ActionResult Login()
        {
            return View();
        }

        public JsonResult SifreKontrol(string userName, string password)
        {
            bool durum = false;
            DateTime zaman = DateTime.Now.Date;
            Kullanici accountControl = db.Kullanicis.Where(t => t.IsActive == true && t.KullaniciAdi == userName && t.Sifre == password && t.BitisTarihi >= zaman).FirstOrDefault();
            if (accountControl != null)
            {
                durum = true;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Login(string userName, string password)
        {
            try
            {
                if (userName != null)
                {
                    Kullanici accountControl = db.Kullanicis.Where(t => t.IsActive == true && t.KullaniciAdi == userName && t.Sifre == password).FirstOrDefault();
                    if (accountControl != null)
                    {
                        HttpContext.Session.Add("sifre", password);
                        FormsAuthentication.RedirectFromLoginPage(accountControl.KullaniciAdi, true);
                    }
                }
            }
            catch
            {

            }
            return View();
        }

        public ActionResult Logoff()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Login", "Account");
        }

        public JsonResult SendMail(string adress)
        {
            bool durum = false;
            try
            {
                List<string> adres = new List<string>();
                adres.Add(adress);
                var data = db.Personels.Where(t => t.Email == adress).FirstOrDefault();
                string kisi = data.Adi + " " + data.Soyadi;

                string body = "Sayın " + kisi + ";  <br />";
                body += "Proje Kullanıcı adı: [" + data.Kullanicis.Select(t => t.KullaniciAdi).FirstOrDefault() + "] Şifre: [" + data.Kullanicis.Select(t => t.Sifre).FirstOrDefault() + "] dir. <br />";
                body += "Sorun halinde temes kurunuz.";

                MailHelper hlp = new MailHelper();
                hlp.SenMail(adres, "Şifre Resetleme", body);
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}