﻿using Haveak_Pro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Haveak_Pro.Controllers
{

    public class MesajIslemlerController : Controller
    {
        //
        // GET: /Panel/MesajIslemler/
        //public MesajIslemlerController()
        //{
        //    AktifKullanici.Aktif.SecilenModul = "PANEL";
        //}

        HaveakEntities Entity = new HaveakEntities();
       
       
        public ActionResult Mesajlar()
        {
            var kimlik = AktifKullanici.Aktif.User_Ref;
            DateTime bugun = DateTime.Now.Date;

            if (!UserRoleName.PANEL_AdminGiris.InRole())
            {
                ViewBag.MesajKisiler = Entity.Kull_Rolleri.Where(t => t.Kull_Rol_Adi.Name == UserRoleName.PANEL_AdminGiris && t.Kullanici.Personel_Ref != kimlik).Select(e => e.Kullanici).ToList();
            }
            else
            {
                ViewBag.MesajKisiler = Entity.Mesajlars.Where(t => t.IsActive == true && t.IsDelete == false && t.Alici_Ref == kimlik).Select(w => w.Personel.Kullanicis.FirstOrDefault()).Distinct().ToList();
            }
            return View();
        }

        public ActionResult _MesajIcerikGn(int aliciRef)
        {
            int gonderen = AktifKullanici.Aktif.User_Ref;          
            var data = Entity.Mesajlars.OrderByDescending(y => y.ID).Where(t => t.IsActive == true && t.IsDelete == false && (t.Gonderen_Ref == gonderen || t.Alici_Ref == gonderen)).ToList();
            ViewBag.AliciRef = aliciRef;
            return PartialView("_MesajDetayGenel", data);
        }

        public ActionResult PostaGelen()
        { 
            return View(); 
        }
        public ActionResult PostaGonder()
        {
            return View();
        }
       

        public ActionResult UpdateMesaj(int id, bool IsActive, bool IsDelete)
        {
            string result = "";
            try
            {
                var data = Entity.Mesajlars.Where(r => r.ID == id).FirstOrDefault();               
                data.IsActive = IsActive;
                data.IsDelete = IsDelete;
                Entity.SaveChanges();
                result = "[{\"durum\":true}]";
            }
            catch (Exception)
            {
                result = "[{\"durum\":false}]";
            }
            ContentResult cr = new ContentResult();
            cr.ContentEncoding = System.Text.Encoding.UTF8;
            cr.ContentType = "application/json";
            cr.Content = result;
            return cr;
        }
    }
}
