
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 12/14/2016 16:00:30
-- Generated from EDMX file: D:\HAVEAKPROJELER\Haveak-Pro\Haveak-Pro\Models\HaveakModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [HaveakTur];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_AltSiklar_UniteSorular]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AltSiklars] DROP CONSTRAINT [FK_AltSiklar_UniteSorular];
GO
IF OBJECT_ID(N'[dbo].[FK_AtananBolumler_Bolum]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AtananBolumlers] DROP CONSTRAINT [FK_AtananBolumler_Bolum];
GO
IF OBJECT_ID(N'[dbo].[FK_AtananBolumler_Sinif_Ogrenci]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AtananBolumlers] DROP CONSTRAINT [FK_AtananBolumler_Sinif_Ogrenci];
GO
IF OBJECT_ID(N'[dbo].[FK_OkunanUniteler_AtananBolumler]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OkunanUnitelers] DROP CONSTRAINT [FK_OkunanUniteler_AtananBolumler];
GO
IF OBJECT_ID(N'[dbo].[FK_KursModuller_Bolum]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[KursModullers] DROP CONSTRAINT [FK_KursModuller_Bolum];
GO
IF OBJECT_ID(N'[dbo].[FK_Egitim_Arsivi_Unite]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Egitim_Arsivi] DROP CONSTRAINT [FK_Egitim_Arsivi_Unite];
GO
IF OBJECT_ID(N'[dbo].[FK_Personel_Firmalar]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Personels] DROP CONSTRAINT [FK_Personel_Firmalar];
GO
IF OBJECT_ID(N'[dbo].[FK_Kull_Rol_Adi_Moduller]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Kull_Rol_Adi] DROP CONSTRAINT [FK_Kull_Rol_Adi_Moduller];
GO
IF OBJECT_ID(N'[dbo].[FK_Kull_Rolleri_Kull_Rol_Adi]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Kull_Rolleri] DROP CONSTRAINT [FK_Kull_Rolleri_Kull_Rol_Adi];
GO
IF OBJECT_ID(N'[dbo].[FK_Kull_Rolleri_Kullanici]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Kull_Rolleri] DROP CONSTRAINT [FK_Kull_Rolleri_Kullanici];
GO
IF OBJECT_ID(N'[dbo].[FK_Kullanici_Personel1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Kullanicis] DROP CONSTRAINT [FK_Kullanici_Personel1];
GO
IF OBJECT_ID(N'[dbo].[FK_Loglama_Kullanici]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Loglamas] DROP CONSTRAINT [FK_Loglama_Kullanici];
GO
IF OBJECT_ID(N'[dbo].[FK_KursModuller_UniteModul]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[KursModullers] DROP CONSTRAINT [FK_KursModuller_UniteModul];
GO
IF OBJECT_ID(N'[dbo].[FK_Maddeler_BaslikMadde]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Maddelers] DROP CONSTRAINT [FK_Maddeler_BaslikMadde];
GO
IF OBJECT_ID(N'[dbo].[FK_Mesajlar_Personel]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Mesajlars] DROP CONSTRAINT [FK_Mesajlar_Personel];
GO
IF OBJECT_ID(N'[dbo].[FK_OkunanUniteler_Unite]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OkunanUnitelers] DROP CONSTRAINT [FK_OkunanUniteler_Unite];
GO
IF OBJECT_ID(N'[dbo].[FK_Personel_TIP_Personel]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Personel_TIP] DROP CONSTRAINT [FK_Personel_TIP_Personel];
GO
IF OBJECT_ID(N'[dbo].[FK_Sinif_Ogrenci_Personel]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Sinif_Ogrenci] DROP CONSTRAINT [FK_Sinif_Ogrenci_Personel];
GO
IF OBJECT_ID(N'[dbo].[FK_Personel_Sinavlar_Sinavlar]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Personel_Sinavlar] DROP CONSTRAINT [FK_Personel_Sinavlar_Sinavlar];
GO
IF OBJECT_ID(N'[dbo].[FK_Personel_Sinavlar_Sinif_Ogrenci]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Personel_Sinavlar] DROP CONSTRAINT [FK_Personel_Sinavlar_Sinif_Ogrenci];
GO
IF OBJECT_ID(N'[dbo].[FK_SınavCevaplar_Personel_Sinavlar]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SinavCevaplars] DROP CONSTRAINT [FK_SınavCevaplar_Personel_Sinavlar];
GO
IF OBJECT_ID(N'[dbo].[FK_Personel_Testler_Sinif_Ogrenci]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Personel_Testler] DROP CONSTRAINT [FK_Personel_Testler_Sinif_Ogrenci];
GO
IF OBJECT_ID(N'[dbo].[FK_Personel_Testler_Testler]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Personel_Testler] DROP CONSTRAINT [FK_Personel_Testler_Testler];
GO
IF OBJECT_ID(N'[dbo].[FK_TestCevaplar_Personel_Testler]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TestCevaplars] DROP CONSTRAINT [FK_TestCevaplar_Personel_Testler];
GO
IF OBJECT_ID(N'[dbo].[FK_Personel_TIP_TipSinavlar]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Personel_TIP] DROP CONSTRAINT [FK_Personel_TIP_TipSinavlar];
GO
IF OBJECT_ID(N'[dbo].[FK_TipCevaplar_Personel_TIP]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TipCevaplars] DROP CONSTRAINT [FK_TipCevaplar_Personel_TIP];
GO
IF OBJECT_ID(N'[dbo].[FK_SBagajlar_SBagajKategori]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SBagajlars] DROP CONSTRAINT [FK_SBagajlar_SBagajKategori];
GO
IF OBJECT_ID(N'[dbo].[FK_UniteSorular_SBagajlar]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UniteSorulars] DROP CONSTRAINT [FK_UniteSorular_SBagajlar];
GO
IF OBJECT_ID(N'[dbo].[FK_SBMaddeResimler_SBTehlikeliMKategori]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SBMaddeResimlers] DROP CONSTRAINT [FK_SBMaddeResimler_SBTehlikeliMKategori];
GO
IF OBJECT_ID(N'[dbo].[FK_UniteSorular_SBMaddeResimler]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UniteSorulars] DROP CONSTRAINT [FK_UniteSorular_SBMaddeResimler];
GO
IF OBJECT_ID(N'[dbo].[FK_SınavCevaplar_Sinav_Sorular]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SinavCevaplars] DROP CONSTRAINT [FK_SınavCevaplar_Sinav_Sorular];
GO
IF OBJECT_ID(N'[dbo].[FK_Sinav_Sorular_SınavTipi]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Sinav_Sorular] DROP CONSTRAINT [FK_Sinav_Sorular_SınavTipi];
GO
IF OBJECT_ID(N'[dbo].[FK_Sinav_Sorular_Sinavlar]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Sinav_Sorular] DROP CONSTRAINT [FK_Sinav_Sorular_Sinavlar];
GO
IF OBJECT_ID(N'[dbo].[FK_Sinav_Sorular_UniteSorular]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Sinav_Sorular] DROP CONSTRAINT [FK_Sinav_Sorular_UniteSorular];
GO
IF OBJECT_ID(N'[dbo].[FK_UniteSorular_SınavTipi]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UniteSorulars] DROP CONSTRAINT [FK_UniteSorular_SınavTipi];
GO
IF OBJECT_ID(N'[dbo].[FK_Sinif_Ogrenci_Sinif]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Sinif_Ogrenci] DROP CONSTRAINT [FK_Sinif_Ogrenci_Sinif];
GO
IF OBJECT_ID(N'[dbo].[FK_SoruCevaplar_UniteSorular]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SoruSiklaris] DROP CONSTRAINT [FK_SoruCevaplar_UniteSorular];
GO
IF OBJECT_ID(N'[dbo].[FK_TestCevaplar_TestSorular]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TestCevaplars] DROP CONSTRAINT [FK_TestCevaplar_TestSorular];
GO
IF OBJECT_ID(N'[dbo].[FK_TestSorular_Testler]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Test_Sorular] DROP CONSTRAINT [FK_TestSorular_Testler];
GO
IF OBJECT_ID(N'[dbo].[FK_TestSorular_UniteSorular]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Test_Sorular] DROP CONSTRAINT [FK_TestSorular_UniteSorular];
GO
IF OBJECT_ID(N'[dbo].[FK_Testler_UniteModul]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Testlers] DROP CONSTRAINT [FK_Testler_UniteModul];
GO
IF OBJECT_ID(N'[dbo].[FK_Tip_Sorular_TipSinavlar]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Tip_Sorular] DROP CONSTRAINT [FK_Tip_Sorular_TipSinavlar];
GO
IF OBJECT_ID(N'[dbo].[FK_Tip_Sorular_UniteSorular]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Tip_Sorular] DROP CONSTRAINT [FK_Tip_Sorular_UniteSorular];
GO
IF OBJECT_ID(N'[dbo].[FK_TipCevaplar_Tip_Sorular]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TipCevaplars] DROP CONSTRAINT [FK_TipCevaplar_Tip_Sorular];
GO
IF OBJECT_ID(N'[dbo].[FK_TipEgitim_Sorular_TipEgitimSinavlar]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TipEgitim_Sorular] DROP CONSTRAINT [FK_TipEgitim_Sorular_TipEgitimSinavlar];
GO
IF OBJECT_ID(N'[dbo].[FK_TipEgitim_Sorular_UniteSorular]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TipEgitim_Sorular] DROP CONSTRAINT [FK_TipEgitim_Sorular_UniteSorular];
GO
IF OBJECT_ID(N'[dbo].[FK_Unite_UniteModul]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Unites] DROP CONSTRAINT [FK_Unite_UniteModul];
GO
IF OBJECT_ID(N'[dbo].[FK_UniteSorular_Unite]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UniteSorulars] DROP CONSTRAINT [FK_UniteSorular_Unite];
GO
IF OBJECT_ID(N'[dbo].[FK_Duyurular_Firmalar]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Duyurulars] DROP CONSTRAINT [FK_Duyurular_Firmalar];
GO
IF OBJECT_ID(N'[dbo].[FK_Sinif_Firmalar]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Sinifs] DROP CONSTRAINT [FK_Sinif_Firmalar];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[AltSiklars]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AltSiklars];
GO
IF OBJECT_ID(N'[dbo].[AtananBolumlers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AtananBolumlers];
GO
IF OBJECT_ID(N'[dbo].[Bagajlars]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Bagajlars];
GO
IF OBJECT_ID(N'[dbo].[Bolums]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Bolums];
GO
IF OBJECT_ID(N'[dbo].[Duyurulars]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Duyurulars];
GO
IF OBJECT_ID(N'[dbo].[Egitim_Arsivi]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Egitim_Arsivi];
GO
IF OBJECT_ID(N'[dbo].[EgitimGirisSayfasis]', 'U') IS NOT NULL
    DROP TABLE [dbo].[EgitimGirisSayfasis];
GO
IF OBJECT_ID(N'[dbo].[Firmalars]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Firmalars];
GO
IF OBJECT_ID(N'[dbo].[HBagajlars]', 'U') IS NOT NULL
    DROP TABLE [dbo].[HBagajlars];
GO
IF OBJECT_ID(N'[dbo].[Kull_Rol_Adi]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Kull_Rol_Adi];
GO
IF OBJECT_ID(N'[dbo].[Kull_Rolleri]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Kull_Rolleri];
GO
IF OBJECT_ID(N'[dbo].[Kullanicis]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Kullanicis];
GO
IF OBJECT_ID(N'[dbo].[KursModullers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[KursModullers];
GO
IF OBJECT_ID(N'[dbo].[Loglamas]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Loglamas];
GO
IF OBJECT_ID(N'[dbo].[Maddelers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Maddelers];
GO
IF OBJECT_ID(N'[dbo].[Mesajlars]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Mesajlars];
GO
IF OBJECT_ID(N'[dbo].[Modullers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Modullers];
GO
IF OBJECT_ID(N'[dbo].[OkunanUnitelers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OkunanUnitelers];
GO
IF OBJECT_ID(N'[dbo].[Personels]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Personels];
GO
IF OBJECT_ID(N'[dbo].[Personel_Sinavlar]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Personel_Sinavlar];
GO
IF OBJECT_ID(N'[dbo].[Personel_Testler]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Personel_Testler];
GO
IF OBJECT_ID(N'[dbo].[Personel_TIP]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Personel_TIP];
GO
IF OBJECT_ID(N'[dbo].[SBagajKategoris]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SBagajKategoris];
GO
IF OBJECT_ID(N'[dbo].[SBagajlars]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SBagajlars];
GO
IF OBJECT_ID(N'[dbo].[SBMaddeResimlers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SBMaddeResimlers];
GO
IF OBJECT_ID(N'[dbo].[SBTehlikeliMKategoris]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SBTehlikeliMKategoris];
GO
IF OBJECT_ID(N'[dbo].[Sinav_Sorular]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Sinav_Sorular];
GO
IF OBJECT_ID(N'[dbo].[SinavCevaplars]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SinavCevaplars];
GO
IF OBJECT_ID(N'[dbo].[Sinavlars]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Sinavlars];
GO
IF OBJECT_ID(N'[dbo].[SinavTipis]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SinavTipis];
GO
IF OBJECT_ID(N'[dbo].[Sinifs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Sinifs];
GO
IF OBJECT_ID(N'[dbo].[Sinif_Ogrenci]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Sinif_Ogrenci];
GO
IF OBJECT_ID(N'[dbo].[SoruSiklaris]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SoruSiklaris];
GO
IF OBJECT_ID(N'[dbo].[sysdiagrams]', 'U') IS NOT NULL
    DROP TABLE [dbo].[sysdiagrams];
GO
IF OBJECT_ID(N'[dbo].[TehlikeliMaddeKategoris]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TehlikeliMaddeKategoris];
GO
IF OBJECT_ID(N'[dbo].[Test_Sorular]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Test_Sorular];
GO
IF OBJECT_ID(N'[dbo].[TestCevaplars]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TestCevaplars];
GO
IF OBJECT_ID(N'[dbo].[Testlers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Testlers];
GO
IF OBJECT_ID(N'[dbo].[Tip_Sorular]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Tip_Sorular];
GO
IF OBJECT_ID(N'[dbo].[TipCevaplars]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TipCevaplars];
GO
IF OBJECT_ID(N'[dbo].[TipEgitim_Sorular]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TipEgitim_Sorular];
GO
IF OBJECT_ID(N'[dbo].[TipEgitimSinavlars]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TipEgitimSinavlars];
GO
IF OBJECT_ID(N'[dbo].[TipSinavlars]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TipSinavlars];
GO
IF OBJECT_ID(N'[dbo].[Unites]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Unites];
GO
IF OBJECT_ID(N'[dbo].[UniteModuls]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UniteModuls];
GO
IF OBJECT_ID(N'[dbo].[UniteSorulars]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UniteSorulars];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'AltSiklars'
CREATE TABLE [dbo].[AltSiklars] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [AltCevapIcerik] nvarchar(250)  NOT NULL,
    [AltDogruSecenek] bit  NOT NULL,
    [UniteSorular_Ref] int  NOT NULL,
    [AltSoruSikki] nvarchar(2)  NOT NULL
);
GO

-- Creating table 'AtananBolumlers'
CREATE TABLE [dbo].[AtananBolumlers] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Sinif_Ogrenci_Ref] int  NOT NULL,
    [Bolum_Ref] int  NOT NULL
);
GO

-- Creating table 'Bagajlars'
CREATE TABLE [dbo].[Bagajlars] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [Resim] nvarchar(1024)  NOT NULL,
    [Isim] nvarchar(128)  NULL
);
GO

-- Creating table 'Bolums'
CREATE TABLE [dbo].[Bolums] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [BolumAdi] nvarchar(128)  NOT NULL
);
GO

-- Creating table 'Duyurulars'
CREATE TABLE [dbo].[Duyurulars] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [Baslik] nvarchar(512)  NOT NULL,
    [Duyuru] varchar(max)  NOT NULL,
    [IlanTarihi] datetime  NOT NULL,
    [DuyuruTarihi] datetime  NOT NULL,
    [Tipi] int  NOT NULL,
    [Oncelik] int  NOT NULL,
    [Firma_Ref] int  NULL
);
GO

-- Creating table 'Egitim_Arsivi'
CREATE TABLE [dbo].[Egitim_Arsivi] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [Baslik] nvarchar(max)  NOT NULL,
    [Icerik] varchar(max)  NOT NULL,
    [Unite_Ref] int  NOT NULL,
    [Video] nvarchar(512)  NULL
);
GO

-- Creating table 'EgitimGirisSayfasis'
CREATE TABLE [dbo].[EgitimGirisSayfasis] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Icerik] nvarchar(max)  NOT NULL,
    [Resim] nvarchar(512)  NULL
);
GO

-- Creating table 'Firmalars'
CREATE TABLE [dbo].[Firmalars] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [FirmaAdi] nvarchar(max)  NOT NULL,
    [Email] nvarchar(512)  NULL,
    [Telefon] nvarchar(24)  NULL,
    [Detay] varchar(max)  NULL
);
GO

-- Creating table 'HBagajlars'
CREATE TABLE [dbo].[HBagajlars] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [Resim] nvarchar(1024)  NOT NULL,
    [Isim] nvarchar(128)  NULL
);
GO

-- Creating table 'Kull_Rol_Adi'
CREATE TABLE [dbo].[Kull_Rol_Adi] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [Name] nvarchar(150)  NOT NULL,
    [Moduller_Ref] int  NOT NULL,
    [Grup] int  NULL
);
GO

-- Creating table 'Kull_Rolleri'
CREATE TABLE [dbo].[Kull_Rolleri] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [User_Ref] int  NULL,
    [User_Role_Name_Ref] int  NOT NULL
);
GO

-- Creating table 'Kullanicis'
CREATE TABLE [dbo].[Kullanicis] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [KullaniciAdi] nvarchar(15)  NOT NULL,
    [Sifre] nvarchar(10)  NOT NULL,
    [BaslangicTarihi] datetime  NOT NULL,
    [BitisTarihi] datetime  NULL,
    [Personel_Ref] int  NOT NULL,
    [Sistemde] bit  NULL,
    [Songiristarihi] datetime  NULL
);
GO

-- Creating table 'KursModullers'
CREATE TABLE [dbo].[KursModullers] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Bolum_Ref] int  NULL,
    [UniteModule_Ref] int  NULL
);
GO

-- Creating table 'Loglamas'
CREATE TABLE [dbo].[Loglamas] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [GirisTarihi] datetime  NOT NULL,
    [Kullanici_Ref] int  NOT NULL,
    [YapilanIs] varchar(max)  NOT NULL
);
GO

-- Creating table 'Maddelers'
CREATE TABLE [dbo].[Maddelers] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [ResimAd] nvarchar(250)  NOT NULL,
    [Resim] nvarchar(250)  NOT NULL,
    [TehlikeliMadde_Ref] int  NOT NULL
);
GO

-- Creating table 'Mesajlars'
CREATE TABLE [dbo].[Mesajlars] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [Gonderen_Ref] int  NOT NULL,
    [Alici_Ref] int  NULL,
    [Tarih] datetime  NOT NULL,
    [Icerik] nvarchar(max)  NOT NULL,
    [Saat] time  NOT NULL
);
GO

-- Creating table 'Modullers'
CREATE TABLE [dbo].[Modullers] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsDelete] bit  NOT NULL,
    [IsActive] bit  NOT NULL,
    [Name] nvarchar(100)  NOT NULL
);
GO

-- Creating table 'OkunanUnitelers'
CREATE TABLE [dbo].[OkunanUnitelers] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [AtananBolumler_Ref] int  NOT NULL,
    [Unite_Ref] int  NOT NULL
);
GO

-- Creating table 'Personels'
CREATE TABLE [dbo].[Personels] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [Adi] nvarchar(150)  NOT NULL,
    [Soyadi] nvarchar(150)  NOT NULL,
    [TCNo] varchar(11)  NOT NULL,
    [IsTelefon] nvarchar(24)  NULL,
    [EvTelefon] nvarchar(24)  NULL,
    [CepTelefon] nvarchar(24)  NOT NULL,
    [Email] nvarchar(150)  NOT NULL,
    [Adresi] nvarchar(550)  NOT NULL,
    [Cinsiyet] varchar(10)  NOT NULL,
    [DogumTarihi] datetime  NOT NULL,
    [Resim] nvarchar(100)  NULL,
    [Firma_Ref] int  NULL
);
GO

-- Creating table 'Personel_Sinavlar'
CREATE TABLE [dbo].[Personel_Sinavlar] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [Sinif_Ogrenci_Ref] int  NOT NULL,
    [Sinavlar_Ref] int  NOT NULL,
    [SinavAciklama] bit  NOT NULL,
    [SinavNotuUygulama] decimal(18,2)  NOT NULL,
    [SinavNotuTeorik] decimal(18,2)  NULL,
    [Durum] varchar(50)  NOT NULL,
    [DogruSayisiUygulama] int  NULL,
    [YanlisSayisiUgulama] int  NULL,
    [UygulamaBos] int  NULL,
    [DogruSayisiTeorik] int  NULL,
    [YanlisSayisiTeorik] int  NULL,
    [TeorikBos] int  NULL,
    [SinavTarihi] datetime  NULL,
    [BaslamaSaati] time  NULL,
    [BitisSaati] time  NULL,
    [SinavSuresi] int  NULL,
    [Basladi] int  NOT NULL,
    [Bitti] int  NOT NULL,
    [GirisSaati] time  NULL,
    [CikisSaati] time  NULL,
    [DuyuruTarihi] datetime  NULL
);
GO

-- Creating table 'Personel_Testler'
CREATE TABLE [dbo].[Personel_Testler] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [Sinif_Ogrenci_Ref] int  NOT NULL,
    [Testler_Ref] int  NOT NULL,
    [TestAciklama] bit  NOT NULL,
    [TestNotu] decimal(18,2)  NOT NULL,
    [Durum] varchar(50)  NOT NULL,
    [DogruSayisi] int  NULL,
    [YanlisSayisi] int  NULL,
    [Bos] int  NULL,
    [TestTarihi] datetime  NULL,
    [GirisSaati] time  NULL,
    [CikisSaati] time  NULL,
    [TestSuresi] int  NULL,
    [Basladi] int  NOT NULL,
    [Bitti] int  NOT NULL
);
GO

-- Creating table 'Personel_TIP'
CREATE TABLE [dbo].[Personel_TIP] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [TipNo] nvarchar(12)  NULL,
    [Personel_Ref] int  NOT NULL,
    [TipSinavlar_Ref] int  NOT NULL,
    [TipNotu] decimal(18,2)  NOT NULL,
    [DogruSayisi] int  NULL,
    [YanlisSayisi] int  NULL,
    [TipTarihi] datetime  NULL,
    [GirisSaati] time  NULL,
    [CikisSaati] time  NULL,
    [TipSuresi] int  NULL,
    [Basladi] int  NULL,
    [Bitti] int  NOT NULL,
    [Netice] nvarchar(24)  NULL
);
GO

-- Creating table 'SBagajKategoris'
CREATE TABLE [dbo].[SBagajKategoris] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [Isim] nvarchar(512)  NOT NULL
);
GO

-- Creating table 'SBagajlars'
CREATE TABLE [dbo].[SBagajlars] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [Resim] nvarchar(1024)  NOT NULL,
    [Isim] nvarchar(128)  NULL,
    [SKategori_Ref] int  NOT NULL
);
GO

-- Creating table 'SBMaddeResimlers'
CREATE TABLE [dbo].[SBMaddeResimlers] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [Resim] nvarchar(512)  NOT NULL,
    [SBTM_Ref] int  NOT NULL
);
GO

-- Creating table 'SBTehlikeliMKategoris'
CREATE TABLE [dbo].[SBTehlikeliMKategoris] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [Isim] nvarchar(512)  NOT NULL
);
GO

-- Creating table 'Sinav_Sorular'
CREATE TABLE [dbo].[Sinav_Sorular] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [Sinavlar_Ref] int  NOT NULL,
    [UniteSorular_Ref] int  NOT NULL,
    [SinavTipi_Ref] int  NOT NULL
);
GO

-- Creating table 'SinavCevaplars'
CREATE TABLE [dbo].[SinavCevaplars] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Personel_Sinavlar_Ref] int  NOT NULL,
    [Sınav_Sorular_Ref] int  NOT NULL,
    [GirilenCevap] int  NULL,
    [AltCevap] int  NULL,
    [CevapResimX] int  NULL,
    [CevapResimY] int  NULL,
    [DogruCevap] bit  NULL,
    [DogruAltCevap] bit  NULL,
    [KoordinatSecim] bit  NULL,
    [Netice] bit  NULL
);
GO

-- Creating table 'Sinavlars'
CREATE TABLE [dbo].[Sinavlars] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [SinavNo] nvarchar(15)  NOT NULL,
    [SoruSayisi] int  NOT NULL,
    [Aciklama] nvarchar(max)  NULL
);
GO

-- Creating table 'SinavTipis'
CREATE TABLE [dbo].[SinavTipis] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Baslik] nvarchar(250)  NOT NULL
);
GO

-- Creating table 'Sinifs'
CREATE TABLE [dbo].[Sinifs] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [SinifAdi] nvarchar(50)  NOT NULL,
    [Firma_Ref] int  NULL
);
GO

-- Creating table 'Sinif_Ogrenci'
CREATE TABLE [dbo].[Sinif_Ogrenci] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Personel_Ref] int  NOT NULL,
    [Sinif_Ref] int  NOT NULL,
    [Basladi] bit  NOT NULL,
    [Bitti] bit  NOT NULL,
    [BaslamaTarihi] datetime  NOT NULL,
    [BitisTarihi] datetime  NOT NULL,
    [F] nchar(10)  NULL
);
GO

-- Creating table 'SoruSiklaris'
CREATE TABLE [dbo].[SoruSiklaris] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [CevapIcerik] nvarchar(250)  NOT NULL,
    [UniteSorular_Ref] int  NOT NULL,
    [DogruSecenek] bit  NOT NULL,
    [SoruSikki] varchar(5)  NOT NULL
);
GO

-- Creating table 'sysdiagrams'
CREATE TABLE [dbo].[sysdiagrams] (
    [name] nvarchar(128)  NOT NULL,
    [principal_id] int  NOT NULL,
    [diagram_id] int IDENTITY(1,1) NOT NULL,
    [version] int  NULL,
    [definition] varbinary(max)  NULL
);
GO

-- Creating table 'TehlikeliMaddeKategoris'
CREATE TABLE [dbo].[TehlikeliMaddeKategoris] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [Baslik] nvarchar(150)  NOT NULL,
    [Resim] nvarchar(200)  NULL
);
GO

-- Creating table 'Test_Sorular'
CREATE TABLE [dbo].[Test_Sorular] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [Testler_Ref] int  NOT NULL,
    [UniteSorular_Ref] int  NOT NULL
);
GO

-- Creating table 'TestCevaplars'
CREATE TABLE [dbo].[TestCevaplars] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Personel_Testler_Ref] int  NOT NULL,
    [Test_Sorular_Ref] int  NOT NULL,
    [GirilenCevap] int  NULL,
    [AltCevap] int  NULL,
    [DogruCevap] bit  NULL,
    [DogruAltCevap] bit  NULL,
    [Netice] bit  NULL
);
GO

-- Creating table 'Testlers'
CREATE TABLE [dbo].[Testlers] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [TestNo] nvarchar(15)  NOT NULL,
    [SoruSayisi] int  NOT NULL,
    [Aciklama] nvarchar(max)  NULL,
    [UniteModul_Ref] int  NULL,
    [Sure] int  NULL
);
GO

-- Creating table 'Tip_Sorular'
CREATE TABLE [dbo].[Tip_Sorular] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [TipSinavlar_Ref] int  NOT NULL,
    [UniteSorular_Ref] int  NOT NULL
);
GO

-- Creating table 'TipCevaplars'
CREATE TABLE [dbo].[TipCevaplars] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Personel_Tip_Ref] int  NOT NULL,
    [Tip_Sorular_Ref] int  NOT NULL,
    [GirilenCevap] int  NULL,
    [CevapResimX] int  NULL,
    [CevapResimY] int  NULL,
    [DogruCevap] bit  NULL,
    [KoordinatSecim] bit  NULL,
    [Netice] bit  NULL
);
GO

-- Creating table 'TipEgitim_Sorular'
CREATE TABLE [dbo].[TipEgitim_Sorular] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [TipEgitSinavlar_Ref] int  NOT NULL,
    [Unite_Sorular_Ref] int  NOT NULL
);
GO

-- Creating table 'TipEgitimSinavlars'
CREATE TABLE [dbo].[TipEgitimSinavlars] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [TipEgitimNo] nvarchar(15)  NOT NULL,
    [SoruSayisi] int  NOT NULL,
    [Aciklama] nvarchar(max)  NOT NULL,
    [Sure] int  NOT NULL,
    [Yayinda] bit  NOT NULL
);
GO

-- Creating table 'TipSinavlars'
CREATE TABLE [dbo].[TipSinavlars] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [TipNo] nvarchar(15)  NOT NULL,
    [SoruSayisi] int  NOT NULL,
    [Aciklama] nvarchar(max)  NULL,
    [Sure] int  NOT NULL,
    [Yayinda] bit  NOT NULL
);
GO

-- Creating table 'Unites'
CREATE TABLE [dbo].[Unites] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [UniteAdi] nvarchar(550)  NOT NULL,
    [Modul_Ref] int  NULL
);
GO

-- Creating table 'UniteModuls'
CREATE TABLE [dbo].[UniteModuls] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [Modul] nvarchar(250)  NOT NULL
);
GO

-- Creating table 'UniteSorulars'
CREATE TABLE [dbo].[UniteSorulars] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsDelete] bit  NOT NULL,
    [SBagajlar_Ref] int  NULL,
    [STM_Ref] int  NULL,
    [Soru] nvarchar(550)  NOT NULL,
    [Unite_Ref] int  NULL,
    [XKordinat] decimal(18,2)  NULL,
    [YKordinat] decimal(18,2)  NULL,
    [SinavTipi_Ref] int  NOT NULL,
    [TMOlcek] decimal(18,2)  NULL,
    [TMSaydamlik] decimal(18,2)  NULL,
    [TMVektor] decimal(18,2)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ID] in table 'AltSiklars'
ALTER TABLE [dbo].[AltSiklars]
ADD CONSTRAINT [PK_AltSiklars]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'AtananBolumlers'
ALTER TABLE [dbo].[AtananBolumlers]
ADD CONSTRAINT [PK_AtananBolumlers]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Bagajlars'
ALTER TABLE [dbo].[Bagajlars]
ADD CONSTRAINT [PK_Bagajlars]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Bolums'
ALTER TABLE [dbo].[Bolums]
ADD CONSTRAINT [PK_Bolums]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Duyurulars'
ALTER TABLE [dbo].[Duyurulars]
ADD CONSTRAINT [PK_Duyurulars]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Egitim_Arsivi'
ALTER TABLE [dbo].[Egitim_Arsivi]
ADD CONSTRAINT [PK_Egitim_Arsivi]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'EgitimGirisSayfasis'
ALTER TABLE [dbo].[EgitimGirisSayfasis]
ADD CONSTRAINT [PK_EgitimGirisSayfasis]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Firmalars'
ALTER TABLE [dbo].[Firmalars]
ADD CONSTRAINT [PK_Firmalars]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'HBagajlars'
ALTER TABLE [dbo].[HBagajlars]
ADD CONSTRAINT [PK_HBagajlars]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Kull_Rol_Adi'
ALTER TABLE [dbo].[Kull_Rol_Adi]
ADD CONSTRAINT [PK_Kull_Rol_Adi]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Kull_Rolleri'
ALTER TABLE [dbo].[Kull_Rolleri]
ADD CONSTRAINT [PK_Kull_Rolleri]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Kullanicis'
ALTER TABLE [dbo].[Kullanicis]
ADD CONSTRAINT [PK_Kullanicis]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'KursModullers'
ALTER TABLE [dbo].[KursModullers]
ADD CONSTRAINT [PK_KursModullers]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Loglamas'
ALTER TABLE [dbo].[Loglamas]
ADD CONSTRAINT [PK_Loglamas]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Maddelers'
ALTER TABLE [dbo].[Maddelers]
ADD CONSTRAINT [PK_Maddelers]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Mesajlars'
ALTER TABLE [dbo].[Mesajlars]
ADD CONSTRAINT [PK_Mesajlars]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Modullers'
ALTER TABLE [dbo].[Modullers]
ADD CONSTRAINT [PK_Modullers]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'OkunanUnitelers'
ALTER TABLE [dbo].[OkunanUnitelers]
ADD CONSTRAINT [PK_OkunanUnitelers]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Personels'
ALTER TABLE [dbo].[Personels]
ADD CONSTRAINT [PK_Personels]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Personel_Sinavlar'
ALTER TABLE [dbo].[Personel_Sinavlar]
ADD CONSTRAINT [PK_Personel_Sinavlar]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Personel_Testler'
ALTER TABLE [dbo].[Personel_Testler]
ADD CONSTRAINT [PK_Personel_Testler]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Personel_TIP'
ALTER TABLE [dbo].[Personel_TIP]
ADD CONSTRAINT [PK_Personel_TIP]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'SBagajKategoris'
ALTER TABLE [dbo].[SBagajKategoris]
ADD CONSTRAINT [PK_SBagajKategoris]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'SBagajlars'
ALTER TABLE [dbo].[SBagajlars]
ADD CONSTRAINT [PK_SBagajlars]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'SBMaddeResimlers'
ALTER TABLE [dbo].[SBMaddeResimlers]
ADD CONSTRAINT [PK_SBMaddeResimlers]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'SBTehlikeliMKategoris'
ALTER TABLE [dbo].[SBTehlikeliMKategoris]
ADD CONSTRAINT [PK_SBTehlikeliMKategoris]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Sinav_Sorular'
ALTER TABLE [dbo].[Sinav_Sorular]
ADD CONSTRAINT [PK_Sinav_Sorular]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'SinavCevaplars'
ALTER TABLE [dbo].[SinavCevaplars]
ADD CONSTRAINT [PK_SinavCevaplars]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Sinavlars'
ALTER TABLE [dbo].[Sinavlars]
ADD CONSTRAINT [PK_Sinavlars]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'SinavTipis'
ALTER TABLE [dbo].[SinavTipis]
ADD CONSTRAINT [PK_SinavTipis]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Sinifs'
ALTER TABLE [dbo].[Sinifs]
ADD CONSTRAINT [PK_Sinifs]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Sinif_Ogrenci'
ALTER TABLE [dbo].[Sinif_Ogrenci]
ADD CONSTRAINT [PK_Sinif_Ogrenci]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'SoruSiklaris'
ALTER TABLE [dbo].[SoruSiklaris]
ADD CONSTRAINT [PK_SoruSiklaris]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [diagram_id] in table 'sysdiagrams'
ALTER TABLE [dbo].[sysdiagrams]
ADD CONSTRAINT [PK_sysdiagrams]
    PRIMARY KEY CLUSTERED ([diagram_id] ASC);
GO

-- Creating primary key on [ID] in table 'TehlikeliMaddeKategoris'
ALTER TABLE [dbo].[TehlikeliMaddeKategoris]
ADD CONSTRAINT [PK_TehlikeliMaddeKategoris]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Test_Sorular'
ALTER TABLE [dbo].[Test_Sorular]
ADD CONSTRAINT [PK_Test_Sorular]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'TestCevaplars'
ALTER TABLE [dbo].[TestCevaplars]
ADD CONSTRAINT [PK_TestCevaplars]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Testlers'
ALTER TABLE [dbo].[Testlers]
ADD CONSTRAINT [PK_Testlers]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Tip_Sorular'
ALTER TABLE [dbo].[Tip_Sorular]
ADD CONSTRAINT [PK_Tip_Sorular]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'TipCevaplars'
ALTER TABLE [dbo].[TipCevaplars]
ADD CONSTRAINT [PK_TipCevaplars]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'TipEgitim_Sorular'
ALTER TABLE [dbo].[TipEgitim_Sorular]
ADD CONSTRAINT [PK_TipEgitim_Sorular]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'TipEgitimSinavlars'
ALTER TABLE [dbo].[TipEgitimSinavlars]
ADD CONSTRAINT [PK_TipEgitimSinavlars]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'TipSinavlars'
ALTER TABLE [dbo].[TipSinavlars]
ADD CONSTRAINT [PK_TipSinavlars]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Unites'
ALTER TABLE [dbo].[Unites]
ADD CONSTRAINT [PK_Unites]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'UniteModuls'
ALTER TABLE [dbo].[UniteModuls]
ADD CONSTRAINT [PK_UniteModuls]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'UniteSorulars'
ALTER TABLE [dbo].[UniteSorulars]
ADD CONSTRAINT [PK_UniteSorulars]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [UniteSorular_Ref] in table 'AltSiklars'
ALTER TABLE [dbo].[AltSiklars]
ADD CONSTRAINT [FK_AltSiklar_UniteSorular]
    FOREIGN KEY ([UniteSorular_Ref])
    REFERENCES [dbo].[UniteSorulars]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AltSiklar_UniteSorular'
CREATE INDEX [IX_FK_AltSiklar_UniteSorular]
ON [dbo].[AltSiklars]
    ([UniteSorular_Ref]);
GO

-- Creating foreign key on [Bolum_Ref] in table 'AtananBolumlers'
ALTER TABLE [dbo].[AtananBolumlers]
ADD CONSTRAINT [FK_AtananBolumler_Bolum]
    FOREIGN KEY ([Bolum_Ref])
    REFERENCES [dbo].[Bolums]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AtananBolumler_Bolum'
CREATE INDEX [IX_FK_AtananBolumler_Bolum]
ON [dbo].[AtananBolumlers]
    ([Bolum_Ref]);
GO

-- Creating foreign key on [Sinif_Ogrenci_Ref] in table 'AtananBolumlers'
ALTER TABLE [dbo].[AtananBolumlers]
ADD CONSTRAINT [FK_AtananBolumler_Sinif_Ogrenci]
    FOREIGN KEY ([Sinif_Ogrenci_Ref])
    REFERENCES [dbo].[Sinif_Ogrenci]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AtananBolumler_Sinif_Ogrenci'
CREATE INDEX [IX_FK_AtananBolumler_Sinif_Ogrenci]
ON [dbo].[AtananBolumlers]
    ([Sinif_Ogrenci_Ref]);
GO

-- Creating foreign key on [AtananBolumler_Ref] in table 'OkunanUnitelers'
ALTER TABLE [dbo].[OkunanUnitelers]
ADD CONSTRAINT [FK_OkunanUniteler_AtananBolumler]
    FOREIGN KEY ([AtananBolumler_Ref])
    REFERENCES [dbo].[AtananBolumlers]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_OkunanUniteler_AtananBolumler'
CREATE INDEX [IX_FK_OkunanUniteler_AtananBolumler]
ON [dbo].[OkunanUnitelers]
    ([AtananBolumler_Ref]);
GO

-- Creating foreign key on [Bolum_Ref] in table 'KursModullers'
ALTER TABLE [dbo].[KursModullers]
ADD CONSTRAINT [FK_KursModuller_Bolum]
    FOREIGN KEY ([Bolum_Ref])
    REFERENCES [dbo].[Bolums]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_KursModuller_Bolum'
CREATE INDEX [IX_FK_KursModuller_Bolum]
ON [dbo].[KursModullers]
    ([Bolum_Ref]);
GO

-- Creating foreign key on [Unite_Ref] in table 'Egitim_Arsivi'
ALTER TABLE [dbo].[Egitim_Arsivi]
ADD CONSTRAINT [FK_Egitim_Arsivi_Unite]
    FOREIGN KEY ([Unite_Ref])
    REFERENCES [dbo].[Unites]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Egitim_Arsivi_Unite'
CREATE INDEX [IX_FK_Egitim_Arsivi_Unite]
ON [dbo].[Egitim_Arsivi]
    ([Unite_Ref]);
GO

-- Creating foreign key on [Firma_Ref] in table 'Personels'
ALTER TABLE [dbo].[Personels]
ADD CONSTRAINT [FK_Personel_Firmalar]
    FOREIGN KEY ([Firma_Ref])
    REFERENCES [dbo].[Firmalars]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Personel_Firmalar'
CREATE INDEX [IX_FK_Personel_Firmalar]
ON [dbo].[Personels]
    ([Firma_Ref]);
GO

-- Creating foreign key on [Moduller_Ref] in table 'Kull_Rol_Adi'
ALTER TABLE [dbo].[Kull_Rol_Adi]
ADD CONSTRAINT [FK_Kull_Rol_Adi_Moduller]
    FOREIGN KEY ([Moduller_Ref])
    REFERENCES [dbo].[Modullers]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Kull_Rol_Adi_Moduller'
CREATE INDEX [IX_FK_Kull_Rol_Adi_Moduller]
ON [dbo].[Kull_Rol_Adi]
    ([Moduller_Ref]);
GO

-- Creating foreign key on [User_Role_Name_Ref] in table 'Kull_Rolleri'
ALTER TABLE [dbo].[Kull_Rolleri]
ADD CONSTRAINT [FK_Kull_Rolleri_Kull_Rol_Adi]
    FOREIGN KEY ([User_Role_Name_Ref])
    REFERENCES [dbo].[Kull_Rol_Adi]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Kull_Rolleri_Kull_Rol_Adi'
CREATE INDEX [IX_FK_Kull_Rolleri_Kull_Rol_Adi]
ON [dbo].[Kull_Rolleri]
    ([User_Role_Name_Ref]);
GO

-- Creating foreign key on [User_Ref] in table 'Kull_Rolleri'
ALTER TABLE [dbo].[Kull_Rolleri]
ADD CONSTRAINT [FK_Kull_Rolleri_Kullanici]
    FOREIGN KEY ([User_Ref])
    REFERENCES [dbo].[Kullanicis]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Kull_Rolleri_Kullanici'
CREATE INDEX [IX_FK_Kull_Rolleri_Kullanici]
ON [dbo].[Kull_Rolleri]
    ([User_Ref]);
GO

-- Creating foreign key on [Personel_Ref] in table 'Kullanicis'
ALTER TABLE [dbo].[Kullanicis]
ADD CONSTRAINT [FK_Kullanici_Personel1]
    FOREIGN KEY ([Personel_Ref])
    REFERENCES [dbo].[Personels]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Kullanici_Personel1'
CREATE INDEX [IX_FK_Kullanici_Personel1]
ON [dbo].[Kullanicis]
    ([Personel_Ref]);
GO

-- Creating foreign key on [Kullanici_Ref] in table 'Loglamas'
ALTER TABLE [dbo].[Loglamas]
ADD CONSTRAINT [FK_Loglama_Kullanici]
    FOREIGN KEY ([Kullanici_Ref])
    REFERENCES [dbo].[Kullanicis]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Loglama_Kullanici'
CREATE INDEX [IX_FK_Loglama_Kullanici]
ON [dbo].[Loglamas]
    ([Kullanici_Ref]);
GO

-- Creating foreign key on [UniteModule_Ref] in table 'KursModullers'
ALTER TABLE [dbo].[KursModullers]
ADD CONSTRAINT [FK_KursModuller_UniteModul]
    FOREIGN KEY ([UniteModule_Ref])
    REFERENCES [dbo].[UniteModuls]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_KursModuller_UniteModul'
CREATE INDEX [IX_FK_KursModuller_UniteModul]
ON [dbo].[KursModullers]
    ([UniteModule_Ref]);
GO

-- Creating foreign key on [TehlikeliMadde_Ref] in table 'Maddelers'
ALTER TABLE [dbo].[Maddelers]
ADD CONSTRAINT [FK_Maddeler_BaslikMadde]
    FOREIGN KEY ([TehlikeliMadde_Ref])
    REFERENCES [dbo].[TehlikeliMaddeKategoris]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Maddeler_BaslikMadde'
CREATE INDEX [IX_FK_Maddeler_BaslikMadde]
ON [dbo].[Maddelers]
    ([TehlikeliMadde_Ref]);
GO

-- Creating foreign key on [Gonderen_Ref] in table 'Mesajlars'
ALTER TABLE [dbo].[Mesajlars]
ADD CONSTRAINT [FK_Mesajlar_Personel]
    FOREIGN KEY ([Gonderen_Ref])
    REFERENCES [dbo].[Personels]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Mesajlar_Personel'
CREATE INDEX [IX_FK_Mesajlar_Personel]
ON [dbo].[Mesajlars]
    ([Gonderen_Ref]);
GO

-- Creating foreign key on [Unite_Ref] in table 'OkunanUnitelers'
ALTER TABLE [dbo].[OkunanUnitelers]
ADD CONSTRAINT [FK_OkunanUniteler_Unite]
    FOREIGN KEY ([Unite_Ref])
    REFERENCES [dbo].[Unites]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_OkunanUniteler_Unite'
CREATE INDEX [IX_FK_OkunanUniteler_Unite]
ON [dbo].[OkunanUnitelers]
    ([Unite_Ref]);
GO

-- Creating foreign key on [Personel_Ref] in table 'Personel_TIP'
ALTER TABLE [dbo].[Personel_TIP]
ADD CONSTRAINT [FK_Personel_TIP_Personel]
    FOREIGN KEY ([Personel_Ref])
    REFERENCES [dbo].[Personels]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Personel_TIP_Personel'
CREATE INDEX [IX_FK_Personel_TIP_Personel]
ON [dbo].[Personel_TIP]
    ([Personel_Ref]);
GO

-- Creating foreign key on [Personel_Ref] in table 'Sinif_Ogrenci'
ALTER TABLE [dbo].[Sinif_Ogrenci]
ADD CONSTRAINT [FK_Sinif_Ogrenci_Personel]
    FOREIGN KEY ([Personel_Ref])
    REFERENCES [dbo].[Personels]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Sinif_Ogrenci_Personel'
CREATE INDEX [IX_FK_Sinif_Ogrenci_Personel]
ON [dbo].[Sinif_Ogrenci]
    ([Personel_Ref]);
GO

-- Creating foreign key on [Sinavlar_Ref] in table 'Personel_Sinavlar'
ALTER TABLE [dbo].[Personel_Sinavlar]
ADD CONSTRAINT [FK_Personel_Sinavlar_Sinavlar]
    FOREIGN KEY ([Sinavlar_Ref])
    REFERENCES [dbo].[Sinavlars]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Personel_Sinavlar_Sinavlar'
CREATE INDEX [IX_FK_Personel_Sinavlar_Sinavlar]
ON [dbo].[Personel_Sinavlar]
    ([Sinavlar_Ref]);
GO

-- Creating foreign key on [Sinif_Ogrenci_Ref] in table 'Personel_Sinavlar'
ALTER TABLE [dbo].[Personel_Sinavlar]
ADD CONSTRAINT [FK_Personel_Sinavlar_Sinif_Ogrenci]
    FOREIGN KEY ([Sinif_Ogrenci_Ref])
    REFERENCES [dbo].[Sinif_Ogrenci]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Personel_Sinavlar_Sinif_Ogrenci'
CREATE INDEX [IX_FK_Personel_Sinavlar_Sinif_Ogrenci]
ON [dbo].[Personel_Sinavlar]
    ([Sinif_Ogrenci_Ref]);
GO

-- Creating foreign key on [Personel_Sinavlar_Ref] in table 'SinavCevaplars'
ALTER TABLE [dbo].[SinavCevaplars]
ADD CONSTRAINT [FK_SınavCevaplar_Personel_Sinavlar]
    FOREIGN KEY ([Personel_Sinavlar_Ref])
    REFERENCES [dbo].[Personel_Sinavlar]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SınavCevaplar_Personel_Sinavlar'
CREATE INDEX [IX_FK_SınavCevaplar_Personel_Sinavlar]
ON [dbo].[SinavCevaplars]
    ([Personel_Sinavlar_Ref]);
GO

-- Creating foreign key on [Sinif_Ogrenci_Ref] in table 'Personel_Testler'
ALTER TABLE [dbo].[Personel_Testler]
ADD CONSTRAINT [FK_Personel_Testler_Sinif_Ogrenci]
    FOREIGN KEY ([Sinif_Ogrenci_Ref])
    REFERENCES [dbo].[Sinif_Ogrenci]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Personel_Testler_Sinif_Ogrenci'
CREATE INDEX [IX_FK_Personel_Testler_Sinif_Ogrenci]
ON [dbo].[Personel_Testler]
    ([Sinif_Ogrenci_Ref]);
GO

-- Creating foreign key on [Testler_Ref] in table 'Personel_Testler'
ALTER TABLE [dbo].[Personel_Testler]
ADD CONSTRAINT [FK_Personel_Testler_Testler]
    FOREIGN KEY ([Testler_Ref])
    REFERENCES [dbo].[Testlers]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Personel_Testler_Testler'
CREATE INDEX [IX_FK_Personel_Testler_Testler]
ON [dbo].[Personel_Testler]
    ([Testler_Ref]);
GO

-- Creating foreign key on [Personel_Testler_Ref] in table 'TestCevaplars'
ALTER TABLE [dbo].[TestCevaplars]
ADD CONSTRAINT [FK_TestCevaplar_Personel_Testler]
    FOREIGN KEY ([Personel_Testler_Ref])
    REFERENCES [dbo].[Personel_Testler]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TestCevaplar_Personel_Testler'
CREATE INDEX [IX_FK_TestCevaplar_Personel_Testler]
ON [dbo].[TestCevaplars]
    ([Personel_Testler_Ref]);
GO

-- Creating foreign key on [TipSinavlar_Ref] in table 'Personel_TIP'
ALTER TABLE [dbo].[Personel_TIP]
ADD CONSTRAINT [FK_Personel_TIP_TipSinavlar]
    FOREIGN KEY ([TipSinavlar_Ref])
    REFERENCES [dbo].[TipSinavlars]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Personel_TIP_TipSinavlar'
CREATE INDEX [IX_FK_Personel_TIP_TipSinavlar]
ON [dbo].[Personel_TIP]
    ([TipSinavlar_Ref]);
GO

-- Creating foreign key on [Personel_Tip_Ref] in table 'TipCevaplars'
ALTER TABLE [dbo].[TipCevaplars]
ADD CONSTRAINT [FK_TipCevaplar_Personel_TIP]
    FOREIGN KEY ([Personel_Tip_Ref])
    REFERENCES [dbo].[Personel_TIP]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TipCevaplar_Personel_TIP'
CREATE INDEX [IX_FK_TipCevaplar_Personel_TIP]
ON [dbo].[TipCevaplars]
    ([Personel_Tip_Ref]);
GO

-- Creating foreign key on [SKategori_Ref] in table 'SBagajlars'
ALTER TABLE [dbo].[SBagajlars]
ADD CONSTRAINT [FK_SBagajlar_SBagajKategori]
    FOREIGN KEY ([SKategori_Ref])
    REFERENCES [dbo].[SBagajKategoris]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SBagajlar_SBagajKategori'
CREATE INDEX [IX_FK_SBagajlar_SBagajKategori]
ON [dbo].[SBagajlars]
    ([SKategori_Ref]);
GO

-- Creating foreign key on [SBagajlar_Ref] in table 'UniteSorulars'
ALTER TABLE [dbo].[UniteSorulars]
ADD CONSTRAINT [FK_UniteSorular_SBagajlar]
    FOREIGN KEY ([SBagajlar_Ref])
    REFERENCES [dbo].[SBagajlars]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UniteSorular_SBagajlar'
CREATE INDEX [IX_FK_UniteSorular_SBagajlar]
ON [dbo].[UniteSorulars]
    ([SBagajlar_Ref]);
GO

-- Creating foreign key on [SBTM_Ref] in table 'SBMaddeResimlers'
ALTER TABLE [dbo].[SBMaddeResimlers]
ADD CONSTRAINT [FK_SBMaddeResimler_SBTehlikeliMKategori]
    FOREIGN KEY ([SBTM_Ref])
    REFERENCES [dbo].[SBTehlikeliMKategoris]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SBMaddeResimler_SBTehlikeliMKategori'
CREATE INDEX [IX_FK_SBMaddeResimler_SBTehlikeliMKategori]
ON [dbo].[SBMaddeResimlers]
    ([SBTM_Ref]);
GO

-- Creating foreign key on [STM_Ref] in table 'UniteSorulars'
ALTER TABLE [dbo].[UniteSorulars]
ADD CONSTRAINT [FK_UniteSorular_SBMaddeResimler]
    FOREIGN KEY ([STM_Ref])
    REFERENCES [dbo].[SBMaddeResimlers]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UniteSorular_SBMaddeResimler'
CREATE INDEX [IX_FK_UniteSorular_SBMaddeResimler]
ON [dbo].[UniteSorulars]
    ([STM_Ref]);
GO

-- Creating foreign key on [Sınav_Sorular_Ref] in table 'SinavCevaplars'
ALTER TABLE [dbo].[SinavCevaplars]
ADD CONSTRAINT [FK_SınavCevaplar_Sinav_Sorular]
    FOREIGN KEY ([Sınav_Sorular_Ref])
    REFERENCES [dbo].[Sinav_Sorular]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SınavCevaplar_Sinav_Sorular'
CREATE INDEX [IX_FK_SınavCevaplar_Sinav_Sorular]
ON [dbo].[SinavCevaplars]
    ([Sınav_Sorular_Ref]);
GO

-- Creating foreign key on [SinavTipi_Ref] in table 'Sinav_Sorular'
ALTER TABLE [dbo].[Sinav_Sorular]
ADD CONSTRAINT [FK_Sinav_Sorular_SınavTipi]
    FOREIGN KEY ([SinavTipi_Ref])
    REFERENCES [dbo].[SinavTipis]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Sinav_Sorular_SınavTipi'
CREATE INDEX [IX_FK_Sinav_Sorular_SınavTipi]
ON [dbo].[Sinav_Sorular]
    ([SinavTipi_Ref]);
GO

-- Creating foreign key on [Sinavlar_Ref] in table 'Sinav_Sorular'
ALTER TABLE [dbo].[Sinav_Sorular]
ADD CONSTRAINT [FK_Sinav_Sorular_Sinavlar]
    FOREIGN KEY ([Sinavlar_Ref])
    REFERENCES [dbo].[Sinavlars]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Sinav_Sorular_Sinavlar'
CREATE INDEX [IX_FK_Sinav_Sorular_Sinavlar]
ON [dbo].[Sinav_Sorular]
    ([Sinavlar_Ref]);
GO

-- Creating foreign key on [UniteSorular_Ref] in table 'Sinav_Sorular'
ALTER TABLE [dbo].[Sinav_Sorular]
ADD CONSTRAINT [FK_Sinav_Sorular_UniteSorular]
    FOREIGN KEY ([UniteSorular_Ref])
    REFERENCES [dbo].[UniteSorulars]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Sinav_Sorular_UniteSorular'
CREATE INDEX [IX_FK_Sinav_Sorular_UniteSorular]
ON [dbo].[Sinav_Sorular]
    ([UniteSorular_Ref]);
GO

-- Creating foreign key on [SinavTipi_Ref] in table 'UniteSorulars'
ALTER TABLE [dbo].[UniteSorulars]
ADD CONSTRAINT [FK_UniteSorular_SınavTipi]
    FOREIGN KEY ([SinavTipi_Ref])
    REFERENCES [dbo].[SinavTipis]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UniteSorular_SınavTipi'
CREATE INDEX [IX_FK_UniteSorular_SınavTipi]
ON [dbo].[UniteSorulars]
    ([SinavTipi_Ref]);
GO

-- Creating foreign key on [Sinif_Ref] in table 'Sinif_Ogrenci'
ALTER TABLE [dbo].[Sinif_Ogrenci]
ADD CONSTRAINT [FK_Sinif_Ogrenci_Sinif]
    FOREIGN KEY ([Sinif_Ref])
    REFERENCES [dbo].[Sinifs]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Sinif_Ogrenci_Sinif'
CREATE INDEX [IX_FK_Sinif_Ogrenci_Sinif]
ON [dbo].[Sinif_Ogrenci]
    ([Sinif_Ref]);
GO

-- Creating foreign key on [UniteSorular_Ref] in table 'SoruSiklaris'
ALTER TABLE [dbo].[SoruSiklaris]
ADD CONSTRAINT [FK_SoruCevaplar_UniteSorular]
    FOREIGN KEY ([UniteSorular_Ref])
    REFERENCES [dbo].[UniteSorulars]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SoruCevaplar_UniteSorular'
CREATE INDEX [IX_FK_SoruCevaplar_UniteSorular]
ON [dbo].[SoruSiklaris]
    ([UniteSorular_Ref]);
GO

-- Creating foreign key on [Test_Sorular_Ref] in table 'TestCevaplars'
ALTER TABLE [dbo].[TestCevaplars]
ADD CONSTRAINT [FK_TestCevaplar_TestSorular]
    FOREIGN KEY ([Test_Sorular_Ref])
    REFERENCES [dbo].[Test_Sorular]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TestCevaplar_TestSorular'
CREATE INDEX [IX_FK_TestCevaplar_TestSorular]
ON [dbo].[TestCevaplars]
    ([Test_Sorular_Ref]);
GO

-- Creating foreign key on [Testler_Ref] in table 'Test_Sorular'
ALTER TABLE [dbo].[Test_Sorular]
ADD CONSTRAINT [FK_TestSorular_Testler]
    FOREIGN KEY ([Testler_Ref])
    REFERENCES [dbo].[Testlers]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TestSorular_Testler'
CREATE INDEX [IX_FK_TestSorular_Testler]
ON [dbo].[Test_Sorular]
    ([Testler_Ref]);
GO

-- Creating foreign key on [UniteSorular_Ref] in table 'Test_Sorular'
ALTER TABLE [dbo].[Test_Sorular]
ADD CONSTRAINT [FK_TestSorular_UniteSorular]
    FOREIGN KEY ([UniteSorular_Ref])
    REFERENCES [dbo].[UniteSorulars]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TestSorular_UniteSorular'
CREATE INDEX [IX_FK_TestSorular_UniteSorular]
ON [dbo].[Test_Sorular]
    ([UniteSorular_Ref]);
GO

-- Creating foreign key on [UniteModul_Ref] in table 'Testlers'
ALTER TABLE [dbo].[Testlers]
ADD CONSTRAINT [FK_Testler_UniteModul]
    FOREIGN KEY ([UniteModul_Ref])
    REFERENCES [dbo].[UniteModuls]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Testler_UniteModul'
CREATE INDEX [IX_FK_Testler_UniteModul]
ON [dbo].[Testlers]
    ([UniteModul_Ref]);
GO

-- Creating foreign key on [TipSinavlar_Ref] in table 'Tip_Sorular'
ALTER TABLE [dbo].[Tip_Sorular]
ADD CONSTRAINT [FK_Tip_Sorular_TipSinavlar]
    FOREIGN KEY ([TipSinavlar_Ref])
    REFERENCES [dbo].[TipSinavlars]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Tip_Sorular_TipSinavlar'
CREATE INDEX [IX_FK_Tip_Sorular_TipSinavlar]
ON [dbo].[Tip_Sorular]
    ([TipSinavlar_Ref]);
GO

-- Creating foreign key on [UniteSorular_Ref] in table 'Tip_Sorular'
ALTER TABLE [dbo].[Tip_Sorular]
ADD CONSTRAINT [FK_Tip_Sorular_UniteSorular]
    FOREIGN KEY ([UniteSorular_Ref])
    REFERENCES [dbo].[UniteSorulars]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Tip_Sorular_UniteSorular'
CREATE INDEX [IX_FK_Tip_Sorular_UniteSorular]
ON [dbo].[Tip_Sorular]
    ([UniteSorular_Ref]);
GO

-- Creating foreign key on [Tip_Sorular_Ref] in table 'TipCevaplars'
ALTER TABLE [dbo].[TipCevaplars]
ADD CONSTRAINT [FK_TipCevaplar_Tip_Sorular]
    FOREIGN KEY ([Tip_Sorular_Ref])
    REFERENCES [dbo].[Tip_Sorular]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TipCevaplar_Tip_Sorular'
CREATE INDEX [IX_FK_TipCevaplar_Tip_Sorular]
ON [dbo].[TipCevaplars]
    ([Tip_Sorular_Ref]);
GO

-- Creating foreign key on [TipEgitSinavlar_Ref] in table 'TipEgitim_Sorular'
ALTER TABLE [dbo].[TipEgitim_Sorular]
ADD CONSTRAINT [FK_TipEgitim_Sorular_TipEgitimSinavlar]
    FOREIGN KEY ([TipEgitSinavlar_Ref])
    REFERENCES [dbo].[TipEgitimSinavlars]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TipEgitim_Sorular_TipEgitimSinavlar'
CREATE INDEX [IX_FK_TipEgitim_Sorular_TipEgitimSinavlar]
ON [dbo].[TipEgitim_Sorular]
    ([TipEgitSinavlar_Ref]);
GO

-- Creating foreign key on [Unite_Sorular_Ref] in table 'TipEgitim_Sorular'
ALTER TABLE [dbo].[TipEgitim_Sorular]
ADD CONSTRAINT [FK_TipEgitim_Sorular_UniteSorular]
    FOREIGN KEY ([Unite_Sorular_Ref])
    REFERENCES [dbo].[UniteSorulars]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TipEgitim_Sorular_UniteSorular'
CREATE INDEX [IX_FK_TipEgitim_Sorular_UniteSorular]
ON [dbo].[TipEgitim_Sorular]
    ([Unite_Sorular_Ref]);
GO

-- Creating foreign key on [Modul_Ref] in table 'Unites'
ALTER TABLE [dbo].[Unites]
ADD CONSTRAINT [FK_Unite_UniteModul]
    FOREIGN KEY ([Modul_Ref])
    REFERENCES [dbo].[UniteModuls]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Unite_UniteModul'
CREATE INDEX [IX_FK_Unite_UniteModul]
ON [dbo].[Unites]
    ([Modul_Ref]);
GO

-- Creating foreign key on [Unite_Ref] in table 'UniteSorulars'
ALTER TABLE [dbo].[UniteSorulars]
ADD CONSTRAINT [FK_UniteSorular_Unite]
    FOREIGN KEY ([Unite_Ref])
    REFERENCES [dbo].[Unites]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UniteSorular_Unite'
CREATE INDEX [IX_FK_UniteSorular_Unite]
ON [dbo].[UniteSorulars]
    ([Unite_Ref]);
GO

-- Creating foreign key on [Firma_Ref] in table 'Duyurulars'
ALTER TABLE [dbo].[Duyurulars]
ADD CONSTRAINT [FK_Duyurular_Firmalar]
    FOREIGN KEY ([Firma_Ref])
    REFERENCES [dbo].[Firmalars]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Duyurular_Firmalar'
CREATE INDEX [IX_FK_Duyurular_Firmalar]
ON [dbo].[Duyurulars]
    ([Firma_Ref]);
GO

-- Creating foreign key on [Firma_Ref] in table 'Sinifs'
ALTER TABLE [dbo].[Sinifs]
ADD CONSTRAINT [FK_Sinif_Firmalar]
    FOREIGN KEY ([Firma_Ref])
    REFERENCES [dbo].[Firmalars]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Sinif_Firmalar'
CREATE INDEX [IX_FK_Sinif_Firmalar]
ON [dbo].[Sinifs]
    ([Firma_Ref]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------