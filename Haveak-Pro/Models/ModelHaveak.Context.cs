﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Haveak_Pro.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class HaveakTurEntities : DbContext
    {
        public HaveakTurEntities()
            : base("name=HaveakTurEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<AltSiklar> AltSiklars { get; set; }
        public virtual DbSet<AtananBolumler> AtananBolumlers { get; set; }
        public virtual DbSet<Bagajlar> Bagajlars { get; set; }
        public virtual DbSet<Bolum> Bolums { get; set; }
        public virtual DbSet<DBBilgi> DBBilgis { get; set; }
        public virtual DbSet<Duyurular> Duyurulars { get; set; }
        public virtual DbSet<Egitim_Arsivi> Egitim_Arsivi { get; set; }
        public virtual DbSet<Egitim_Log> Egitim_Log { get; set; }
        public virtual DbSet<EgitimGirisSayfasi> EgitimGirisSayfasis { get; set; }
        public virtual DbSet<Firmalar> Firmalars { get; set; }
        public virtual DbSet<HBagajlar> HBagajlars { get; set; }
        public virtual DbSet<Kull_Rol_Adi> Kull_Rol_Adi { get; set; }
        public virtual DbSet<Kull_Rolleri> Kull_Rolleri { get; set; }
        public virtual DbSet<Kullanici> Kullanicis { get; set; }
        public virtual DbSet<KursModuller> KursModullers { get; set; }
        public virtual DbSet<Loglama> Loglamas { get; set; }
        public virtual DbSet<Maddeler> Maddelers { get; set; }
        public virtual DbSet<Mesajlar> Mesajlars { get; set; }
        public virtual DbSet<Moduller> Modullers { get; set; }
        public virtual DbSet<OkunanUniteler> OkunanUnitelers { get; set; }
        public virtual DbSet<Personel> Personels { get; set; }
        public virtual DbSet<Personel_Sinavlar> Personel_Sinavlar { get; set; }
        public virtual DbSet<Personel_Testler> Personel_Testler { get; set; }
        public virtual DbSet<Personel_TIP> Personel_TIP { get; set; }
        public virtual DbSet<RBagajlar> RBagajlars { get; set; }
        public virtual DbSet<SBagajKategori> SBagajKategoris { get; set; }
        public virtual DbSet<SBagajlar> SBagajlars { get; set; }
        public virtual DbSet<SBMaddeResimler> SBMaddeResimlers { get; set; }
        public virtual DbSet<SBTehlikeliMKategori> SBTehlikeliMKategoris { get; set; }
        public virtual DbSet<Simulator_Kategori> Simulator_Kategori { get; set; }
        public virtual DbSet<SimulatorLog> SimulatorLogs { get; set; }
        public virtual DbSet<Sinav_Sorular> Sinav_Sorular { get; set; }
        public virtual DbSet<SinavCevaplar> SinavCevaplars { get; set; }
        public virtual DbSet<Sinavlar> Sinavlars { get; set; }
        public virtual DbSet<SinavTipi> SinavTipis { get; set; }
        public virtual DbSet<Sinif> Sinifs { get; set; }
        public virtual DbSet<Sinif_Ogrenci> Sinif_Ogrenci { get; set; }
        public virtual DbSet<SoruKategori> SoruKategoris { get; set; }
        public virtual DbSet<SoruSiklari> SoruSiklaris { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<TehlikeliMaddeKategori> TehlikeliMaddeKategoris { get; set; }
        public virtual DbSet<Test_Sorular> Test_Sorular { get; set; }
        public virtual DbSet<TestCevaplar> TestCevaplars { get; set; }
        public virtual DbSet<Testler> Testlers { get; set; }
        public virtual DbSet<Tip_Sorular> Tip_Sorular { get; set; }
        public virtual DbSet<TipCevaplar> TipCevaplars { get; set; }
        public virtual DbSet<TipEgitim_Sorular> TipEgitim_Sorular { get; set; }
        public virtual DbSet<TipEgitimSinavlar> TipEgitimSinavlars { get; set; }
        public virtual DbSet<TipSinavlar> TipSinavlars { get; set; }
        public virtual DbSet<Unite> Unites { get; set; }
        public virtual DbSet<UniteModul> UniteModuls { get; set; }
        public virtual DbSet<UniteSorular> UniteSorulars { get; set; }
    
        public virtual ObjectResult<PersonelToplamSinavSoruMaddeDagilim_Result> PersonelToplamSinavSoruMaddeDagilim(Nullable<int> personel)
        {
            var personelParameter = personel.HasValue ?
                new ObjectParameter("personel", personel) :
                new ObjectParameter("personel", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<PersonelToplamSinavSoruMaddeDagilim_Result>("PersonelToplamSinavSoruMaddeDagilim", personelParameter);
        }
    
        public virtual ObjectResult<PersonelToplamSinavSoruMaddeDagilimDogruAdet_Result> PersonelToplamSinavSoruMaddeDagilimDogruAdet(Nullable<int> personel)
        {
            var personelParameter = personel.HasValue ?
                new ObjectParameter("personel", personel) :
                new ObjectParameter("personel", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<PersonelToplamSinavSoruMaddeDagilimDogruAdet_Result>("PersonelToplamSinavSoruMaddeDagilimDogruAdet", personelParameter);
        }
    
        public virtual ObjectResult<PersonelToplamTipSoruMaddeDagilim_Result> PersonelToplamTipSoruMaddeDagilim(Nullable<int> personel)
        {
            var personelParameter = personel.HasValue ?
                new ObjectParameter("personel", personel) :
                new ObjectParameter("personel", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<PersonelToplamTipSoruMaddeDagilim_Result>("PersonelToplamTipSoruMaddeDagilim", personelParameter);
        }
    
        public virtual ObjectResult<PersonelToplamTipSoruMaddeDagilimDogruAdet_Result> PersonelToplamTipSoruMaddeDagilimDogruAdet(Nullable<int> personel)
        {
            var personelParameter = personel.HasValue ?
                new ObjectParameter("personel", personel) :
                new ObjectParameter("personel", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<PersonelToplamTipSoruMaddeDagilimDogruAdet_Result>("PersonelToplamTipSoruMaddeDagilimDogruAdet", personelParameter);
        }
    
        public virtual ObjectResult<sp_EgitimSinifOgrenciGenel_Result> sp_EgitimSinifOgrenciGenel()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_EgitimSinifOgrenciGenel_Result>("sp_EgitimSinifOgrenciGenel");
        }
    
        public virtual ObjectResult<sp_GenelPersonelDurumDegerlendirme_Result> sp_GenelPersonelDurumDegerlendirme()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GenelPersonelDurumDegerlendirme_Result>("sp_GenelPersonelDurumDegerlendirme");
        }
    
        public virtual ObjectResult<sp_GenelPersonelDurumDegerlendirmeFirma_Result> sp_GenelPersonelDurumDegerlendirmeFirma(Nullable<int> firmaRef, string ad, string soyad, Nullable<System.DateTime> baslangictarihi, Nullable<System.DateTime> bitistarihi)
        {
            var firmaRefParameter = firmaRef.HasValue ?
                new ObjectParameter("firmaRef", firmaRef) :
                new ObjectParameter("firmaRef", typeof(int));
    
            var adParameter = ad != null ?
                new ObjectParameter("ad", ad) :
                new ObjectParameter("ad", typeof(string));
    
            var soyadParameter = soyad != null ?
                new ObjectParameter("soyad", soyad) :
                new ObjectParameter("soyad", typeof(string));
    
            var baslangictarihiParameter = baslangictarihi.HasValue ?
                new ObjectParameter("baslangictarihi", baslangictarihi) :
                new ObjectParameter("baslangictarihi", typeof(System.DateTime));
    
            var bitistarihiParameter = bitistarihi.HasValue ?
                new ObjectParameter("bitistarihi", bitistarihi) :
                new ObjectParameter("bitistarihi", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GenelPersonelDurumDegerlendirmeFirma_Result>("sp_GenelPersonelDurumDegerlendirmeFirma", firmaRefParameter, adParameter, soyadParameter, baslangictarihiParameter, bitistarihiParameter);
        }
    
        public virtual ObjectResult<Nullable<System.TimeSpan>> sp_GenelToplamSurecler(Nullable<int> idx)
        {
            var idxParameter = idx.HasValue ?
                new ObjectParameter("idx", idx) :
                new ObjectParameter("idx", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<System.TimeSpan>>("sp_GenelToplamSurecler", idxParameter);
        }
    
        public virtual ObjectResult<sp_Personel_Result> sp_Personel(Nullable<int> idx)
        {
            var idxParameter = idx.HasValue ?
                new ObjectParameter("idx", idx) :
                new ObjectParameter("idx", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_Personel_Result>("sp_Personel", idxParameter);
        }
    
        public virtual ObjectResult<sp_Personel_TIP_Result> sp_Personel_TIP(Nullable<int> idx)
        {
            var idxParameter = idx.HasValue ?
                new ObjectParameter("idx", idx) :
                new ObjectParameter("idx", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_Personel_TIP_Result>("sp_Personel_TIP", idxParameter);
        }
    
        public virtual ObjectResult<sp_PersonelSinavlar_Result> sp_PersonelSinavlar(Nullable<int> idx)
        {
            var idxParameter = idx.HasValue ?
                new ObjectParameter("idx", idx) :
                new ObjectParameter("idx", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_PersonelSinavlar_Result>("sp_PersonelSinavlar", idxParameter);
        }
    
        public virtual ObjectResult<sp_PersonelTestler_Result> sp_PersonelTestler(Nullable<int> idx)
        {
            var idxParameter = idx.HasValue ?
                new ObjectParameter("idx", idx) :
                new ObjectParameter("idx", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_PersonelTestler_Result>("sp_PersonelTestler", idxParameter);
        }
    
        public virtual ObjectResult<sp_SinifToplamSurecler_Result> sp_SinifToplamSurecler(Nullable<int> idx, string sinifAdi)
        {
            var idxParameter = idx.HasValue ?
                new ObjectParameter("idx", idx) :
                new ObjectParameter("idx", typeof(int));
    
            var sinifAdiParameter = sinifAdi != null ?
                new ObjectParameter("sinifAdi", sinifAdi) :
                new ObjectParameter("sinifAdi", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_SinifToplamSurecler_Result>("sp_SinifToplamSurecler", idxParameter, sinifAdiParameter);
        }
    
        public virtual ObjectResult<sp_ToplamSurecler_Result> sp_ToplamSurecler(Nullable<int> idx)
        {
            var idxParameter = idx.HasValue ?
                new ObjectParameter("idx", idx) :
                new ObjectParameter("idx", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_ToplamSurecler_Result>("sp_ToplamSurecler", idxParameter);
        }
    
        public virtual ObjectResult<sp_UniteSorular_Result> sp_UniteSorular(Nullable<int> unitID, Nullable<int> stipiRef)
        {
            var unitIDParameter = unitID.HasValue ?
                new ObjectParameter("unitID", unitID) :
                new ObjectParameter("unitID", typeof(int));
    
            var stipiRefParameter = stipiRef.HasValue ?
                new ObjectParameter("stipiRef", stipiRef) :
                new ObjectParameter("stipiRef", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_UniteSorular_Result>("sp_UniteSorular", unitIDParameter, stipiRefParameter);
        }
    
        public virtual ObjectResult<sp_GenelPersonelDurumDegerlendirmeFirmaListe_Result> sp_GenelPersonelDurumDegerlendirmeFirmaListe(Nullable<int> firmaRef, string ad, string soyad, Nullable<System.DateTime> baslangictarihi, Nullable<System.DateTime> bitistarihi)
        {
            var firmaRefParameter = firmaRef.HasValue ?
                new ObjectParameter("firmaRef", firmaRef) :
                new ObjectParameter("firmaRef", typeof(int));
    
            var adParameter = ad != null ?
                new ObjectParameter("ad", ad) :
                new ObjectParameter("ad", typeof(string));
    
            var soyadParameter = soyad != null ?
                new ObjectParameter("soyad", soyad) :
                new ObjectParameter("soyad", typeof(string));
    
            var baslangictarihiParameter = baslangictarihi.HasValue ?
                new ObjectParameter("baslangictarihi", baslangictarihi) :
                new ObjectParameter("baslangictarihi", typeof(System.DateTime));
    
            var bitistarihiParameter = bitistarihi.HasValue ?
                new ObjectParameter("bitistarihi", bitistarihi) :
                new ObjectParameter("bitistarihi", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GenelPersonelDurumDegerlendirmeFirmaListe_Result>("sp_GenelPersonelDurumDegerlendirmeFirmaListe", firmaRefParameter, adParameter, soyadParameter, baslangictarihiParameter, bitistarihiParameter);
        }
    
        public virtual ObjectResult<sp_EgitimBolumKonuDagilimlari_Result> sp_EgitimBolumKonuDagilimlari(Nullable<int> sinif_Ogrenci_Ref)
        {
            var sinif_Ogrenci_RefParameter = sinif_Ogrenci_Ref.HasValue ?
                new ObjectParameter("Sinif_Ogrenci_Ref", sinif_Ogrenci_Ref) :
                new ObjectParameter("Sinif_Ogrenci_Ref", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_EgitimBolumKonuDagilimlari_Result>("sp_EgitimBolumKonuDagilimlari", sinif_Ogrenci_RefParameter);
        }
    
        public virtual ObjectResult<sp_EgitimCalimaKontrol_Result> sp_EgitimCalimaKontrol(Nullable<int> sinif_Ogrenci_Ref)
        {
            var sinif_Ogrenci_RefParameter = sinif_Ogrenci_Ref.HasValue ?
                new ObjectParameter("Sinif_Ogrenci_Ref", sinif_Ogrenci_Ref) :
                new ObjectParameter("Sinif_Ogrenci_Ref", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_EgitimCalimaKontrol_Result>("sp_EgitimCalimaKontrol", sinif_Ogrenci_RefParameter);
        }
    
        [DbFunction("HaveakTurEntities", "fnStringList2Int")]
        public virtual IQueryable<Nullable<int>> fnStringList2Int(string list)
        {
            var listParameter = list != null ?
                new ObjectParameter("List", list) :
                new ObjectParameter("List", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.CreateQuery<Nullable<int>>("[HaveakTurEntities].[fnStringList2Int](@List)", listParameter);
        }
    
        public virtual ObjectResult<sp_GenelSinifPersonelDurumDegerlendirme_Result> sp_GenelSinifPersonelDurumDegerlendirme(string param)
        {
            var paramParameter = param != null ?
                new ObjectParameter("param", param) :
                new ObjectParameter("param", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GenelSinifPersonelDurumDegerlendirme_Result>("sp_GenelSinifPersonelDurumDegerlendirme", paramParameter);
        }
    
        public virtual ObjectResult<sp_GenelSinifDegerlendirme_Result> sp_GenelSinifDegerlendirme(Nullable<int> firmaRef, string ad, string soyad, Nullable<System.DateTime> baslangictarihi, Nullable<System.DateTime> bitistarihi)
        {
            var firmaRefParameter = firmaRef.HasValue ?
                new ObjectParameter("firmaRef", firmaRef) :
                new ObjectParameter("firmaRef", typeof(int));
    
            var adParameter = ad != null ?
                new ObjectParameter("ad", ad) :
                new ObjectParameter("ad", typeof(string));
    
            var soyadParameter = soyad != null ?
                new ObjectParameter("soyad", soyad) :
                new ObjectParameter("soyad", typeof(string));
    
            var baslangictarihiParameter = baslangictarihi.HasValue ?
                new ObjectParameter("baslangictarihi", baslangictarihi) :
                new ObjectParameter("baslangictarihi", typeof(System.DateTime));
    
            var bitistarihiParameter = bitistarihi.HasValue ?
                new ObjectParameter("bitistarihi", bitistarihi) :
                new ObjectParameter("bitistarihi", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GenelSinifDegerlendirme_Result>("sp_GenelSinifDegerlendirme", firmaRefParameter, adParameter, soyadParameter, baslangictarihiParameter, bitistarihiParameter);
        }
    }
}
