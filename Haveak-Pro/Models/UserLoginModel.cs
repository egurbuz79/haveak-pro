﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Haveak_Pro.Models
{
    [System.Web.Mvc.OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class UserLoginModel : IDisposable
    {
        public Kullanici User { get; set; }
        public ICollection<Moduller> UserModule { get; set; }
        public ICollection<Kull_Rolleri> UserRole { get; set; }
        public Personel Employee { get; set; }       
       
        public UserLoginModel(int UserRef)
        {

            HaveakTurEntities Entites = new HaveakTurEntities();
            var data = Entites.Kullanicis.Where(p => p.ID == UserRef && p.IsActive == true && p.IsDelete == false).FirstOrDefault();
            if (data != null)
            {
                User = data;
                UserModule = data.Kull_Rolleri.Select(s => s.Kull_Rol_Adi.Moduller).Distinct().ToList();
                UserRole = data.Kull_Rolleri.Where(t=>t.IsDelete == false && t.IsActive == true).ToList();
                Employee = data.Personel;
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}