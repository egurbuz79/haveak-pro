﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Haveak_Pro.Models
{
    public abstract class UserLog : IDisposable
    {
        public static void Logwrite(LogDetail detail)
        {
            using (HaveakTurEntities db = new HaveakTurEntities())
            {
                int ogrenci_ref = db.Sinif_Ogrenci.OrderByDescending(r=>r.ID).Where(w => w.Personel_Ref == detail.user_Ref && w.Bitti == false).OrderByDescending(w => w.ID).Select(q => q.ID).FirstOrDefault();
                var logIcerik = db.SimulatorLogs.Where(r => r.Sinif_Ogrenci_Ref == ogrenci_ref && r.Bitti == null).FirstOrDefault();
                SimulatorLog logdata = new SimulatorLog();
                logdata.Sinif_Ogrenci_Ref = ogrenci_ref;
                logdata.Simulator_Kategori_Ref = detail.Simulator_Kategori_Ref;
                if (logIcerik == null && detail.IslemTuru == Convert.ToInt32(IslemTuru.Giris_Islemi))
                {
                    logdata.GirisZamani = DateTime.Now;
                    logdata.Basladi = Convert.ToInt32(IslemTuru.Giris_Islemi);
                    db.SimulatorLogs.Add(logdata);
                }
                else if (detail.IslemTuru == Convert.ToInt32(IslemTuru.Cikis_Islemi))
                {
                    logIcerik.CikisZamani = DateTime.Now;
                    logIcerik.Bitti = Convert.ToInt32(IslemTuru.Cikis_Islemi);                    
                   
                }                
                else
                {
                    logIcerik.CikisZamani = logIcerik.GirisZamani.AddMinutes(1);
                    logIcerik.Bitti = Convert.ToInt32(IslemTuru.Cikis_Islemi);                    
                  
                    SimulatorLog logdatayeni = new SimulatorLog();
                    logdatayeni.Sinif_Ogrenci_Ref = ogrenci_ref;
                    logdatayeni.GirisZamani = DateTime.Now;
                    logdatayeni.Basladi = Convert.ToInt32(IslemTuru.Giris_Islemi);
                    logdatayeni.Simulator_Kategori_Ref = detail.Simulator_Kategori_Ref;
                    db.SimulatorLogs.Add(logdatayeni);
                }
                db.SaveChanges();
            }
        }
                
        public static void LogBaggage(int userRef)
        {
            using (HaveakTurEntities db = new HaveakTurEntities())
            {
                int ogrenci_ref = db.Sinif_Ogrenci.OrderByDescending(r=>r.ID).Where(w => w.Personel_Ref == userRef && w.Bitti == false).Select(q => q.ID).FirstOrDefault();
                var logIcerik = db.SimulatorLogs.Where(r => r.Sinif_Ogrenci_Ref == ogrenci_ref && r.Bitti == null).FirstOrDefault();
                int bagajAdet = 0;
                if (logIcerik != null)
                {
                    bagajAdet = logIcerik.IncelenenBagaj == null ? 0 : logIcerik.IncelenenBagaj.Value;
                    logIcerik.IncelenenBagaj = bagajAdet + 1;
                    db.SaveChanges();
                }
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }

    public sealed class LogDetail : IDisposable
    {
        public int user_Ref { get; set; }
        public int Simulator_Kategori_Ref { get; set; }
        public int IslemTuru { get; set; }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }

    public enum IslemTuru : int
    {
        Giris_Islemi = 1,
        Cikis_Islemi = 2
    }
}