//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Haveak_Pro.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class UniteSorular
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public UniteSorular()
        {
            this.AltSiklars = new HashSet<AltSiklar>();
            this.Sinav_Sorular = new HashSet<Sinav_Sorular>();
            this.SoruSiklaris = new HashSet<SoruSiklari>();
            this.Test_Sorular = new HashSet<Test_Sorular>();
            this.Tip_Sorular = new HashSet<Tip_Sorular>();
            this.TipEgitim_Sorular = new HashSet<TipEgitim_Sorular>();
        }
    
        public int ID { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public Nullable<int> SBagajlar_Ref { get; set; }
        public Nullable<int> STM_Ref { get; set; }
        public string Soru { get; set; }
        public Nullable<int> Unite_Ref { get; set; }
        public Nullable<decimal> XKordinat { get; set; }
        public Nullable<decimal> YKordinat { get; set; }
        public int SinavTipi_Ref { get; set; }
        public Nullable<decimal> TMOlcek { get; set; }
        public Nullable<decimal> TMSaydamlik { get; set; }
        public Nullable<decimal> TMVektor { get; set; }
        public Nullable<decimal> TMIsik { get; set; }
        public Nullable<int> SoruKategori_Ref { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AltSiklar> AltSiklars { get; set; }
        public virtual SBagajlar SBagajlar { get; set; }
        public virtual SBMaddeResimler SBMaddeResimler { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Sinav_Sorular> Sinav_Sorular { get; set; }
        public virtual SinavTipi SinavTipi { get; set; }
        public virtual SoruKategori SoruKategori { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SoruSiklari> SoruSiklaris { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Test_Sorular> Test_Sorular { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tip_Sorular> Tip_Sorular { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TipEgitim_Sorular> TipEgitim_Sorular { get; set; }
        public virtual Unite Unite { get; set; }
    }
}
