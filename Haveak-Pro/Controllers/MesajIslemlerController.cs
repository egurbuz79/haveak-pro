﻿using Haveak_Pro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Haveak_Pro.Controllers
{

    public class MesajIslemlerController : Controller
    {
        //
        // GET: /Panel/MesajIslemler/
        KimlikIdentity kullaniciBilgi;
        HaveakTurEntities Entity = new HaveakTurEntities();

        public MesajIslemlerController()
        {
            kullaniciBilgi = AktifKullanici.Aktif;
        }


        public ActionResult Mesajlar()
        {
            DateTime bugun = DateTime.Now.Date;
            int? firmaId = kullaniciBilgi.FirmaId;
            IQueryable<Kullanici> mesajlar = null;
            if (!UserRoleName.ADMIN_Egitim.InRole() && !UserRoleName.PANEL_AdminGiris.InRole())
            {
                mesajlar = Entity.Kullanicis.Where(r => r.Kull_Rolleri.Any(t => t.Kull_Rol_Adi.Name == UserRoleName.PANEL_AdminGiris || t.Kull_Rol_Adi.Name == UserRoleName.ADMIN_Egitim) && r.Personel_Ref != kullaniciBilgi.User_Ref && r.IsActive);

                //mesajlar = Entity.Kull_Rolleri.Where(t => (t.Kull_Rol_Adi.Name == UserRoleName.PANEL_AdminGiris || t.Kull_Rol_Adi.Name == UserRoleName.ADMIN_Egitim) && t.Kullanici.Personel_Ref != kullaniciBilgi.User_Ref && t.Kullanici.Personel.IsActive == true).Select(e => e.Kullanici);
            }
            else
            {
                mesajlar = Entity.Kullanicis.Where(r => r.Kull_Rolleri.Any(t => t.Kull_Rol_Adi.Name != UserRoleName.PANEL_AdminGiris && t.Kull_Rol_Adi.Name != UserRoleName.ADMIN_Egitim) && r.Personel_Ref != kullaniciBilgi.User_Ref && r.IsActive);

                // mesajlar = Entity.Mesajlars.Where(t => t.IsActive == true && t.IsDelete == false && t.Alici_Ref == kullaniciBilgi.User_Ref && t.Personel.IsActive == true).Select(w => w.Personel.Kullanicis.FirstOrDefault()).Distinct();
            }
            if (kullaniciBilgi.FirmaId != null)
            {
                mesajlar = mesajlar.Where(r => r.Personel.Firma_Ref == firmaId);
            }

            ViewBag.Bugungelenler = Entity.Mesajlars.Where(t => t.IsActive == true && t.IsDelete == false && t.Alici_Ref == kullaniciBilgi.User_Ref && t.Tarih==bugun).Select(w => w.Personel.ID).Distinct().ToList();

            ViewBag.MesajKisiler = mesajlar.Select(r => new KullaniciPersonel
            {
                ID = r.Personel_Ref,
                Adi = r.Personel.Adi,
                Soyadi = r.Personel.Soyadi,
                Resim = r.Personel.Resim
            }).ToList();
            return View();
        }

        public ActionResult _MesajIcerikGnl(int aliciRef)
        {
            int gonderen = kullaniciBilgi.User_Ref;
            var data = Entity.Mesajlars.OrderByDescending(y => y.ID).Where(t => t.IsActive == true && t.IsDelete == false && (t.Gonderen_Ref == gonderen || t.Alici_Ref == gonderen)).ToList();
            ViewBag.AliciRef = aliciRef;
            return PartialView("_MesajDetayGenel", data);
        }


        public ActionResult UpdateMesaj(int id, bool IsActive, bool IsDelete)
        {
            bool result = false;
            try
            {
                var data = Entity.Mesajlars.Where(r => r.ID == id).FirstOrDefault();
                data.IsActive = IsActive;
                data.IsDelete = IsDelete;
                Entity.SaveChanges();
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            Entity.Dispose();
            base.Dispose(disposing);
        }
    }

    public class KullaniciPersonel
    {
        public int ID { get; set; }
        public string Resim { get; set; }
        public string Adi { get; set; }
        public string Soyadi { get; set; }
    }
}
