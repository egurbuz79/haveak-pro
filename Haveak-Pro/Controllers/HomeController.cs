﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Haveak_Pro.Models;

namespace Haveak_Pro.Controllers
{
    public class HomeController : Controller
    {

        HaveakTurEntities Entity = new HaveakTurEntities();
        public ActionResult Index()
        {
            try
            {
                var kimlik = AktifKullanici.Aktif.User_Ref;
                int? firmaId = AktifKullanici.Aktif.FirmaId;
                if (kimlik != 0)
                {
                    var userRole = AktifKullanici.Aktif.Roller;
                    if (userRole.Contains("PANEL/AdminGirisi"))
                    {
                        ViewBag.MesajIcerik = Entity.Mesajlars.Where(t => t.Alici_Ref == kimlik && t.IsActive == true && t.IsDelete == false).ToList();
                    }
                    else
                    {
                        ViewBag.MesajIcerik = Entity.Mesajlars.Where(r => r.Alici_Ref == kimlik && r.IsActive == true && r.IsDelete == false && r.Personel.Firma_Ref == firmaId).ToList();
                    }
                    TimeSpan zaman = DateTime.Now.TimeOfDay;
                    DateTime bugun = DateTime.Now.Date;
                    ViewBag.AdminDetay = Entity.Kullanicis.Where(t => t.Kull_Rolleri.Any(m => m.User_Role_Name_Ref == 1)).ToList();
                    ViewBag.MesajBaslik = (from d in Entity.Mesajlars group d by new { d.Personel.ID, d.Personel.Adi, d.Personel.Soyadi } into grp select new ProjectMessage { ID = grp.Key.ID, Ad = grp.Key.Adi, Soyad = grp.Key.Soyadi }).ToList();
                    IQueryable<Duyurular> qduyuru = Entity.Duyurulars.Include("Firmalar");
                    
                    List<Duyurular> duyuruData = null;
                    if (firmaId != null)
                    {
                        if (UserRoleName.PANEL_AdminGiris.InRole())
                        {
                            duyuruData = qduyuru.Where(t => t.IsDelete == false && t.IsActive == true).ToList();
                        }
                        else
                        {
                            duyuruData = qduyuru.Where(t => t.IsDelete == false && t.IsActive == true && (t.Firma_Ref == firmaId || t.Firma_Ref == null)).ToList();                           
                        }                        
                    }
                    else
                    {
                        duyuruData = qduyuru.Where(t => t.IsDelete == false && t.IsActive == true).ToList();
                    }
                    ViewBag.Duyurular = duyuruData;
                    ViewBag.Sinavlar = Entity.Personel_Sinavlar.Where(t => t.IsActive == true && t.IsDelete == false && t.Sinif_Ogrenci.Personel_Ref == kimlik && t.SinavTarihi.Value >= bugun && t.BitisSaati >= zaman).ToList();
                    ViewBag.SinavAciklama = Entity.Personel_Sinavlar.Where(t => t.IsActive == true && t.IsDelete == false && t.Sinif_Ogrenci.Personel_Ref == kimlik && t.DuyuruTarihi.Value == bugun).ToList();
                    IQueryable<Kullanici> mesajlar = null;
                    if (!UserRoleName.ADMIN_Egitim.InRole() && !UserRoleName.PANEL_AdminGiris.InRole())
                    {
                        mesajlar = Entity.Kullanicis.Where(r => r.Kull_Rolleri.Any(t => t.Kull_Rol_Adi.Name == UserRoleName.PANEL_AdminGiris || t.Kull_Rol_Adi.Name == UserRoleName.ADMIN_Egitim) && r.Personel_Ref != kimlik && r.IsActive);
                    }
                    else
                    {
                        mesajlar = Entity.Kullanicis.Where(r => r.Kull_Rolleri.Any(t => t.Kull_Rol_Adi.Name != UserRoleName.PANEL_AdminGiris && t.Kull_Rol_Adi.Name != UserRoleName.ADMIN_Egitim) && r.Personel_Ref != kimlik && r.IsActive);
                    }
                    if (firmaId != null)
                    {
                        mesajlar = mesajlar.Where(r => r.Personel.Firma_Ref == firmaId);
                    }

                    ViewBag.MesajKisiler = mesajlar.Select(r => new KullaniciPersonel
                    {
                        ID = r.Personel_Ref,
                        Adi = r.Personel.Adi,
                        Soyadi = r.Personel.Soyadi,
                        Resim = r.Personel.Resim
                    }).ToList();

                    //if (!UserRoleName.ADMIN_Egitim.InRole())
                    //{
                    //    ViewBag.Adminler = Entity.Kull_Rolleri.Where(t => t.Kull_Rol_Adi.Name == UserRoleName.ADMIN_Egitim && t.Kullanici.Personel_Ref != kimlik && t.Kullanici.Personel.Firma_Ref == AktifKullanici.Aktif.FirmaId).Select(e => e.Kullanici).ToList();
                    //}
                    //else
                    //{
                    //    ViewBag.Adminler = Entity.Mesajlars.Where(t => t.IsActive == true && t.IsDelete == false && t.Alici_Ref == kimlik && t.Tarih == bugun && t.Personel.Firma_Ref == AktifKullanici.Aktif.FirmaId).Select(w => w.Personel.Kullanicis.FirstOrDefault()).Distinct().ToList();
                    //}

                }
            }
            catch
            {
                RedirectToAction("Logoff", "Kontrol", new { area = "" });
            }
            return View();
        }

        public JsonResult VitrinMesajlar()
        {
            List<ProjectMessage> data = new List<ProjectMessage>();
            try
            {
                DateTime bugun = DateTime.Now.Date;
                var kimlik = AktifKullanici.Aktif.User_Ref;
                data = Entity.Mesajlars.OrderByDescending(y => y.ID).Where(t => t.Alici_Ref == kimlik && t.Tarih == bugun).Select(t => new ProjectMessage
                {
                    Ad = t.Personel.Adi,
                    Soyad = t.Personel.Soyadi,
                    Resim = (t.Personel.Resim ?? "RYok.jpg"),
                    Icerik = t.Icerik
                }).Take(1).ToList();
            }
            catch
            {
                RedirectToAction("Logoff", "Account", new { area = "" });
            }
            return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Mesajlar()
        {
            var kimlik = AktifKullanici.Aktif.User_Ref;
            if (kimlik != 0)
            {
                var userRole = AktifKullanici.Aktif.Roller;
                if (userRole.Contains("PANEL/AdminGirisi"))
                {
                    ViewBag.MesajIcerik = Entity.Mesajlars.Where(t => t.IsActive == true && t.IsDelete == false).ToList();
                }
                else
                {
                    ViewBag.MesajIcerik = Entity.Mesajlars.Where(r => r.Gonderen_Ref == kimlik && r.IsActive == true && r.IsDelete == false).ToList();
                }
                ViewBag.AdminDetay = Entity.Kullanicis.Where(t => t.Kull_Rolleri.Any(m => m.User_Role_Name_Ref == 1)).ToList();
                ViewBag.MesajBaslik = (from d in Entity.Mesajlars group d by new { d.Personel.ID, d.Personel.Adi, d.Personel.Soyadi } into grp select new ProjectMessage { ID = grp.Key.ID, Ad = grp.Key.Adi, Soyad = grp.Key.Soyadi }).ToList();
            }
            var data2 = Entity.Mesajlars.Where(t => t.IsActive == true && t.IsDelete == false).Select(y =>
                               new { y.ID, y.Alici_Ref, y.Icerik, y.Personel.Adi, y.Personel.Resim, y.Personel.Soyadi, y.Tarih }).ToList();

            return Json(data2, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public ActionResult _MesajIcerikVt(int aliciRef)
        {
            int gonderen = AktifKullanici.Aktif.User_Ref;
            DateTime bugun = DateTime.Now.Date;
            var data = Entity.Mesajlars.OrderByDescending(y => y.ID).Where(t => t.IsActive == true && t.IsDelete == false && (t.Gonderen_Ref == gonderen || t.Alici_Ref == gonderen) && t.Tarih == bugun).ToList();
            ViewBag.AliciRef = aliciRef;
            return PartialView("_MesajDetay", data);
        }

        public JsonResult MesajGonder(int aliciRef, string mesaj)
        {
            bool durum = false;
            try
            {
                int gonderenRef = AktifKullanici.Aktif.User_Ref;
                DateTime tarih = DateTime.Now.Date;
                TimeSpan saat = DateTime.Now.TimeOfDay;

                Mesajlar msj = new Models.Mesajlar();
                msj.IsActive = true;
                msj.IsDelete = false;
                msj.Gonderen_Ref = gonderenRef;
                msj.Alici_Ref = aliciRef;
                msj.Tarih = tarih;
                msj.Icerik = mesaj;
                msj.Saat = saat;
                Entity.Mesajlars.Add(msj);
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);

        }

        protected override void Dispose(bool disposing)
        {
            Entity.Dispose();
            base.Dispose(disposing);
        }
    }


    public class ProjectMessage
    {
        public int ID { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Resim { get; set; }
        public string Icerik { get; set; }
    }
}
