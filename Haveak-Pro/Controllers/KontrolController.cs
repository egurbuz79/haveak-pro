﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Haveak_Pro.Models;
using CaptchaMvc.HtmlHelpers;
using System.Drawing;
using System.IO;
using System.Drawing.Text;
using System.Drawing.Drawing2D;

namespace Haveak_Pro.Controllers
{
    [System.Web.Mvc.OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class KontrolController : Controller
    {

        //
        // GET: /Account/
        HaveakTurEntities db = new HaveakTurEntities();
        public ActionResult Giris(int? id)
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            return View(id);
        }
        private DateTime bugun = DateTime.Now;
        private int gun = DateTime.Now.Day;
        private int ay = DateTime.Now.Month;
        private int yil = DateTime.Now.Year;

        public JsonResult SifreKontrol(string userName, string password)
        {
            int durum = 0;
            DateTime zaman = DateTime.Now.Date;
            Kullanici accountControl = db.Kullanicis.Where(t => t.IsActive == true && t.KullaniciAdi == userName && t.Sifre == password && t.BitisTarihi >= zaman).FirstOrDefault();
            if (accountControl != null)
            {
                if (accountControl.Personel.Firmalar.IsActive)
                {
                    durum = 2; // Kayıt Doğrulandı
                }
                else
                {
                    durum = -3; //Firma Aktif Değil
                }
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Giris(string userName, string password, string Secure)
        {
            try
            {
                if (userName != null)
                {
                    if (Session["Captcha"] == null)
                    {
                        return null;
                    }
                    int total = Convert.ToInt32(Session["Captcha"]);
                    if (Secure == "")
                    {
                        Secure = "0";
                    }
                    if (int.Parse(Secure) == total)
                    {
                        Kullanici accountControl = db.Kullanicis.Where(t => t.IsActive == true && t.KullaniciAdi == userName && t.Sifre == password && t.Personel.Firmalar.IsActive && t.Personel.IsDelete==false).FirstOrDefault();
                        if (accountControl != null)
                        {
                            //KullaniciLoglama(accountControl.ID);
                            HttpContext.Session.Add("sifre", password);
                            FormsAuthentication.RedirectFromLoginPage(accountControl.KullaniciAdi, true);
                        }
                    }
                    else
                    {
                        Logoff();
                    }
                }
            }
            catch
            {
                Logoff();
            }
            return View();
        }
        //public bool KullaniciLoglama(int kullaniciRef)
        //{           
        //    bool durum = false;
        //    try
        //    {
        //        Loglama logdetay = db.Loglamas.Where(w =>
        //            w.Kullanici_Ref == kullaniciRef &&
        //            w.GirisTarihi.Day == gun &&
        //            w.GirisTarihi.Month == ay &&
        //            w.GirisTarihi.Year == yil).FirstOrDefault();
        //        if (logdetay == null)
        //        {
        //            logdetay = new Loglama();
        //            logdetay.IsActive = true;
        //            logdetay.IsDelete = false;
        //            logdetay.Kullanici_Ref = kullaniciRef;
        //            logdetay.GirisTarihi = bugun;
        //            logdetay.YapilanIs = "Kullanicı Girişi";
        //            db.Loglamas.Add(logdetay);
        //            db.SaveChanges();
        //            durum = true;
        //        }
        //    }
        //    catch
        //    {
        //        durum = false;
        //    }
        //    return durum;
        //}
        public ActionResult Logoff()
        {
            int userRef = AktifKullanici.Aktif.Id;
            Loglama log = db.Loglamas.Where(w =>
                w.Kullanici_Ref == userRef &&
                w.GirisTarihi.Day == gun &&
                w.GirisTarihi.Month == ay &&
                w.GirisTarihi.Year == yil &&
                w.CikisSaati == null
                ).FirstOrDefault();
            if (log != null)
            {
                log.CikisSaati = bugun;
                TimeSpan fark = bugun - log.GirisTarihi;
                log.ToplamSure = fark;
                db.SaveChanges();
            }
            Session.Abandon();
            FormsAuthentication.SignOut();
            return RedirectToAction("Giris", "Kontrol");
        }

        public JsonResult SendMail(string adress)
        {
            bool durum = false;
            try
            {
                List<string> adres = new List<string>();
                adres.Add(adress);
                var data = db.Personels.Where(t => t.Email == adress).FirstOrDefault();
                string kisi = data.Adi + " " + data.Soyadi;

                string body = "Sayın <I>" + kisi + "; </I> <br />";
                body += "Proje Kullanıcı adı: <b>[" + data.Kullanicis.Select(t => t.KullaniciAdi).FirstOrDefault() + "] Şifre: [" + data.Kullanicis.Select(t => t.Sifre).FirstOrDefault() + "]</b> dir. <br />";
                body += "Sorun halinde temes kurunuz.";

                MailHelper hlp = new MailHelper();
                hlp.SenMail(adres, "Kullanıcı adı,Şifre Bilgisi", body);
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ResimGetir(string user)
        {
            ResimData data = db.Kullanicis.Where(t => t.IsDelete == false && t.IsActive == true && t.KullaniciAdi == user).Select(q => new ResimData { Resim = q.Personel.Resim, Adi = q.Personel.Adi, Soyadi = q.Personel.Soyadi }).FirstOrDefault();
            if (data == null)
            {
                data = new ResimData();
                data.Adi = "Kullanıcı";
                data.Resim = "RYok.jpg";
                data.Soyadi = "Bulunamadı!";
            }
            if (data.Resim == null)
            {
                data.Resim = "RYok.jpg";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }

     

        //public JsonResult ReadData()
        //{
        //    ManagementScope scope = new ManagementScope("\\\\" + Environment.MachineName + "\\root\\cimv2");

        //    scope.Connect();

        //    ManagementObject wmiClass = new ManagementObject(scope, new ManagementPath("Win32_BaseBoard.Tag=\"Base Board\""), new ObjectGetOptions());

        //    foreach (PropertyData propData in wmiClass.Properties)
        //    {
        //        SerialNumber = scope.Path.ToString();
        //    }
        //}
        public ActionResult CaptchaImage(string prefix, bool noisy = true)
        {
            var rand = new Random((int)DateTime.Now.Ticks);

            //yeni soru üret
            int a = rand.Next(10, 99);
            int b = rand.Next(0, 9);
            var captcha = string.Format("{0} + {1} = ?", a, b);

            //cevabı sakla
            Session["Captcha" + prefix] = a + b;

            //resim oluştur
            FileContentResult img = null;

            using (var mem = new MemoryStream())
            using (var bmp = new Bitmap(130, 30))
            using (var gfx = Graphics.FromImage((Image)bmp))
            {
                gfx.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                gfx.SmoothingMode = SmoothingMode.AntiAlias;
                gfx.FillRectangle(Brushes.White, new Rectangle(0, 0, bmp.Width, bmp.Height));

                //karmaşa ekle
                if (noisy)
                {
                    int i, r, x, y;
                    var pen = new Pen(Color.Yellow);
                    for (i = 1; i < 10; i++)
                    {
                        pen.Color = Color.FromArgb(
                        (rand.Next(0, 255)),
                        (rand.Next(0, 255)),
                        (rand.Next(0, 255)));

                        r = rand.Next(0, (130 / 3));
                        x = rand.Next(0, 130);
                        y = rand.Next(0, 30);

                        gfx.DrawEllipse(pen, x - r, y - r, r, r);
                    }
                }

                //soruyu ekle
                gfx.DrawString(captcha, new Font("Tahoma", 15), Brushes.Gray, 2, 3);

                //resim olarak çiz
                bmp.Save(mem, System.Drawing.Imaging.ImageFormat.Jpeg);
                img = this.File(mem.GetBuffer(), "image/Jpeg");
            }

            return img;
        }


        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
    public class ResimData : IDisposable
    {
        public string Resim { get; set; }
        public string Adi { get; set; }
        public string Soyadi { get; set; }


        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}