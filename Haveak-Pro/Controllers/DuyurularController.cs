﻿using Haveak_Pro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Haveak_Pro.Controllers
{
    public class DuyurularController : Controller
    {
        //
        // GET: /Duyurular/

        HaveakTurEntities Entity = new HaveakTurEntities();        
        public ActionResult Index()
        {
            int? firmaId = AktifKullanici.Aktif.FirmaId;
            IQueryable<Duyurular> duyurular = Entity.Duyurulars;
            List<Duyurular> data = null;
            if (firmaId != null)
            {
                data = duyurular.Where(t => t.IsDelete == false && t.IsActive == true && t.Firma_Ref == firmaId).ToList();
            }
            else
            {
                data = duyurular.Where(t => t.IsDelete == false && t.IsActive == true).ToList();
            }
            return View(data);
        }

        protected override void Dispose(bool disposing)
        {
            Entity.Dispose();
            base.Dispose(disposing);
        }
    }
}
