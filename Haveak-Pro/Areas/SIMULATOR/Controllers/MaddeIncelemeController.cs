﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Haveak_Pro.Models;

namespace Haveak_Pro.Areas.SIMULATOR.Controllers
{
    public class MaddeIncelemeController : Controller
    {
        //
        // GET: /SIMULATOR/MaddeInceleme/

        HaveakTurEntities Entity = new HaveakTurEntities();
        public ActionResult MADDEINCELEME()        
        {
            ViewBag.Kategori = Entity.TehlikeliMaddeKategoris.Where(r => r.IsActive == true && r.IsDelete == false).ToList();
            LogDetail dty = new LogDetail();
            dty.IslemTuru = 1;
            dty.Simulator_Kategori_Ref = 6;
            dty.user_Ref = AktifKullanici.Aktif.User_Ref;
        //    UserLog.Logwrite(dty);
            return View();
        }

        public JsonResult KategoriMaddeler(int id)
        {
            var data = Entity.Maddelers.OrderBy(s => Guid.NewGuid()).Where(t => t.IsActive == true && t.IsDelete == false && t.TehlikeliMadde_Ref == id)
                .Select(r => new { r.ID, r.ResimAd, r.Resim, AnaResim = r.TehlikeliMaddeKategori.Resim}).ToList();

            return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LogYaz(int islemTur, int simulatorRef)
        {
            LogDetail dtl = new LogDetail();
            dtl.user_Ref = AktifKullanici.Aktif.User_Ref;
            dtl.IslemTuru = islemTur;
            dtl.Simulator_Kategori_Ref = simulatorRef;
            UserLog.Logwrite(dtl);
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LogBagaj()
        {
            int userRef = AktifKullanici.Aktif.User_Ref;
            UserLog.LogBaggage(userRef);
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            Entity.Dispose();
            base.Dispose(disposing);
        }
    }
}
