﻿using Haveak_Pro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Haveak_Pro.Areas.SIMULATOR.Controllers
{
    public class BagajIncelemeController : Controller
    {
        //
        // GET: /SIMULATOR/BagajInceleme/
        public BagajIncelemeController()
        {
            AktifKullanici.Aktif.SecilenModul = "SIMULATOR";
        }
        HaveakTurEntities Entity = new HaveakTurEntities();
        public ActionResult HEIMANN()
        { 
            return View();
        }       

        public ActionResult NUCTECH()
        {
            ViewBag.Bagaj = "page-navigation-top";
            ViewBag.Menu = "x-navigation-horizontal";
            ViewBag.SideBar = "";
            return View();
        }

        public ActionResult NUCTECH_ONE()
        {
            return View();
        }

        public ActionResult RAPISCAN()
        {
            return View();
        }

        public JsonResult NuctechBagajActiveList()
        {
            var data = Entity.Bagajlars.OrderBy(r => Guid.NewGuid()).Where(t => t.IsActive == true && t.IsDelete == false /*&& t.Resim2 != null*/).Select(r => new { r.ID, r.Resim, r.Resim2 }).ToList();
            return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Nuctech_One_BagajActiveList()
        {
            var data = Entity.Bagajlars.OrderBy(r => Guid.NewGuid()).Where(t => t.IsActive == true && t.IsDelete == false).Select(r => new { r.ID, r.Resim }).ToList();
            return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }


        public JsonResult HiemanBagajActiveList()
        {
            var data = Entity.HBagajlars.OrderBy(r => Guid.NewGuid()).Where(t => t.IsActive == true && t.IsDelete == false).Select(r => new { r.ID, r.Resim }).ToList();
            return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RapiscanBagajActiveList()
        {
            var data = Entity.RBagajlars.OrderBy(r => Guid.NewGuid()).Where(t => t.IsActive == true && t.IsDelete == false).Select(r => new { r.ID, r.Resim }).ToList();
            return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public ActionResult NuctechBagajResimleriGetir(int id)
        {
            var data = Entity.Bagajlars.OrderBy(r => Guid.NewGuid()).Where(t => t.IsActive == true && t.IsDelete == false && t.ID == id /*&& t.Resim2 != null*/).Select(r => new { r.ID, r.Resim, r.Resim2 }).ToList();
            return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Nuctech_One_BagajResimleriGetir(int id)
        {
            var data = Entity.Bagajlars.OrderBy(r => Guid.NewGuid()).Where(t => t.IsActive == true && t.IsDelete == false && t.ID == id).Select(r => new { r.ID, r.Resim }).ToList();
            return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public ActionResult HiemanBagajResimleriGetir(int id)
        {
            var data = Entity.HBagajlars.OrderBy(r => Guid.NewGuid()).Where(t => t.IsActive == true && t.IsDelete == false && t.ID == id).Select(r => new { r.ID, r.Resim }).ToList();
            return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RapiscanBagajResimleriGetir(int id)
        {
            var data = Entity.RBagajlars.OrderBy(r => Guid.NewGuid()).Where(t => t.IsActive == true && t.IsDelete == false && t.ID == id).Select(r => new { r.ID, r.Resim }).ToList();
            return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LogYaz(int islemTur, int simulatorRef)
        {
            LogDetail dtl = new LogDetail();
            dtl.user_Ref = AktifKullanici.Aktif.User_Ref;
            dtl.IslemTuru = islemTur;
            dtl.Simulator_Kategori_Ref = simulatorRef;
            UserLog.Logwrite(dtl);
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LogBagaj()
        {         
            int userRef = AktifKullanici.Aktif.User_Ref;
            UserLog.LogBaggage(userRef);
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            Entity.Dispose();
            base.Dispose(disposing);
        }
    }
}
