﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Haveak_Pro.Models;


namespace Haveak_Pro.Areas.EGITIM.Controllers
{
    [System.Web.Mvc.OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    [Authorize(Roles = UserRoleName.EGITIM_OgrenciGirisi)]
    public class HomeController : Controller
    {
        //
        // GET: /Egitim/Home/
        HaveakTurEntities Entity = new HaveakTurEntities();

        public ActionResult Index()
        {
            int user_ref = AktifKullanici.Aktif.User_Ref;
            ViewBag.Aciklama = Entity.EgitimGirisSayfasis.FirstOrDefault();
            DateTime tarih = DateTime.Now.Date;
            var sinif_Ogrenci = Entity.Sinif_Ogrenci.Where(r => r.Basladi == true && r.Bitti == false && r.Personel_Ref == user_ref && r.BitisTarihi >= tarih).ToList();
            ViewBag.SinifOgrData = Entity.Sinif_Ogrenci.OrderByDescending(r=>r.BitisTarihi).Where(r => r.Basladi == true && r.Bitti == false && r.Personel_Ref == user_ref && r.BitisTarihi >= tarih).FirstOrDefault();
            return View(sinif_Ogrenci);
        }

        protected override void Dispose(bool disposing)
        {
            Entity.Dispose();
            base.Dispose(disposing);
        }

        public JsonResult OkunduBilgisi(int atananBolum_Ref, int unite_Ref)
        {
            bool durum = false;
            var kontrol = Entity.OkunanUnitelers.Where(r => r.Unite_Ref == unite_Ref && r.AtananBolumler_Ref == atananBolum_Ref).Count();
            if (kontrol > 0)
            {
                durum = true;
            }
            else
            {
                try
                {
                    OkunanUniteler unt = new OkunanUniteler();
                    unt.Unite_Ref = unite_Ref;
                    unt.AtananBolumler_Ref = atananBolum_Ref;
                    Entity.OkunanUnitelers.Add(unt);
                    Entity.SaveChanges();
                    durum = true;
                }
                catch
                {
                    durum = false;
                }
            }

            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

    }
}
