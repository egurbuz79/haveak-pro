﻿using Haveak_Pro.Areas.PANEL.Models;
using Haveak_Pro.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Haveak_Pro.Areas.EGITIM.Controllers
{
    [Authorize(Roles = UserRoleName.EGITIM_OgrenciGirisi)]
    public class IcerikController : Controller
    {
        //
        // GET: /EGITIM/Icerik/
         HaveakTurEntities Entity = new HaveakTurEntities();
        public ActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> BolumBasliklari(int id)
        {
            int kullaniciID = AktifKullanici.Aktif.User_Ref;
            
            var data = await Entity.Unites.OrderBy(t => t.Sira).Where(t => t.Modul_Ref == id && t.IsActive == true && t.IsDelete == false).Select(e => new
            {
                e.ID,
                e.UniteAdi,
                abRef = e.OkunanUnitelers.Where(r => r.AtananBolumler.Sinif_Ogrenci.Personel_Ref == kullaniciID).Select(q => q.AtananBolumler_Ref).Count(),                
                obref = e.OkunanUnitelers.Where(r => r.AtananBolumler.Sinif_Ogrenci.Personel_Ref == kullaniciID).Select(q => q.AtananBolumler_Ref).FirstOrDefault(),
                unite_modul_ref = e.Modul_Ref,
                aktifTestAdet = e.UniteModul.Testlers.Where(r => r.Personel_Testler.Any(t => t.Sinif_Ogrenci.Personel_Ref == kullaniciID)).Count()
            }).ToListAsync();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Konu(int id, int? atananblm)
        {           
            List<Egitim_Arsivi> Uniteler = Entity.Egitim_Arsivi.Where(r => r.IsActive == true && r.IsDelete == false && r.Unite_Ref == id).ToList();
            int? modulID = Entity.Unites.Where(t => t.ID == id && t.IsDelete == false).Select(r => r.Modul_Ref).FirstOrDefault();
            ViewBag.UniteID = Entity.Unites.OrderBy(e=>e.Sira).Where(t => t.Modul_Ref == modulID.Value && t.IsActive==true && t.IsDelete == false).Select(y => y.ID).ToList();
            //int atanaBolumRef = Entity.KursModullers.Where(e => e.).First();
            ViewBag.AtananBolum = atananblm ?? 0;
            ViewBag.baslamatar = DateTime.Now;
            return View(Uniteler);
        }

        public JsonResult EgitimLogAta(DateTime baslangic, DateTime bitis, int uniteId)
        {
            int perID = AktifKullanici.Aktif.User_Ref;
            int ogr_ref = Entity.Sinif_Ogrenci.OrderByDescending(q => q.ID).Where(t => t.Personel_Ref == perID && t.Bitti == false).Select(w => w.ID).FirstOrDefault();
            DateTime tarih = bitis.Date;
            TimeSpan giriszaman = baslangic.TimeOfDay;
            TimeSpan cikiszamani = bitis.TimeOfDay;
            EgitimLogHelper hlp = new EgitimLogHelper();
            bool sonuc = hlp.EgitimLog(ogr_ref, tarih, giriszaman, cikiszamani, uniteId);
            return Json(sonuc, JsonRequestBehavior.AllowGet);
        }
        //public JsonResult Testler(int modulRef)
        //{
        //    var data = Entity.Testlers.Where(t => t.UniteModul_Ref == modulRef && t.IsActive == true && t.IsDelete == false).Select(r => new { r.ID, r.TestNo, r.UniteModul_Ref}).ToList();
        //    return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        //}

    }
}
