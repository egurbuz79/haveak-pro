﻿using Haveak_Pro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Haveak_Pro.Areas.SINAV.Controllers
{
    public class TipEgitimTestController : Controller
    {
        //
        // GET: /SINAV/TipEgitimTest/
      
        HaveakTurEntities entity = new HaveakTurEntities();       
        public ActionResult TipEgitimTesteBasla(int id)
        {
            ViewBag.TipEgitimtestID = id;  
            List<TipEgitim_Sorular> data = entity.TipEgitim_Sorular.OrderBy(x => Guid.NewGuid()).Where(r => r.TipEgitSinavlar_Ref == id && r.IsActive == true && r.IsDelete == false).ToList();
            Session["Sorular"] = data;                    
            Session["SoruIceriklerListe"] = null;
            return View(data);
        }
        public JsonResult TipEgitimTestBitir()
        {
            Session["Sorular"] = null;
            Session["SoruIceriklerListe"] = null;
            Session["SoruIcerikler"] = null;
            return Json(true, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
       
        public ActionResult _TSoruDetay(int TipEgitim_Ref, int idx, int Kalansoru)
        {
            List<TipEgitim_Sorular> sorular = null;
            sorular = Session["Sorular"] as List<TipEgitim_Sorular>;
            int index = 0;         

            List<SoruDetaylar> SoruListe = new List<SoruDetaylar>();
            foreach (var item in sorular)
            {
                index++;
                int toplamSoru = sorular.Count;
                int kalan = toplamSoru - index;       
                SoruListe.Add(new SoruDetaylar{ Kalan = kalan, Sira= index, SoruNo = item.ID });
            }
            SoruDetaylar detaySoru = SoruListe.Where(r => r.SoruNo == idx).Select(w => new SoruDetaylar { Sira = w.Sira, SoruNo = w.SoruNo, Kalan = w.Kalan }).FirstOrDefault();
            int nextID = SoruListe.SkipWhile(i => i.SoruNo != idx)
                .Skip(1)
                .Select(i => i.SoruNo).FirstOrDefault();              
            ViewBag.SonrakiSoru = nextID;     
            Session["SoruIcerikler"] = detaySoru;
            if (Session["SoruIceriklerListe"] == null)
            {
                Session["SoruIceriklerListe"] = SoruListe;
            }
            ViewBag.SoruSonucListe = (List<SoruDetaylar>)Session["SoruIceriklerListe"];
            ViewBag.Kalansoru = Kalansoru;
            TipEgitim_Sorular data = sorular.Where(t => t.ID == idx).FirstOrDefault();
            ViewBag.entity = entity;
            return PartialView("_SoruIcerikEgitimTip", data);
        }

        public JsonResult SoruCevapla(int soruRef, string girilenCevap, bool? koordinatSecim)
        {            
            bool sonuc = false;
            string cevap = "Seçim Doğru";
            try
            {
                var soruDetay = entity.TipEgitim_Sorular.Where(t => t.ID == soruRef).FirstOrDefault();                
                var dogruCevap = soruDetay.UniteSorular.SoruSiklaris.Where(t => t.IsActive == true && t.IsDelete == false && t.DogruSecenek == true).Select(w => w.SoruSikki).FirstOrDefault();
                if (girilenCevap == "a)")
                {
                    if (dogruCevap == girilenCevap)
                    {
                        sonuc = true;
                    }
                    else
                    {
                        sonuc = false;
                        if (dogruCevap == "b)")
                        {
                            string maddeTuru = soruDetay.UniteSorular.SBMaddeResimler.SBTehlikeliMKategori.Isim;
                            cevap = "Yasaklı madde ('" + maddeTuru + "') bulunmaktadır!";
                        }
                        else
                        {
                            cevap = "Soruda yasaklı madde yoktur";
                        }
                    }
                }
                else
                {

                    if (girilenCevap == "b)" && koordinatSecim == true)
                    {
                        sonuc = true;                         
                    }
                    else
                    {
                        sonuc = false;
                        string maddeTuru = soruDetay.UniteSorular.SBMaddeResimler.SBTehlikeliMKategori.Isim;
                        cevap = "Maddenin ('" + maddeTuru + "') yerini işaretleyiniz!";                                             
                    }
                }
            }
            catch
            {
                sonuc = false;
                cevap = "Soruda yasaklı madde yoktur";
            }
            var dataSorular = (List<SoruDetaylar>)Session["SoruIceriklerListe"];
            var isle = dataSorular.Where(w => w.SoruNo == soruRef).First();
            isle.Cevap = sonuc;
            Session["SoruIceriklerListe"] = dataSorular;
            return Json(cevap, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            entity.Dispose();
            base.Dispose(disposing);
        }
    }

    public class SoruDetaylar:IDisposable
    {
        public int SoruNo { get; set; }
        public int Sira { get; set; }
        public int Kalan { get; set; }
        public bool? Cevap { get; set; }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
