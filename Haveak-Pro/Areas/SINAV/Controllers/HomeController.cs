﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Haveak_Pro.Models;
using Haveak_Pro.Areas.SINAV.Models;
using Haveak_Pro.Areas.PANEL.Models;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Haveak_Pro.Areas.SINAV.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /SINAV/Home/

        HaveakTurEntities Entity = new HaveakTurEntities();
        int user = AktifKullanici.Aktif.User_Ref;

        #region Sınav İşlemler
        public async Task<ActionResult> Index()
        {
            //ViewBag.Duyurular = await Entity.Duyurulars.Where(t => t.IsDelete == false && t.IsActive == true && t.Tipi == 2).ToListAsync();
            ViewBag.SinavSonucu = await Entity.Personel_Sinavlar.Where(r => r.Bitti == 1 && r.Durum != "AÇIKLANMADI" && r.Sinif_Ogrenci.Personel_Ref == user).ToListAsync();
            var acikKalanSinav = await Entity.Personel_Sinavlar.Where(r => r.IsActive == true && r.Sinif_Ogrenci.Personel_Ref == user && r.Bitti == 0).ToListAsync();
            foreach (var item in acikKalanSinav)
            {
                //DateTime pTarih = item.SinavTarihi.Value;
                //TimeSpan girisSaati = item.GirisSaati.HasValue == false ? TimeSpan.Zero : item.GirisSaati.Value;
                //DateTime girisZaman = pTarih.Add(girisSaati);
                //DateTime bitisGerekenSure = girisZaman.Add(TimeSpan.FromMinutes(item.SinavSuresi.Value));

                DateTime tarih = item.SinavTarihi.Value;
                TimeSpan bitisSaati = item.BitisSaati.Value;
                DateTime bugun = DateTime.Now.Date;
                string saat = DateTime.Now.ToString("H:mm");
                TimeSpan mevcutZaman = TimeSpan.Parse(saat);
                TimeSpan aktifSaat = mevcutZaman;
                if (tarih <= bugun && bitisSaati < aktifSaat)
                {
                    var sinavBitir = await Entity.Personel_Sinavlar.Where(t => t.ID == item.ID).FirstAsync();
                    sinavBitir.Bitti = 1;
                    sinavBitir.CikisSaati = bitisSaati;
                    Entity.SaveChanges();
                }
            }
            return View();
        }

        public async Task<ActionResult> _SSoruDetay(int idx, int SoruNo, int Personel_Sinav_Ref)
        {
            int userRef = AktifKullanici.Aktif.User_Ref;  
            Sinav_Sorular data = await (from d in Entity.Sinav_Sorular
                                        where d.ID == idx
                                        select d).FirstAsync();

            ViewBag.Personel_Sinav_Ref = Personel_Sinav_Ref;
            ViewBag.Soruno = SoruNo;
            return PartialView("_SoruIcerik", data);
        }

        public async Task<ActionResult> _SCevapDurum(int Sinavlar_Ref, int Personel_Sinav_Ref)
        {
            var sorudata = (List<Sinav_Sorular>)HttpContext.Session["SinavSorular"];
            if (sorudata == null)
            {
                sorudata = await Entity.Sinav_Sorular.OrderBy(r => r.ID).Where(r => r.Sinavlar_Ref == Sinavlar_Ref && r.IsActive == true).ToListAsync();
            }
            ViewBag.GirilenCevaplar = await Entity.SinavCevaplars.Include(r=>r.Sinav_Sorular.UniteSorular.SoruSiklaris).Where(m => m.Personel_Sinavlar_Ref == Personel_Sinav_Ref).ToListAsync();
            //ViewBag.SoruSiklari = await Entity.Sinav_Sorular.OrderBy(t => t.ID).Where(t => t.Sinavlar_Ref==Sinavlar_Ref).Select(e => e.UniteSorular.SoruSiklaris.FirstOrDefault()).ToListAsync();

            var sinavTipi = sorudata.Select(r => r.SinavTipi).FirstOrDefault();
            ViewBag.SinavTipi = sinavTipi;
            ViewBag.Entity = Entity;
            ViewBag.Personel_Sinav_Ref = Personel_Sinav_Ref;
            return PartialView("_SinavCevapDurum", sorudata);            
        }
        public async Task<ActionResult> _SSoruDurum(int Sinavlar_Ref, int Personel_Sinav_Ref)
        {
            var sorudata = (List<Sinav_Sorular>)HttpContext.Session["SinavSorular"];
            if (sorudata == null)
            {
                sorudata = await Entity.Sinav_Sorular.OrderBy(r => r.ID).Where(r => r.Sinavlar_Ref == Sinavlar_Ref && r.IsActive == true).ToListAsync();
            }
            ViewBag.Entity = Entity;
            ViewBag.Personel_Sinav_Ref = Personel_Sinav_Ref;
            return PartialView("_SinavSorularDurum", sorudata);          
        }


        public async Task<ActionResult> _SinavKontrolBir()
        {
            List<int> yayindasinavlar = await Entity.Sinavlars.Where(r => r.Yayinda == true && r.IsActive && !r.IsDelete && r.Firma_Ref == AktifKullanici.Aktif.FirmaId).Select(t=>t.ID).ToListAsync();
            List<int> girilenSinavlar = await Entity.Personel_Sinavlar.Where(r => r.Sinif_Ogrenci.Personel_Ref == user).Select(t => t.Sinavlar.ID).ToListAsync();
            DateTime bugun = DateTime.Now.Date.AddDays(-1);
            List<int> girilmeyenSinavlar = yayindasinavlar.Except(girilenSinavlar).ToList();
            ViewBag.psinavlar = await Entity.Personel_Sinavlar.OrderByDescending(r => r.ID).Where(t => t.IsActive == true && t.IsDelete == false && t.SinavTarihi >= bugun && t.Sinif_Ogrenci.Personel_Ref == user && (t.Basladi == 1 || t.Basladi == 0) && t.Bitti == 0).ToListAsync();
            return PartialView("_SinavKontrol", girilmeyenSinavlar);
        }

        public async Task<ActionResult> _SinavDuyuruBir()
        {
            List<Personel_Sinavlar> data = await Entity.Personel_Sinavlar.OrderByDescending(w => w.ID).Where(t => t.IsActive == true && t.IsDelete == false && t.Sinif_Ogrenci.Personel_Ref == user).ToListAsync();
            return PartialView("_SinavDuyurular", data);
        }

        public async Task<ActionResult> _SinavCevaplar(int id)
        {
            var pcevap = await Entity.SinavCevaplars.Where(y => y.Personel_Sinavlar_Ref == id).ToListAsync();
            return PartialView("_SinavDetay", pcevap);
        }

        public JsonResult AltSiklarGetir(int id)
        {
            var data = Entity.AltSiklars.Where(t => t.IsActive == true && t.UniteSorular_Ref == id).Select(r => new { r.ID, r.AltSoruSikki, r.AltCevapIcerik }).ToList();
            return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }


        public JsonResult SinavCevap(int Personel_SinavRef, int SinavSoruRef, int? Cevap, int? AltCevap, int? xkordinat, int? ykordinat, bool? koordinatSecim)
        {
            int? durum = 0;
            try
            {
                SinavCevaplar control = Entity.SinavCevaplars.Where(t => t.Personel_Sinavlar_Ref == Personel_SinavRef && t.Sınav_Sorular_Ref == SinavSoruRef).FirstOrDefault();
                if (control != null)
                {
                    control.Personel_Sinavlar_Ref = Personel_SinavRef;
                    control.Sınav_Sorular_Ref = SinavSoruRef;
                    control.GirilenCevap = Cevap;
                    control.AltCevap = AltCevap;
                    control.CevapResimX = xkordinat;
                    control.CevapResimY = ykordinat;
                    control.DogruCevap = Entity.SoruSiklaris.Where(r => r.ID == Cevap).Select(e => e.DogruSecenek).FirstOrDefault();
                    control.DogruAltCevap = Entity.AltSiklars.Where(y => y.ID == AltCevap).Select(r => r.AltDogruSecenek).FirstOrDefault();
                    control.KoordinatSecim = koordinatSecim;

                    var unite = Entity.Sinav_Sorular.Where(r => r.ID == SinavSoruRef).Select(n => new { n.UniteSorular_Ref, n.SinavTipi_Ref }).FirstOrDefault();
                    int altsik = Entity.AltSiklars.Where(t => t.UniteSorular_Ref == unite.UniteSorular_Ref).Count();
                    decimal? resim = Entity.UniteSorulars.Where(t => t.ID == unite.UniteSorular_Ref).Select(u => u.XKordinat).FirstOrDefault();

                    bool sonuc = false;
                    if (altsik != 0 && resim != null && unite.SinavTipi_Ref == (int)EnumSinav.SinavTipi.Uygulama)
                    {
                        if (control.DogruCevap == true && control.DogruAltCevap == true && control.KoordinatSecim == true)
                        {
                            sonuc = true;
                        }
                        else
                        {
                            sonuc = false;
                        }
                    }
                    else if (altsik == 0 && unite.SinavTipi_Ref == (int)EnumSinav.SinavTipi.Uygulama)
                    {
                        var secenek = Entity.SoruSiklaris.Where(r => r.ID == Cevap && r.DogruSecenek == true).Select(e => e.SoruSikki).FirstOrDefault();
                        if (secenek == "a)")
                        {
                            if (control.DogruCevap == true)
                            {
                                sonuc = true;
                            }
                            else
                            {
                                sonuc = false;
                            }
                        }
                        else
                        {

                            if (control.DogruCevap == true && control.KoordinatSecim == true)
                            {
                                sonuc = true;
                            }
                            else
                            {
                                sonuc = false;
                            }

                        }
                    }
                    else if (unite.SinavTipi_Ref == (int)EnumSinav.SinavTipi.Teorik)
                    {
                        if (control.DogruCevap == true)
                        {
                            sonuc = true;
                        }
                        else
                        {
                            sonuc = false;
                        }
                    }
                    control.Netice = sonuc;
                    Entity.SaveChanges();
                    //durum = "Guncellendi";
                }
                else
                {
                    int unite_Ref = Entity.Sinav_Sorular.Where(t => t.ID == SinavSoruRef).Select(r => r.UniteSorular_Ref).FirstOrDefault();
                    control = new SinavCevaplar();
                    control.Personel_Sinavlar_Ref = Personel_SinavRef;
                    control.Sınav_Sorular_Ref = SinavSoruRef;
                    control.GirilenCevap = Cevap;
                    control.AltCevap = AltCevap;
                    control.CevapResimX = xkordinat;
                    control.CevapResimY = ykordinat;
                    control.DogruCevap = Entity.SoruSiklaris.Where(r => r.ID == Cevap).Select(e => e.DogruSecenek).FirstOrDefault();
                    control.DogruAltCevap = Entity.AltSiklars.Where(y => y.ID == AltCevap).Select(r => r.AltDogruSecenek).FirstOrDefault();
                    control.KoordinatSecim = koordinatSecim;

                    var unite = Entity.Sinav_Sorular.Where(r => r.ID == SinavSoruRef).Select(n => new { n.UniteSorular_Ref, n.SinavTipi_Ref }).FirstOrDefault();
                    int altsik = Entity.AltSiklars.Where(t => t.UniteSorular_Ref == unite.UniteSorular_Ref).Count();
                    int? resim = Entity.UniteSorulars.Where(t => t.ID == unite.UniteSorular_Ref).Select(u => u.SBagajlar_Ref).FirstOrDefault();

                    bool sonuc = false;
                    if (altsik != 0 && resim != null && unite.SinavTipi_Ref == (int)EnumSinav.SinavTipi.Uygulama)
                    {
                        if (control.DogruCevap == true && control.DogruAltCevap == true && control.KoordinatSecim == true)
                        {
                            sonuc = true;
                        }
                        else
                        {
                            sonuc = false;
                        }
                    }
                    else if (altsik == 0 && unite.SinavTipi_Ref == (int)EnumSinav.SinavTipi.Uygulama)
                    {
                        var secenek = Entity.SoruSiklaris.Where(r => r.ID == Cevap && r.DogruSecenek == true).Select(e => e.SoruSikki).FirstOrDefault();
                        if (secenek == "a)")
                        {
                            if (control.DogruCevap == true)
                            {
                                sonuc = true;
                            }
                            else
                            {
                                sonuc = false;
                            }
                        }
                        else
                        {

                            if (control.DogruCevap == true && control.KoordinatSecim == true)
                            {
                                sonuc = true;
                            }
                            else
                            {
                                sonuc = false;
                            }

                        }
                    }
                    else if (unite.SinavTipi_Ref == (int)EnumSinav.SinavTipi.Teorik)
                    {
                        if (control.DogruCevap == true)
                        {
                            sonuc = true;
                        }
                        else
                        {
                            sonuc = false;
                        }
                    }
                    control.Netice = sonuc;
                    Entity.SinavCevaplars.Add(control);
                    Entity.SaveChanges();

                    //durum = "Tamam";
                }                
                int? SonrakisoruID = Entity.Sinav_Sorular.Where(r => r.ID > SinavSoruRef && r.Sinavlar.Personel_Sinavlar.Any(t=> t.ID == control.Personel_Sinavlar_Ref)).Select(t => t.ID).FirstOrDefault();
                durum = SonrakisoruID;
            }
            catch
            {
                durum = 0;
            }
            return Json(durum, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> SinavaBasla(int? id, int? sinavId)
        {
            DateTime bugun = DateTime.Now.Date;
            TimeSpan aktifzaman = DateTime.Now.TimeOfDay;
            int? sinifOgrenciRef = await Entity.Sinif_Ogrenci.OrderByDescending(r => r.ID).Where(t => t.Bitti == false && t.Personel_Ref == user).Select(r => r.ID).FirstOrDefaultAsync();
            List<Sinav_Sorular> sorudata = null;
            IQueryable<Personel_Sinavlar> sinavKontrol = Entity.Personel_Sinavlar.Where(r => r.IsActive && !r.IsDelete && r.Sinif_Ogrenci_Ref == sinifOgrenciRef);
            Personel_Sinavlar aktifSinav;
            if (sinavId != null)
            {
                aktifSinav = new Personel_Sinavlar();
                var aktifSinavKontrol = await sinavKontrol.Where(r => r.Basladi == 1 && r.Bitti == 0 && r.SinavTarihi == bugun && r.BaslamaSaati <= aktifzaman && r.BitisSaati >= aktifzaman).FirstOrDefaultAsync();
                if (aktifSinavKontrol != null)
                {
                    aktifSinav = await Entity.Personel_Sinavlar.Where(t => t.Sinavlar_Ref == sinavId && t.Sinif_Ogrenci_Ref == sinifOgrenciRef).FirstOrDefaultAsync();
                    if (aktifSinav == null)
                    {
                        aktifSinav = aktifSinavKontrol;
                    }
                }
                else
                {
                    aktifSinav = null;
                }
            }
            else
            {
                aktifSinav = await Entity.Personel_Sinavlar.Where(t => t.ID == id).FirstOrDefaultAsync();
            }

            if (aktifSinav == null)
            {
                int sogrenciRef = await Entity.Sinif_Ogrenci.OrderByDescending(r => r.ID).Where(r => r.Personel_Ref == user && !r.Bitti).Select(w => w.ID).FirstOrDefaultAsync();
                Sinavlar sinav = await Entity.Sinavlars.Where(r => r.ID == sinavId).FirstAsync();
                TimeSpan baslamaSaati = DateTime.Now.TimeOfDay;
                int surex = sinav.SoruSayisi == 30 ? 20 : sinav.SoruSayisi == 50 ? 30 : 40;
                TimeSpan addMinute = TimeSpan.FromMinutes(surex);
                TimeSpan bitisSaati = baslamaSaati.Add(addMinute);
                TimeSpan bsaati = TimeSpan.Parse(string.Format("{0:hh\\:mm\\:ss}", baslamaSaati));
                TimeSpan bitsaati = TimeSpan.Parse(string.Format("{0:hh\\:mm\\:ss}", bitisSaati));
                aktifSinav = new Personel_Sinavlar();
                aktifSinav.IsActive = true;
                aktifSinav.IsDelete = false;
                aktifSinav.Sinif_Ogrenci_Ref = sogrenciRef;
                aktifSinav.Sinavlar_Ref = sinavId.Value;
                aktifSinav.SinavAciklama = false;
                aktifSinav.SinavNotuUygulama = 0;
                aktifSinav.SinavNotuTeorik = 0;
                aktifSinav.Durum = "AÇIKLANMADI";
                aktifSinav.DogruSayisiUygulama = 0;
                aktifSinav.YanlisSayisiTeorik = 0;
                aktifSinav.UygulamaBos = 0;
                aktifSinav.YanlisSayisiUgulama = 0;
                aktifSinav.DogruSayisiTeorik = 0;
                aktifSinav.TeorikBos = 0;
                aktifSinav.SinavTarihi = DateTime.Now.Date;
                aktifSinav.BaslamaSaati = bsaati;
                aktifSinav.BitisSaati = bitsaati;
                aktifSinav.SinavSuresi = surex;
                aktifSinav.Basladi = 1;
                aktifSinav.Bitti = 0;
                aktifSinav.GirisSaati = bsaati;
                aktifSinav.CikisSaati = null;
                Entity.Personel_Sinavlar.Add(aktifSinav);
                await Entity.SaveChangesAsync();
            }
            ViewBag.AktifSinav = aktifSinav.Sinavlar_Ref;
            ViewBag.PerSinavID = aktifSinav.ID;
            sorudata = await Entity.Sinav_Sorular.Include(r=>r.SinavTipi).Include(r=>r.UniteSorular.SoruSiklaris).OrderBy(r => r.ID).Where(r => r.Sinavlar_Ref == aktifSinav.Sinavlar_Ref && r.IsActive == true).ToListAsync();
            HttpContext.Session.Add("SinavSorular", sorudata);
            return View(sorudata);
        }

        public JsonResult SinavStart(int id)
        {
            string durum = "";
            Personel_Sinavlar aktifSinav = Entity.Personel_Sinavlar.Where(t => t.ID == id).FirstOrDefault();
            if (aktifSinav.Basladi == 0)
            {
                aktifSinav.Basladi = 1;
                string saat = DateTime.Now.ToString("H:mm:ss");
                TimeSpan mevcutZaman = TimeSpan.Parse(saat);
                aktifSinav.GirisSaati = mevcutZaman;
                Entity.SaveChanges();
                durum = "tamam";
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SinavBitir(int aktifSinavID)
        {
            bool durum = false;
            try
            {
                Personel_Sinavlar aktifSinav = await Entity.Personel_Sinavlar.FindAsync(aktifSinavID);
                aktifSinav.Bitti = 1;
                string saat = DateTime.Now.ToString("H:mm:ss");
                TimeSpan mevcutZaman = TimeSpan.Parse(saat);
                aktifSinav.CikisSaati = mevcutZaman;

                IList<PANEL.Controllers.SinavSonuclari> cevaplar = new List<PANEL.Controllers.SinavSonuclari>();
                int sinavEtiket = aktifSinav.Sinavlar_Ref;
                var sinavSorular = await Entity.Sinav_Sorular.Where(r => r.Sinavlar_Ref == sinavEtiket && r.IsActive == true && r.IsDelete == false).ToListAsync();
                foreach (var item2 in sinavSorular)
                {
                    var cevapkontrol = item2.SinavCevaplars.Where(r => r.Personel_Sinavlar_Ref == aktifSinav.ID).Select(t => new { t.Netice, t.Sınav_Sorular_Ref }).FirstOrDefault();

                    string cevabi = "";
                    if (cevapkontrol == null)
                    {
                        cevabi = "YOK";
                    }
                    else
                    {
                        if (cevapkontrol.Netice == true)
                        {
                            cevabi = "True";
                        }
                        else
                        {
                            cevabi = "False";
                        }
                    }
                    PANEL.Controllers.SinavSonuclari datax = new PANEL.Controllers.SinavSonuclari();
                    datax.PerID = aktifSinav.ID;
                    datax.SoruID = item2.ID;
                    datax.Cevap = cevabi;
                    datax.SoruTipi = item2.SinavTipi_Ref;
                    cevaplar.Add(datax);
                }

                int TeorikDogruSayisi = cevaplar.Where(t => t.SoruTipi == 2 && t.Cevap == "True").Count();
                int TeorikYanlisSayisi = cevaplar.Where(t => t.SoruTipi == 2 && t.Cevap == "False").Count();
                int TeorikBos = cevaplar.Where(t => t.SoruTipi == 2 && t.Cevap == "YOK").Count();

                int UygulamaDogruSayisi = cevaplar.Where(t => t.SoruTipi == 1 && t.Cevap == "True").Count();
                int UygulamaYanlisSayisi = cevaplar.Where(t => t.SoruTipi == 1 && t.Cevap == "False").Count();
                int UygulamaBos = cevaplar.Where(t => t.SoruTipi == 1 && t.Cevap == "YOK").Count();

                decimal TamPuan = 100;

                int ToplamSoruUygulama = sinavSorular.Where(y => y.SinavTipi_Ref == 1).Count();
                int ToplamSoruTeorik = sinavSorular.Where(y => y.SinavTipi_Ref == 2).Count();
                decimal soruPuanUygulama = 0;
                decimal soruPuanTeorik = 0;
                if (ToplamSoruUygulama != 0)
                {
                    soruPuanUygulama = TamPuan / ToplamSoruUygulama;
                }
                if (ToplamSoruTeorik != 0)
                {
                    soruPuanTeorik = TamPuan / ToplamSoruTeorik;
                }


                decimal SinavNotuUygulama = soruPuanUygulama * UygulamaDogruSayisi;
                decimal SinavNotuTeorik = soruPuanTeorik * TeorikDogruSayisi;

                string Netice = "";
                int uygulamaSoruVami = sinavSorular.Where(e => e.SinavTipi_Ref == (int)Enumlar.SoruTipleri.UYGULAMA).Count();
                int teorikSoruVarmi = sinavSorular.Where(e => e.SinavTipi_Ref == (int)Enumlar.SoruTipleri.TEORIK).Count();

                if (uygulamaSoruVami > 0 && teorikSoruVarmi > 0)
                {
                    if (SinavNotuUygulama >= 75 && SinavNotuTeorik >= 75)
                    {
                        Netice = "GEÇTİ";
                    }
                    else
                    {
                        Netice = "KALDI";
                    }
                }
                else if (uygulamaSoruVami > 0 && teorikSoruVarmi == 0)
                {
                    if (SinavNotuUygulama >= 75)
                    {
                        Netice = "GEÇTİ";
                    }
                    else
                    {
                        Netice = "KALDI";
                    }
                }
                else if (uygulamaSoruVami == 0 && teorikSoruVarmi > 0)
                {
                    if (SinavNotuTeorik >= 75)
                    {
                        Netice = "GEÇTİ";
                    }
                    else
                    {
                        Netice = "KALDI";
                    }
                }
                aktifSinav.SinavAciklama = true;
                aktifSinav.DogruSayisiUygulama = UygulamaDogruSayisi;
                aktifSinav.DogruSayisiTeorik = TeorikDogruSayisi;
                aktifSinav.YanlisSayisiUgulama = UygulamaYanlisSayisi;
                aktifSinav.YanlisSayisiTeorik = TeorikYanlisSayisi;
                aktifSinav.SinavNotuUygulama = SinavNotuUygulama;
                aktifSinav.SinavNotuTeorik = SinavNotuTeorik;
                aktifSinav.UygulamaBos = UygulamaBos;
                aktifSinav.TeorikBos = TeorikBos;
                aktifSinav.Durum = Netice;
                await Entity.SaveChangesAsync();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SinavCevaplarPersonel(int id)
        {
            return View(id);
        }
        #endregion

        #region Test İşlemler
        public async Task<ActionResult> TesteBasla(int id)
        {
            int user = AktifKullanici.Aktif.User_Ref;
            List<Test_Sorular> data = await Entity.Test_Sorular.Where(r => r.Testler_Ref == id && r.IsActive == true).ToListAsync();
            Personel_Testler aktifTest = await Entity.Personel_Testler.Where(t => t.Sinif_Ogrenci.Personel_Ref == user && t.Testler_Ref == id && t.Bitti == 0).FirstOrDefaultAsync();
            int prsID = 0;
            int aktifTst = 0;
            if (aktifTest != null)
            {
                aktifTst = aktifTest.Testler_Ref;
                prsID = aktifTest.ID;
            }
            ViewBag.AktifTest = aktifTst;
            ViewBag.PerTestID = prsID;

            //else
            //{
            //    return RedirectToAction("Index", "Home");
            //}

            return View(data);
        }

        public async Task<ActionResult> _TSoruDetay(int Test_Ref, int idx, int SoruNo)
        {
            Test_Sorular data = await (from d in Entity.Test_Sorular
                                       where
                                           //d.Testler_Ref == Test_Ref &&
                                           d.ID == idx
                                       //d.Testler.Personel_Testler.Where(r => r.Sinif_Ogrenci.Personel_Ref == userRef).Select(t => t.Testler_Ref).FirstOrDefault() == Test_Ref
                                       select d).FirstOrDefaultAsync();
            //Sinav_Sorular data = Entity.Sinav_Sorular.Where(t => t.Sinavlar_Ref == Sinav_Ref && t.ID == idx).First();
            ViewBag.PersonelTest = await Entity.Personel_Testler.Include(r => r.TestCevaplars).Where(r => r.Sinif_Ogrenci.Personel_Ref == user && r.Testler_Ref == data.Testler_Ref).FirstOrDefaultAsync();
            ViewBag.Soruno = SoruNo;
            ViewBag.TestRef = Test_Ref;
            return PartialView("_SoruIcerikTest", data);
        }

        public ActionResult _TestCevaplar(int id)
        {
            var pcevap = Entity.TestCevaplars.Where(y => y.Personel_Testler_Ref == id).ToList();
            return PartialView("_TestDetay", pcevap);
        }

        public JsonResult TestStart(int id, int sinif_Ogrenci_ref)
        {
            string durum = "";
            Personel_Testler aktifTest = new Personel_Testler();
            aktifTest.IsActive = true;
            aktifTest.IsDelete = false;
            aktifTest.Sinif_Ogrenci_Ref = sinif_Ogrenci_ref;
            aktifTest.Testler_Ref = id;
            aktifTest.TestAciklama = false;
            aktifTest.TestNotu = 0;
            aktifTest.Durum = "AÇIKLANMADI";
            aktifTest.DogruSayisi = 0;
            aktifTest.YanlisSayisi = 0;
            aktifTest.Bos = 0;
            aktifTest.TestTarihi = DateTime.Now.Date;
            string saat = DateTime.Now.ToString("H:mm:ss");
            TimeSpan mevcutZaman = TimeSpan.Parse(saat);
            aktifTest.GirisSaati = mevcutZaman;
            aktifTest.Basladi = 1;
            aktifTest.Bitti = 0;
            Entity.Personel_Testler.Add(aktifTest);
            Entity.SaveChanges();
            durum = "tamam";

            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult TestBitir(int aktifTestID)
        {
            string durum = "";
            try
            {
                int user = AktifKullanici.Aktif.User_Ref;
                Personel_Testler aktifTest = Entity.Personel_Testler.Where(t => t.ID == aktifTestID).FirstOrDefault();
                aktifTest.Bitti = 1;
                string saat = DateTime.Now.ToString("H:mm:ss");
                TimeSpan mevcutZaman = TimeSpan.Parse(saat);
                aktifTest.CikisSaati = mevcutZaman;
                TimeSpan dakika = aktifTest.CikisSaati.Value - aktifTest.GirisSaati.Value;
                aktifTest.TestSuresi = dakika.Minutes == 0 ? 1 : dakika.Minutes;
                Entity.SaveChanges();
                bool sonucu = TestSonuclandirma(aktifTest.ID, aktifTest.Testler_Ref);
                if (sonucu)
                {
                    durum = "tamam";
                }
            }
            catch
            {
                durum = "hata";
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }


        public bool TestSonuclandirma(int id, int testRef)
        {
            bool durum = false;
            try
            {
                List<TestSonuclari> cevaplar = new List<TestSonuclari>();
                var testSorular = Entity.Test_Sorular.Where(r => r.Testler_Ref == testRef && r.IsActive == true && r.IsDelete == false).ToList();
                foreach (var item2 in testSorular)
                {
                    var cevapkontrol = item2.TestCevaplars.Where(r => r.Personel_Testler_Ref == id).Select(t => new { t.Netice, t.Test_Sorular_Ref }).FirstOrDefault();
                    string cevabi = "";
                    if (cevapkontrol == null)
                    {
                        cevabi = "YOK";
                    }
                    else
                    {
                        if (cevapkontrol.Netice == true)
                        {
                            cevabi = "True";
                        }
                        else
                        {
                            cevabi = "False";
                        }
                    }
                    TestSonuclari datax = new TestSonuclari();
                    datax.PerID = id;
                    datax.SoruID = item2.ID;
                    datax.Cevap = cevabi;
                    cevaplar.Add(datax);
                }

                int DogruSayisi = cevaplar.Where(t => t.Cevap == "True").Count();
                int YanlisSayisi = cevaplar.Where(t => t.Cevap == "False").Count();
                int Bos = cevaplar.Where(t => t.Cevap == "YOK").Count();

                decimal TamPuan = 100;

                int ToplamSoru = testSorular.Count();

                decimal soruPuan = 0;
                if (ToplamSoru != 0)
                {
                    soruPuan = TamPuan / ToplamSoru;
                }

                decimal TestNotu = soruPuan * DogruSayisi;

                string Netice = "";
                if (TestNotu >= 70)
                {
                    Netice = "GEÇTİ";
                }
                else
                {
                    Netice = "KALDI";
                }

                var perTestIsleme = Entity.Personel_Testler.Where(t => t.ID == id).FirstOrDefault();
                perTestIsleme.DogruSayisi = DogruSayisi;
                perTestIsleme.YanlisSayisi = YanlisSayisi;
                perTestIsleme.TestNotu = TestNotu;
                perTestIsleme.Bos = Bos;
                perTestIsleme.Durum = Netice;
                perTestIsleme.TestAciklama = true;
                Entity.SaveChanges();
                durum = true;

            }
            catch
            {
                durum = false;
            }
            return durum;
        }

        public ActionResult Modul_Testler(int id, int? sonkonu)
        {
            int user_ref = AktifKullanici.Aktif.User_Ref;
            var testler = Entity.Testlers.Where(t => t.UniteModul_Ref == id && t.IsActive == true && t.IsDelete == false).ToList();
            ViewBag.Testler = Entity.Personel_Testler.Where(t => t.Sinif_Ogrenci.Personel_Ref == user_ref && t.Testler.UniteModul_Ref == id).ToList();
            ViewBag.Idx = id;
            List<Personel_Testler> data = Entity.Personel_Testler.Where(t => t.IsActive == true && t.IsDelete == false && t.Sinif_Ogrenci.Personel_Ref == user_ref).ToList();
            ViewBag.TestCevaplar = data;
            var bittiKontrol = Entity.Personel_Testler.Where(e => e.Bitti == 1 && e.TestAciklama == false).ToList();
            foreach (var itembt in bittiKontrol)
            {
                if (itembt != null)
                {
                    TestBitir(itembt.ID);
                }
            }
            ViewBag.Sonkonu = sonkonu ?? 0;
            return View(testler);
        }


        public JsonResult TestCevap(int Personel_TestRef, int TestSoruRef, int? Cevap, int? AltCevap)
        {
            string durum = "";
            try
            {
                var control = Entity.TestCevaplars.Where(t => t.Personel_Testler_Ref == Personel_TestRef && t.Test_Sorular_Ref == TestSoruRef).FirstOrDefault();
                if (control != null)
                {
                    control.Personel_Testler_Ref = Personel_TestRef;
                    control.Test_Sorular_Ref = TestSoruRef;
                    control.GirilenCevap = Cevap;
                    control.AltCevap = AltCevap;
                    control.DogruCevap = Entity.SoruSiklaris.Where(r => r.ID == Cevap).Select(e => e.DogruSecenek).FirstOrDefault();
                    control.DogruAltCevap = Entity.AltSiklars.Where(y => y.ID == AltCevap).Select(r => r.AltDogruSecenek).FirstOrDefault();

                    var unite = Entity.Test_Sorular.Where(r => r.ID == TestSoruRef).Select(n => new { n.UniteSorular_Ref }).FirstOrDefault();
                    int altsik = Entity.AltSiklars.Where(t => t.UniteSorular_Ref == unite.UniteSorular_Ref).Count();


                    bool sonuc = false;

                    if (altsik != 0)
                    {
                        if (control.DogruCevap == true && control.DogruAltCevap == true)
                        {
                            sonuc = true;
                        }
                        else
                        {
                            sonuc = false;
                        }
                    }
                    else if (altsik == 0)
                    {
                        var secenek = Entity.SoruSiklaris.Where(r => r.ID == Cevap && r.DogruSecenek == true).Select(e => e.SoruSikki).FirstOrDefault();
                        if (secenek == "a)")
                        {
                            if (control.DogruCevap == true)
                            {
                                sonuc = true;
                            }
                            else
                            {
                                sonuc = false;
                            }
                        }
                        else
                        {

                            if (control.DogruCevap == true)
                            {
                                sonuc = true;
                            }
                            else
                            {
                                sonuc = false;
                            }
                        }
                    }

                    control.Netice = sonuc;
                    Entity.SaveChanges();
                    durum = "Guncellendi";
                }
                else
                {
                    int unite_Ref = Entity.Test_Sorular.Where(t => t.ID == TestSoruRef).Select(r => r.UniteSorular_Ref).FirstOrDefault();
                    var data = new TestCevaplar();
                    data.Personel_Testler_Ref = Personel_TestRef;
                    data.Test_Sorular_Ref = TestSoruRef;
                    data.GirilenCevap = Cevap;
                    data.AltCevap = AltCevap;
                    data.DogruCevap = Entity.SoruSiklaris.Where(r => r.ID == Cevap).Select(e => e.DogruSecenek).FirstOrDefault();
                    data.DogruAltCevap = Entity.AltSiklars.Where(y => y.ID == AltCevap).Select(r => r.AltDogruSecenek).FirstOrDefault();

                    var unite = Entity.Test_Sorular.Where(r => r.ID == TestSoruRef).Select(n => new { n.UniteSorular_Ref }).FirstOrDefault();
                    int altsik = Entity.AltSiklars.Where(t => t.UniteSorular_Ref == unite.UniteSorular_Ref).Count();

                    bool sonuc = false;
                    if (altsik != 0)
                    {
                        if (data.DogruCevap == true && data.DogruAltCevap == true)
                        {
                            sonuc = true;
                        }
                        else
                        {
                            sonuc = false;
                        }
                    }
                    else if (altsik == 0)
                    {
                        var secenek = Entity.SoruSiklaris.Where(r => r.ID == Cevap && r.DogruSecenek == true).Select(e => e.SoruSikki).FirstOrDefault();
                        if (secenek == "a)")
                        {
                            if (data.DogruCevap == true)
                            {
                                sonuc = true;
                            }
                            else
                            {
                                sonuc = false;
                            }
                        }
                        else
                        {

                            if (data.DogruCevap == true)
                            {
                                sonuc = true;
                            }
                            else
                            {
                                sonuc = false;
                            }

                        }
                    }

                    data.Netice = sonuc;
                    Entity.TestCevaplars.Add(data);
                    Entity.SaveChanges();

                    durum = "Tamam";
                }

            }
            catch
            {
                durum = "İşlem sırasında hata meydana geldi tekrar deneyiniz!";
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            Entity.Dispose();
            base.Dispose(disposing);
        }

    }
    public class TestSonuclari : IDisposable
    {
        public int PerID { get; set; }
        public int SoruID { get; set; }
        public string Cevap { get; set; }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }

    public class GirilenCevap : IDisposable
    {

        public int? Cevap { get; set; }
        public int Sinav_Sorular_Ref { get; set; }
        public int? AltCevap { get; set; }
        public int Personel_Sinavlar_Ref { get; set; }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }

    public class SoruSiklariLocal : IDisposable
    {

        public int ID { get; set; }
        public string SoruSikki { get; set; }
        public string CevapIcerik { get; set; }
        public bool DogruSecenek { get; set; }


        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
