﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Haveak_Pro.Models;
using Haveak_Pro.Areas.SINAV.Models;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Haveak_Pro.Areas.SINAV.Controllers
{
    public class TipSinavController : Controller
    {
        //
        // GET: /SINAV/TipSinav/
        HaveakTurEntities entity = new HaveakTurEntities();
        public async Task<ActionResult> Index(bool? tst)
        {
            int user = AktifKullanici.Aktif.User_Ref;
            IList<Personel_TIP> tiptest = await (from r in entity.Personel_TIP where r.IsActive && !r.IsDelete && r.Personel_Ref == user select r).ToListAsync();
            IList<TipSinavlar> data = await (from d in entity.TipSinavlars where d.IsActive && !d.IsDelete && d.Yayinda select d).ToListAsync();
            IList<TipEgitimSinavlar> TipEgitimTest = await (from f in entity.TipEgitimSinavlars orderby Guid.NewGuid() where f.IsActive && !f.IsDelete && f.Yayinda select f).ToListAsync();

            ViewBag.aktifKontrol = tiptest.Where(e => e.Bitti == 0).Select(w => w.TipSinavlar_Ref).FirstOrDefault();
            ViewBag.PersonelTIP = tiptest;
            ViewBag.TipEgitimTest = TipEgitimTest;
            foreach (var item in tiptest)
            {
                if (item.Basladi == 1 && item.Bitti == 0)
                {
                    DateTime tipTarih = item.TipTarihi.Value;
                    TimeSpan girisSaati = item.GirisSaati.Value;
                    DateTime girisZaman = tipTarih.Add(girisSaati);
                    DateTime bitisGerekenSure = girisZaman.Add(TimeSpan.FromMinutes(item.TipSinavlar.Sure));
                    DateTime suan = DateTime.Now;
                    if (suan > bitisGerekenSure)
                    {
                        ActionResult drm = TipTestBitir(item.ID);
                    }
                }
            }
            ViewBag.Test = tst;
            return View(data);
        }

        public ActionResult _tipDetay(int id)
        {
            return PartialView("_TipDetayListe", id);
        }

        public async Task<ActionResult> TipTesteBasla(int id)
        {
            int user = AktifKullanici.Aktif.User_Ref;
            Personel_TIP aktifSinav = await entity.Personel_TIP.Where(t => t.TipSinavlar_Ref == id && t.Personel_Ref == user && t.IsActive && !t.IsDelete && t.Bitti == 0).FirstOrDefaultAsync();
            ViewBag.Per_TIP = aktifSinav;
            ViewBag.TipSinavID = id;
            IList<Tip_Sorular> data = await entity.Tip_Sorular.Where(r => r.TipSinavlar_Ref == id && r.IsActive && !r.IsDelete).ToListAsync();
            return View(data);
        }

        public async Task<ActionResult> _TSoruDetay(int Tip_Ref, int idx)
        {
            int userRef = AktifKullanici.Aktif.User_Ref;
            int sonrakiSoru = 0;
            int index = 0;

            ICollection<TipCevaplar> ckontrol = await entity.Personel_TIP.Where(r => r.TipSinavlar_Ref == Tip_Ref && r.Personel_Ref == userRef && r.Bitti == 0).Select(e => e.TipCevaplars).FirstOrDefaultAsync();
            if (ckontrol != null)
            {
                int cevapson = ckontrol.OrderByDescending(q => q.ID).Select(r => r.Tip_Sorular_Ref).FirstOrDefault();
                var sorular = await (from d in entity.Tip_Sorular
                                     where
                                         d.TipSinavlar_Ref == Tip_Ref &&
                                         d.IsActive == true &&
                                         d.IsDelete == false &&
                                   d.TipSinavlar.Personel_TIP.Where(r => r.Personel_Ref == userRef && r.Bitti == 0).Select(t => t.TipSinavlar_Ref).FirstOrDefault() == Tip_Ref
                                     select d.ID).ToListAsync();

                Dictionary<int, int> SoruListe = new Dictionary<int, int>();
                foreach (var item in sorular)
                {
                    index++;
                    SoruListe.Add(index, item);
                }
                int soruKey = SoruListe.Where(r => r.Value == cevapson).Select(w => w.Key).FirstOrDefault();
                soruKey++;
                ViewBag.SoruIndex = soruKey;

                sonrakiSoru = SoruListe.Where(q => q.Key == soruKey).Select(t => t.Value).FirstOrDefault();
                idx = sonrakiSoru;
            }
            else
            {
                sonrakiSoru = 0;
                ViewBag.SoruIndex = 0;
            }
            Tip_Sorular data = await entity.Tip_Sorular.FindAsync(idx);
            ViewBag.Soruno = sonrakiSoru;

            ViewBag.TipRef = Tip_Ref;
            ViewBag.entity = entity;

            return PartialView("_SoruIcerikTip", data);
        }

        public async Task<ActionResult> _TipCevaplar(int id)
        {
            var pcevap = await entity.TipCevaplars.Where(y => y.Personel_Tip_Ref == id && y.Tip_Sorular.IsActive == true && y.Tip_Sorular.IsDelete == false).ToListAsync();
            return PartialView("_TipDetay", pcevap);
        }

        public async Task<ActionResult> TipTestStart(int id, int personel_ref)
        {
            string tipNo = Guid.NewGuid().ToString().Split('-')[0];
            string durum = string.Empty;
            try
            {
                Personel_TIP aktifTest = new Personel_TIP();
                aktifTest.IsActive = true;
                aktifTest.IsDelete = false;
                aktifTest.Personel_Ref = personel_ref;
                aktifTest.TipSinavlar_Ref = id;
                //aktifTest.TipAciklama = false;
                aktifTest.TipNotu = 0;
                aktifTest.TipNo = tipNo.ToUpper();
                aktifTest.DogruSayisi = 0;
                aktifTest.YanlisSayisi = 0;
                //aktifTest.Bos = 0;
                aktifTest.TipTarihi = DateTime.Now.Date;
                string saat = DateTime.Now.ToString("H:mm:ss");
                TimeSpan mevcutZaman = TimeSpan.Parse(saat);
                aktifTest.GirisSaati = mevcutZaman;
                aktifTest.Basladi = 1;
                aktifTest.Bitti = 0;
                entity.Personel_TIP.Add(aktifTest);
                await entity.SaveChangesAsync();
                durum = "ok";
            }
            catch (Exception ex)
            {
                durum = ex.Message;
            }

            return Json(durum, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TipTestBitir(int aktifTipTestID)
        {
            bool sonuc = false;
            try
            {
                //int user = AktifKullanici.Aktif.User_Ref;
                Personel_TIP aktifTest = entity.Personel_TIP.Find(aktifTipTestID);
                DateTime tipTarih = aktifTest.TipTarihi.Value;
                TimeSpan girisSaati = aktifTest.GirisSaati.Value;
                DateTime girisZaman = tipTarih.Add(girisSaati);
                DateTime bitisGerekenSure = girisZaman.Add(TimeSpan.FromMinutes(aktifTest.TipSinavlar.Sure));
                aktifTest.Bitti = 1;
                DateTime suan = DateTime.Now;
                if (suan > bitisGerekenSure)
                {
                    string saat = bitisGerekenSure.ToString("H:mm:ss");
                    aktifTest.CikisSaati = TimeSpan.Parse(saat);
                }
                else
                {
                    string saat = DateTime.Now.ToString("H:mm:ss");
                    aktifTest.CikisSaati = TimeSpan.Parse(saat);
                }
                TimeSpan dakika = aktifTest.CikisSaati.Value - aktifTest.GirisSaati.Value;
                aktifTest.TipSuresi = dakika.Minutes == 0 ? 1 : dakika.Minutes;
                entity.SaveChanges();
                sonuc = TipSonuclandirma(aktifTest);

            }
            catch
            {
                sonuc = false;
            }
            return Json(sonuc, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> TipSinavCevap(int Personel_TipRef, int TipSoruRef, int? Cevap, int? xkordinat, int? ykordinat, bool? koordinatSecim)
        {
            string durum = string.Empty;
            try
            {
                IQueryable<SoruSiklari> sorusiklar = entity.SoruSiklaris.Where(r => r.ID == Cevap);
                var data = new TipCevaplar();
                data.Personel_Tip_Ref = Personel_TipRef;
                data.Tip_Sorular_Ref = TipSoruRef;
                data.GirilenCevap = Cevap;
                data.CevapResimX = xkordinat;
                data.CevapResimY = ykordinat;
                data.DogruCevap = await sorusiklar.Select(e => e.DogruSecenek).FirstOrDefaultAsync();
                data.KoordinatSecim = koordinatSecim;
                bool sonuc = false;

                var secenek = await sorusiklar.Where(r => r.DogruSecenek == true).Select(e => e.SoruSikki).FirstOrDefaultAsync();
                if (secenek == "a)")
                {
                    if (data.DogruCevap == true)
                    {
                        sonuc = true;
                    }
                    else
                    {
                        sonuc = false;
                    }
                }
                else
                {
                    if (data.DogruCevap == true && data.KoordinatSecim == true)
                    {
                        sonuc = true;
                    }
                    else
                    {
                        sonuc = false;
                    }
                }

                data.Netice = sonuc;
                entity.TipCevaplars.Add(data);
                await entity.SaveChangesAsync();
                durum = "Tamam";
            }
            catch
            {
                durum = "İşlem sırasında hata meydana geldi tekrar deneyiniz!";
            }
            return Json(durum, JsonRequestBehavior.AllowGet);
        }

        public bool TipSonuclandirma(Personel_TIP aktifTest)
        {
            bool durum = false;
            try
            {
                List<TestSonuclari> cevaplar = new List<TestSonuclari>();
                var testSorular = entity.Tip_Sorular.Where(r => r.TipSinavlar_Ref == aktifTest.TipSinavlar_Ref && r.IsActive == true && r.IsDelete == false).Select(w => new { w.ID, w.TipCevaplars }).ToList();
                foreach (var item2 in testSorular)
                {
                    var cevapkontrol = item2.TipCevaplars.Where(r => r.Personel_Tip_Ref == aktifTest.ID).Select(t => new { t.Netice, t.Tip_Sorular_Ref }).FirstOrDefault();
                    string cevabi = "";
                    if (cevapkontrol == null)
                    {
                        cevabi = "YOK";
                    }
                    else
                    {
                        if (cevapkontrol.Netice == true)
                        {
                            cevabi = "True";
                        }
                        else
                        {
                            cevabi = "False";
                        }
                    }
                    TestSonuclari datax = new TestSonuclari();
                    datax.PerID = aktifTest.ID;
                    datax.SoruID = item2.ID;
                    datax.Cevap = cevabi;
                    cevaplar.Add(datax);
                }

                int DogruSayisi = cevaplar.Where(t => t.Cevap == "True").Count();
                int YanlisSayisi = cevaplar.Where(t => t.Cevap == "False" || t.Cevap == "" || t.Cevap == null).Count();
                //int Bos = cevaplar.Where(t => t.Cevap == "YOK").Count();

                decimal TamPuan = 100;

                int ToplamSoru = testSorular.Count();

                decimal soruPuan = 0;
                if (ToplamSoru != 0)
                {
                    soruPuan = TamPuan / ToplamSoru;
                }

                decimal TestNotu = soruPuan * DogruSayisi;

                string Netice = "";
                if (TestNotu >= 70)
                {
                    Netice = "GEÇTİ";
                }
                else
                {
                    Netice = "KALDI";
                }

                Personel_TIP perTestIsleme = aktifTest;
                perTestIsleme.DogruSayisi = DogruSayisi;
                perTestIsleme.YanlisSayisi = YanlisSayisi;
                perTestIsleme.TipNotu = TestNotu;
                //perTestIsleme.Bos = Bos;
                //perTestIsleme.Durum = Netice;
                //perTestIsleme.TipAciklama = true;
                perTestIsleme.Netice = Netice;
                entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return durum;
        }

        public ActionResult LogYaz(int islemTur, int simulatorRef)
        {
            LogDetail dtl = new LogDetail();
            dtl.user_Ref = AktifKullanici.Aktif.User_Ref;
            dtl.IslemTuru = islemTur;
            dtl.Simulator_Kategori_Ref = simulatorRef;
            UserLog.Logwrite(dtl);
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LogBagaj()
        {
            int userRef = AktifKullanici.Aktif.User_Ref;
            UserLog.LogBaggage(userRef);
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GisCevaplarPersonel(int id)
        {
            return View(id);
        }

        protected override void Dispose(bool disposing)
        {
            entity.Dispose();
            base.Dispose(disposing);
        }
    }
}
