﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Haveak_Pro.Models;
using PagedList;

namespace Haveak_Pro.Areas.PANEL.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    [Authorize(Roles = UserRoleName.PANEL_AdminGiris + "," + UserRoleName.ADMIN_BagajTehlikelimadde)]
    public class GoruntuIslemlerController : Controller
    {
        //
        // GET: /Panel/GoruntuIslemler/
        public GoruntuIslemlerController()
        {
            AktifKullanici.Aktif.SecilenModul = "PANEL";
        }

        HaveakTurEntities Entity = new HaveakTurEntities();

        #region Kategori İşlemler


        public ActionResult TehlikeliMaddeKategori(int? page)
        {
            int pageSize = 100;
            int pageIndex = 1;
            int recordNo = 0;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<TehlikeliMaddeKategori> listb = Entity.TehlikeliMaddeKategoris.OrderByDescending(m => m.ID).Where(t => t.IsActive == true && t.IsDelete == false).ToPagedList(pageIndex, pageSize);
            if (pageIndex != 1)
            {
                recordNo = ((pageIndex - 1) * pageSize);
            }
            ViewBag.RecordNo = recordNo;
            return View(listb);
        }


        [HttpPost]
        public ActionResult SaveTMKategori(string Baslik, HttpPostedFileBase Resim)
        {
            TehlikeliMaddeKategori tip = new TehlikeliMaddeKategori();
            tip.IsActive = true;
            tip.IsDelete = false;
            tip.Baslik = Baslik;
            tip.Resim = savedocument(Resim);
            Entity.TehlikeliMaddeKategoris.Add(tip);
            Entity.SaveChanges();
            return RedirectToAction("TehlikeliMaddeKategori", "GoruntuIslemler", new { area = "Panel" });
        }

        public ActionResult UpdateTMKategori(int ID, string Baslik, HttpPostedFileBase Resim)
        {
            string result = "";
            try
            {
                var data = Entity.TehlikeliMaddeKategoris.Where(r => r.ID == ID).FirstOrDefault();
                data.Baslik = Baslik;
                if (Resim != null)
                {
                    var silDosya = Server.MapPath("~/" + data.Resim);
                    if (System.IO.File.Exists(silDosya))
                    {
                        System.IO.File.Delete(silDosya);
                    }
                    data.Resim = savedocument(Resim);
                }
                Entity.SaveChanges();
                result = "[{\"durum\":true}]";
            }
            catch (Exception)
            {
                result = "[{\"durum\":false}]";
            }
            ContentResult cr = new ContentResult();
            cr.ContentEncoding = System.Text.Encoding.UTF8;
            cr.ContentType = "application/json";
            cr.Content = result;
            return cr;
        }
        #endregion

        #region Kategori Madde İşlemler
        public ActionResult Maddeler()
        {
            List<Maddeler> Detay = null;
            //ViewBag.RecordNo = 0;
            ViewBag.TMkategori = Entity.TehlikeliMaddeKategoris.Where(t => t.IsActive == true && t.IsDelete == false).ToList();
            return View(Detay);
        }

        public ActionResult MaddelerAra(int? page, string search)
        {
            //int pageSize = 100;
            //int pageIndex = 1;
            //int recordNo = 0;
            //pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            //IPagedList<Maddeler> listb = Entity.Maddelers.OrderBy(m => m.ID).Where(t => t.ResimAd.Contains(search) && t.IsActive == true && t.IsDelete == false).ToPagedList(pageIndex, pageSize);
            //if (pageIndex != 1)
            //{
            //    recordNo = ((pageIndex - 1) * pageSize);
            //}
            List<Maddeler> listb = Entity.Maddelers.OrderBy(m => m.ID).Where(t => t.ResimAd.Contains(search) && t.IsActive == true && t.IsDelete == false).ToList();
            //ViewBag.RecordNo = recordNo;
            return View("Maddeler", listb);
        }

        public JsonResult DeleteMaddeler(int ID)
        {
            bool durum = false;
            var bgj = Entity.Maddelers.Where(t => t.ID == ID).FirstOrDefault();
            try
            {
                bgj.IsActive = false;
                bgj.IsDelete = true;
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public ActionResult _MaddeResimGetir(List<int> IDs)
        {
            List<Maddeler> bgjlar = Entity.Maddelers.Where(item => IDs.Contains(item.ID) || IDs.Contains(item.AltResimRef.Value)).ToList();
            return PartialView("_ResimKategori", bgjlar);
        }

        public ActionResult _MaddeAltResimGetir(int Id)
        {
            List<Maddeler> bgjlar = Entity.Maddelers.Where(i=> i.AltResimRef == Id).ToList();
            return PartialView("_AltKategoriResimler", bgjlar);
        }


        public JsonResult MaddeResimGrupla(List<ResimKategori> data)
        {
            bool durum = false;
            int seciliId = data.Where(w => w.statu == true).Select(q => q.id).FirstOrDefault();
            Maddeler anabgj = Entity.Maddelers.Where(t => t.ID == seciliId).FirstOrDefault();
            anabgj.AltResimRef = 0;
            Entity.SaveChanges();
            foreach (var item in data)
            {
                if (item.id != seciliId)
                {
                    Maddeler guncelleAlt = Entity.Maddelers.Where(w => w.ID == item.id).FirstOrDefault();
                    guncelleAlt.AltResimRef = seciliId;
                }
            }
            try
            {
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult MaddeGruptanCikart(int Id)
        {
            bool durum = false;
            var data = Entity.Maddelers.Where(e => e.ID == Id).FirstOrDefault();
            data.AltResimRef = null;
            durum = Entity.SaveChanges() > 0 ? true :false;
            return Json(durum, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateMaddeler(int ID, HttpPostedFileBase Resim)
        {
            bool durum = false;
            var madde = Entity.Maddelers.Where(t => t.ID == ID).FirstOrDefault();
            try
            {
                madde.ResimAd = Resim.FileName.Split('.')[0];
                if (Resim != null)
                {
                    var silDosya = Server.MapPath("~/" + madde.Resim);
                    if (System.IO.File.Exists(silDosya))
                    {
                        System.IO.File.Delete(silDosya);
                    }
                    madde.Resim = savedocument(Resim);
                }
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public ActionResult KategoriMaddelerGetir(int? KatID)
        {
            //int pageSize = 48;
            //int pageIndex = 1;
            //int recordNo = 0;
            //pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            List<Maddeler> Detay = Entity.Maddelers.OrderBy(t => t.ID).Where(t => t.IsActive == true && t.IsDelete == false && t.TehlikeliMadde_Ref == KatID && (t.AltResimRef == 0 || t.AltResimRef == null)).ToList();
            //if (pageIndex != 1)
            //{
            //    recordNo = ((pageIndex - 1) * pageSize);
            //}
            ViewBag.GelenKatID = KatID;
            //ViewBag.RecordNo = recordNo;
            ViewBag.TMkategori = Entity.TehlikeliMaddeKategoris.Where(t => t.IsActive == true && t.IsDelete == false).ToList();
            return View("Maddeler", Detay);
        }

        public JsonResult GrupSil(int Id)
        {
            bool durum = false;
            var data = Entity.Maddelers.Where(e => e.ID == Id).FirstOrDefault();
            if (data.AltResimRef == 0)
            {
                var altResimKkontrol = Entity.Maddelers.Where(q => q.AltResimRef == Id).Count();
                if (altResimKkontrol != 0)
                {
                    durum = false;
                }
                else
                {
                    data.AltResimRef = null;
                    Entity.SaveChanges();
                    durum = true;
                }
            }
            else
            {
                data.AltResimRef = null;
                Entity.SaveChanges();
                durum = true;
            }
            return Json(durum, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public ActionResult SaveMaddeler(HttpPostedFileBase[] Resim, int TehlikeliMadde_Ref)
        {
            foreach (var item in Resim)
            {
                Maddeler tip = new Maddeler();
                tip.IsActive = true;
                tip.IsDelete = false;
                tip.ResimAd = item.FileName.Split('.')[0];
                tip.Resim = savedocument(item);
                tip.TehlikeliMadde_Ref = TehlikeliMadde_Ref;
                Entity.Maddelers.Add(tip);
            }
            Entity.SaveChanges();
            return RedirectToAction("KategoriMaddelerGetir", "GoruntuIslemler", new { area = "Panel", page = 1, KatID = TehlikeliMadde_Ref });
        }

        #endregion

        //protected string savedocumentTm(HttpPostedFileBase document, string folderName)
        //{
        //    if (document != null)
        //    {
        //        if (!Directory.Exists(Server.MapPath("~/DocumentFiles/imagebank/" + folderName)))
        //        {
        //            Directory.CreateDirectory(Server.MapPath("~/DocumentFiles/imagebank/" + folderName));
        //        }
        //        FileInfo f = new FileInfo(document.FileName);
        //        string Fname = Guid.NewGuid() + f.Name;
        //        string path = "DocumentFiles/imagebank/" + folderName + "/" + Fname;
        //        //f.Directory.Create();
        //        document.SaveAs(Server.MapPath("~/" + path));
        //        return path;
        //    }
        //    else
        //    {
        //        return "";
        //    }

        //}

        protected string savedocument(HttpPostedFileBase document)
        {
            if (document != null)
            {
                FileInfo f = new FileInfo(document.FileName);
                string Fname = Guid.NewGuid() + f.Name;
                string path = "DocumentFiles/imagebank/" + Fname;
                f.Directory.Create();
                document.SaveAs(Server.MapPath("~/" + path));
                return path;
            }
            else
            {
                return "";
            }

        }

        protected override void Dispose(bool disposing)
        {
            Entity.Dispose();
            base.Dispose(disposing);
        }
    }

    public class ResimKategori
    {
        public int id { get; set; }
        public bool statu { get; set; }
    }
}
