﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Haveak_Pro.Models;
using System.Data.Entity;
using System.Web.Security;
using System.Text;
using System.Threading.Tasks;


namespace Haveak_Pro.Areas.PANEL.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class HomeController : Controller
    {
        //
        // GET: /Panel/Home/

        HaveakTurEntities Entity = new HaveakTurEntities();
        public HomeController()
        {
            AktifKullanici.Aktif.SecilenModul = "PANEL";

        }

        private DateTime bugun = DateTime.Now;
        private int gun = DateTime.Now.Day;
        private int ay = DateTime.Now.Month;
        private int yil = DateTime.Now.Year;
        private int userRef = AktifKullanici.Aktif.Id;

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris + "," + UserRoleName.ADMIN_BagajTehlikelimadde + "," + UserRoleName.ADMIN_Egitim + "," + UserRoleName.ADMIN_PersonelKullanici + "," + UserRoleName.ADMIN_Sinav)]
        public async Task<ActionResult> Index()
        {
            ViewBag.Sinavlar = await Entity.Personel_Sinavlar.Where(t => t.SinavAciklama == true && t.IsDelete == false).Select(r => r.Durum).ToListAsync();
            var sorular =  await Entity.SinavTipis.ToListAsync();
            var listSoru = sorular.Where(t => t.UniteSorulars.Where(r => r.IsActive == true && r.IsDelete == false).Count() > 0).Select(q => q.UniteSorulars).ToList();

            List<SinavDurum> datac = new List<SinavDurum>();
            foreach (var items in listSoru)
            {
                var datax = items.GroupBy(t => new { t.SinavTipi.Baslik, t.SinavTipi_Ref }).Select(q => new SinavDurum { Baslik = q.Key.Baslik, sinavRef = q.Count() }).FirstOrDefault();
                datac.Add(datax);
            }
            ViewBag.Sorular = datac;
            Loglama log = await Entity.Loglamas.OrderByDescending(w=>w.ID).Where(w =>
                 w.Kullanici_Ref == userRef &&
                 w.GirisTarihi.Day == gun &&
                 w.GirisTarihi.Month == ay &&
                 w.GirisTarihi.Year == yil).FirstOrDefaultAsync();

            Loglama logdetay = new Loglama();
            if (log == null || (log != null && log.CikisSaati != null))
            {

                logdetay.IsActive = true;
                logdetay.IsDelete = false;
                logdetay.Kullanici_Ref = userRef;
                logdetay.GirisTarihi = bugun;
                logdetay.YapilanIs = "Admin İşlem";
                Entity.Loglamas.Add(logdetay);
                await Entity.SaveChangesAsync();
            }           

            ViewBag.Nuctech = await Entity.Bagajlars.Where(t => t.IsActive == true && t.IsDelete == false).Select(r => r.ID).CountAsync();
            ViewBag.Hieman = await Entity.HBagajlars.Where(t => t.IsActive == true && t.IsDelete == false).Select(r => r.ID).CountAsync();
            ViewBag.Rapiscan = await Entity.RBagajlars.Where(t => t.IsActive == true && t.IsDelete == false).Select(r => r.ID).CountAsync();
            ViewBag.SBagaj = await Entity.SBagajlars.Where(t => t.IsActive == true && t.IsDelete == false).Select(r => r.ID).CountAsync();
            ViewBag.Madde = await Entity.Maddelers.Where(t => t.IsActive == true && t.IsDelete == false).ToListAsync();
            ViewBag.TMaddeler = await Entity.SBMaddeResimlers.Where(t => t.IsActive == true && t.IsDelete == false).ToListAsync();

            ViewBag.TMKategori = await Entity.TehlikeliMaddeKategoris.Where(e => e.IsActive == true && e.IsDelete == false).ToListAsync();
            ViewBag.STMKategori = await Entity.SBTehlikeliMKategoris.Where(e => e.IsActive == true && e.IsDelete == false).ToListAsync();

           
            int toplamUyesayi = 0; 
            IQueryable<Kullanici> uyeler = Entity.Kullanicis.Where(r => r.IsActive == true && r.IsDelete == false && r.Songiristarihi.Value == bugun.Date);
            if (UserRoleName.PANEL_AdminGiris.InRole())
            {
                ViewBag.AktifUyeler = await  uyeler.Where(r => r.Sistemde == true).ToListAsync();
                toplamUyesayi = await uyeler.CountAsync();
            }
            else
            {
                ViewBag.AktifUyeler = await uyeler.Where(w => w.Sistemde == true && w.Personel.Firma_Ref == AktifKullanici.Aktif.FirmaId).ToListAsync();
                toplamUyesayi = await uyeler.Where(w=>w.Personel.Firma_Ref== AktifKullanici.Aktif.FirmaId).CountAsync();
            }
            ViewBag.TumKullanici = toplamUyesayi;
            return View();
        }

        #region Resim Tip İşlemler
        //[Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        //public ActionResult ResimTip()
        //{
        //    var Detay = Entity.ResimTipis.ToList();
        //    return View(Detay);
        //}
        //[HttpPost]
        //public ActionResult SaveResimTip(string ResimTip)
        //{
        //    ResimTipi tip = new ResimTipi();
        //    tip.IsActive = true;
        //    tip.IsDelete = false;
        //    tip.TipAdi = ResimTip;
        //    Entity.ResimTipis.Add(tip);
        //    Entity.SaveChanges();
        //    return RedirectToAction("ResimTip", "Home", new { area = "Panel" });
        //}

        //public ActionResult UpdateResimTip(int ID, string ResimTipUp, bool IsActive, bool IsDelete)
        //{
        //    string result = "";
        //    try
        //    {
        //        var data = Entity.ResimTipis.Where(r => r.ID == ID).FirstOrDefault();
        //        data.TipAdi = ResimTipUp;
        //        data.IsActive = IsActive;
        //        data.IsDelete = IsDelete;
        //        Entity.SaveChanges();
        //        result = "[{\"durum\":true}]";
        //    }
        //    catch (Exception)
        //    {
        //        result = "[{\"durum\":false}]";
        //    }
        //    ContentResult cr = new ContentResult();
        //    cr.ContentEncoding = System.Text.Encoding.UTF8;
        //    cr.ContentType = "application/json";
        //    cr.Content = result;
        //    return cr;
        //}
        #endregion


        public JsonResult SenMassageUser(int Personel_Ref, string Icerik, int Alici_Ref)
        {
            int kullaniciID = Entity.Kullanicis.Where(y => y.Personel_Ref == Personel_Ref).Select(r => r.ID).FirstOrDefault();
            var control = Entity.Kull_Rolleri.Where(t => t.User_Ref == kullaniciID).ToList();
            bool result = false;
            try
            {
                var data = new Mesajlar();
                data.IsActive = true;
                data.IsDelete = false;
                data.Alici_Ref = Alici_Ref;
                data.Icerik = Icerik;
                data.Tarih = DateTime.Now;
                Entity.Mesajlars.Add(data);
                Entity.SaveChanges();
                result = true;
            }
            catch
            {
                result = false;
            }

            return Json(result, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }


        protected override void Dispose(bool disposing)
        {
            Entity.Dispose();
            base.Dispose(disposing);
        }
    }
    public class SinavDurum
    {
        public int sinavRef { get; set; }
        public string Baslik { get; set; }
    }

    public class TMKategoriler
    {
        public string Kategori { get; set; }
        public int Adet { get; set; }
    }
}
