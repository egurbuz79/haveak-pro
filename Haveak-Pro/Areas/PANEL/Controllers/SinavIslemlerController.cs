﻿using Haveak_Pro.Areas.PANEL.Models;
using Haveak_Pro.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace Haveak_Pro.Areas.PANEL.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    [Authorize(Roles = UserRoleName.ADMIN_Sinav + "," + UserRoleName.PANEL_AdminGiris)]
    public class SinavIslemlerController : Controller
    {
        //
        // GET: /Panel/SınavIslemler/
        HaveakTurEntities Entity = new HaveakTurEntities();
        int? firmaRef;
        public SinavIslemlerController()
        {
            AktifKullanici.Aktif.SecilenModul = "PANEL";
            firmaRef = AktifKullanici.Aktif.FirmaId;
        }

        public async Task<ActionResult> YeniSoruTanimlama()
        {
            ViewBag.Uniteler = await Entity.Unites.Where(r => r.IsActive == true && r.IsDelete == false).ToListAsync();
            ViewBag.UniteModuller = await Entity.UniteModuls.Where(r => r.IsActive == true && r.IsDelete == false).ToListAsync();
            ViewBag.STMKategori = await Entity.SBTehlikeliMKategoris.Where(t => t.IsActive == true && t.IsDelete == false).ToListAsync();
            ViewBag.SBagajKategori = await Entity.SBagajKategoris.Where(t => t.IsActive == true && t.IsDelete == false).ToListAsync();
            ViewBag.Sorukategori = await Entity.SoruKategoris.ToListAsync();
            ViewBag.SinavTipi = await Entity.SinavTipis.ToListAsync();
            return View();
        }

        public async Task<ActionResult> _KategoriBagajlar(int kategori_Ref, int? bagajId)
        {
            var data = await Entity.SBagajlars.Where(t => t.SKategori_Ref == kategori_Ref && t.IsActive == true && t.IsDelete == false).ToListAsync();
            TempData["bagajId"] = bagajId;
            return PartialView("_KategoriSBagajlar", data);
        }

        public ActionResult _KategoriMaddeler(int madde_Ref, int? smaddeId)
        {
            List<SBMaddeResimler> mdata = new List<SBMaddeResimler>();
            IQueryable<SBMaddeResimler> data = Entity.SBMaddeResimlers.Where(t => t.SBTM_Ref == madde_Ref);
            SBMaddeResimler orjmadde = null;
            if (smaddeId != null)
            {
                orjmadde = data.Where(t => t.ID == smaddeId).FirstOrDefault();
                mdata.Add(orjmadde);
            }
            mdata.AddRange(data.Where(t => t.IsActive && !t.IsDelete).ToList());
            ViewData["smaddeId"] = smaddeId;
            return PartialView("_KategoriSMaddeler", mdata);
        }


        public async Task<JsonResult> SoruMaddeKontrol(int bagajRef, int maddeRef)
        {
            var kontrol = await Entity.UniteSorulars.Where(t => t.SBagajlar_Ref == bagajRef && t.STM_Ref == maddeRef).CountAsync();
            return Json(kontrol, JsonRequestBehavior.AllowGet);

        }

        public async Task<ActionResult> SinavCevaplar(int id)
        {
            var pcevap = await Entity.SinavCevaplars.Where(y => y.Personel_Sinavlar_Ref == id).ToListAsync();
            return View(pcevap);
        }

        public async Task<ActionResult> SoruGuncelle(int id)
        {
            ViewBag.UniteModuller = await Entity.UniteModuls.Where(r => r.IsActive == true && r.IsDelete == false).ToListAsync();
            ViewBag.STMKategori = await Entity.SBTehlikeliMKategoris.Where(t => t.IsActive == true && t.IsDelete == false).ToListAsync();
            ViewBag.SBagajKategori = await Entity.SBagajKategoris.Where(t => t.IsActive == true && t.IsDelete == false).ToListAsync();
            ViewBag.SoruKategori = await Entity.SoruKategoris.ToListAsync();
            //ViewBag.SinavSorular = Entity.BaslikMaddes.Where(t => t.KategoriMadde_Ref == 1007).ToList();
            ViewBag.SinavTipi = await Entity.SinavTipis.ToListAsync();
            var data = await Entity.UniteSorulars.Where(g => g.ID == id && g.IsActive == true).FirstOrDefaultAsync();
            return View(data);
        }

        public string ServerTime()
        {
            return DateTime.Now.ToString();
        }

        [HttpPost]
        public async Task<ActionResult> SoruKaydet(int SinavTipi_Ref, int SoruKategori_Ref, int? Unite_Ref, string Soru, List<string> SoruSikki, List<string> CevapIcerik, List<bool> DogruSecenek,
                                     int? SBagajlarData, int? STMData, string XKordinat, string YKordinat, List<string> AltSoruSikki, List<string> AltCevapIcerik,
                                     List<bool> AltDogruSecenek, decimal? TMOlcek, decimal? TMVektor, decimal? TMSaydamlik, decimal? TMIsik)
        {

            UniteSorular soruDetay = new UniteSorular();
            soruDetay.IsActive = true;
            soruDetay.IsDelete = false;
            if (SBagajlarData != null)
            {
                soruDetay.SBagajlar_Ref = SBagajlarData.Value;
            }
            if (STMData != null)
            {
                soruDetay.STM_Ref = STMData;
            }
            soruDetay.Soru = Soru;
            soruDetay.Unite_Ref = Unite_Ref.Value;
            soruDetay.SinavTipi_Ref = SinavTipi_Ref;
            if (XKordinat != "" && YKordinat != "")
            {
                soruDetay.XKordinat = Convert.ToDecimal(XKordinat.Replace('.', ','));
                soruDetay.YKordinat = Convert.ToDecimal(YKordinat.Replace('.', ','));
                soruDetay.TMOlcek = TMOlcek;
                soruDetay.TMVektor = TMVektor;
                soruDetay.TMSaydamlik = TMSaydamlik;
                soruDetay.TMIsik = TMIsik;
            }
            soruDetay.SoruKategori_Ref = SoruKategori_Ref;
            Entity.UniteSorulars.Add(soruDetay);
            await Entity.SaveChangesAsync();

            if (SoruSikki != null)
            {
                var tSoruSikki = SoruSikki.Select((z, Index) => new { Name = z, Ix = Index });
                var tCevapIcerik = CevapIcerik.Select((z, Index) => new { Name = z, Ix = Index });
                var tDogruSecenek = DogruSecenek.Select((z, Index) => new { Name = z, Ix = Index });
                var Sorular = from SoruSikkix in tSoruSikki
                              join CevapIcerikx in tCevapIcerik on SoruSikkix.Ix equals CevapIcerikx.Ix
                              join DogruSecenekx in tDogruSecenek on SoruSikkix.Ix equals DogruSecenekx.Ix
                              select new SoruSiklari
                              {
                                  IsActive = true,
                                  IsDelete = false,
                                  CevapIcerik = CevapIcerikx.Name,
                                  UniteSorular_Ref = soruDetay.ID,
                                  DogruSecenek = DogruSecenekx.Name,
                                  SoruSikki = SoruSikkix.Name
                              };
                foreach (var itemSoru in Sorular)
                {
                    Entity.SoruSiklaris.Add(itemSoru);
                }
                await Entity.SaveChangesAsync();

                if (AltSoruSikki != null)
                {
                    var tAltSoruSikki = AltSoruSikki.Select((z, Index) => new { Name = z, Ix = Index });
                    var tAltCevapIcerik = AltCevapIcerik.Select((z, Index) => new { Name = z, Ix = Index });
                    var tAltDogruSecenek = AltDogruSecenek.Select((z, Index) => new { Name = z, Ix = Index });
                    var AltSorular = from AltSoruSikkix in tAltSoruSikki
                                     join AltCevapIcerikx in tAltCevapIcerik on AltSoruSikkix.Ix equals AltCevapIcerikx.Ix
                                     join AltDogruSecenekx in tAltDogruSecenek on AltSoruSikkix.Ix equals AltDogruSecenekx.Ix
                                     select new AltSiklar
                                     {
                                         IsActive = true,
                                         IsDelete = false,
                                         AltCevapIcerik = AltCevapIcerikx.Name,
                                         UniteSorular_Ref = soruDetay.ID,
                                         AltDogruSecenek = AltDogruSecenekx.Name,
                                         AltSoruSikki = AltSoruSikkix.Name
                                     };
                    foreach (var itemAltSoru in AltSorular)
                    {
                        Entity.AltSiklars.Add(itemAltSoru);
                    }
                    Entity.SaveChanges();
                }
            }

            return RedirectToAction("YeniSoruTanimlama", "SinavIslemler", new { area = "Panel" });
        }

        public JsonResult OtomatikSoruListe(int soruadet, int uniteId)
        {
            var kategoriIds = Entity.SBTehlikeliMKategoris.Where(t => t.IsActive && !t.IsDelete).Select(y => y.ID).ToList();
            kategoriIds.Add(0); //Temiz Bagaj
            List<MaddeOranlarView> data = new OtomatikSinavOranlama().OtomatikUygSinavOlustur(soruadet, kategoriIds, uniteId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SoruAktifPasif(int ID, bool aktif)
        {
            var data = await Entity.UniteSorulars.Where(t => t.ID == ID).FirstOrDefaultAsync();
            data.IsActive = aktif;
            await Entity.SaveChangesAsync();
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        //public JsonResult SoruGuncelleGenelUyg(int ID, int Unite_Ref, string Soru, int rm_Id, int? XBaslangic, int? XBitis, int? YBaslangic, int? YBitis)
        //{
        //    string kaydet = "";
        //    UniteSorular soruDetay = Entity.UniteSorulars.Where(r => r.ID == ID).First();        
        //    soruDetay.BaslikMadde_Ref = rm_Id;            
        //    soruDetay.Soru = Soru;
        //    soruDetay.Unite_Ref = Unite_Ref;
        //    soruDetay.XBaslangic = XBaslangic;
        //    soruDetay.XBitis = XBitis;
        //    soruDetay.YBaslangic = YBaslangic;
        //    soruDetay.YBitis = YBitis;         
        //    try
        //    {
        //        Entity.SaveChanges();  
        //        kaydet = "Güncelleme Başarılı";
        //    }
        //    catch
        //    {
        //        kaydet = "Güncelem Sırasında Bir hata oluştu tekrar deneyiniz! (Genel)";
        //    }
        //    Entity.SaveChanges();

        //    return Json(kaydet,  JsonRequestBehavior.AllowGet);
        //}

        public async Task<JsonResult> SoruGuncelleGenel(int ID, int SinavTipi_Ref, int SoruKategori_Ref, int? Unite_Ref, string Soru, int? SBagajlar_Ref, int? STM_Ref, string XKordinat, string YKordinat, decimal? TMOlcek, decimal? TMVektor, decimal? TMSaydamlik, decimal? TMIsik)
        {
            bool kaydet = false;
            UniteSorular soruDetay = await Entity.UniteSorulars.Where(r => r.ID == ID).FirstAsync();
            soruDetay.Soru = Soru;
            soruDetay.Unite_Ref = Unite_Ref == null ? 0 : Unite_Ref.Value;
            soruDetay.SoruKategori_Ref = SoruKategori_Ref;
            if (SinavTipi_Ref == (int)Enumlar.SoruTipleri.UYGULAMA || SinavTipi_Ref == (int)Enumlar.SoruTipleri.TIP || SinavTipi_Ref == (int)Enumlar.SoruTipleri.TIP_EGITIM)
            {
                soruDetay.SBagajlar_Ref = SBagajlar_Ref;
                if (STM_Ref != 0)
                {
                    soruDetay.STM_Ref = STM_Ref;
                    soruDetay.XKordinat = Convert.ToDecimal(XKordinat.Replace('.', ','));
                    soruDetay.YKordinat = Convert.ToDecimal(YKordinat.Replace('.', ','));
                    soruDetay.TMOlcek = TMOlcek;
                    soruDetay.TMVektor = TMVektor;
                    soruDetay.TMIsik = TMIsik;
                    soruDetay.TMSaydamlik = TMSaydamlik;
                }
            }
            try
            {
                kaydet = true;
            }
            catch
            {
                kaydet = false;
            }
            await Entity.SaveChangesAsync();
            return Json(kaydet, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SoruGuncelleSiklar(int ID, string SoruSikki, string CevapIcerik, bool DogruSecenek)
        {
            bool kaydet = false;
            SoruSiklari detay = await Entity.SoruSiklaris.Where(t => t.ID == ID).FirstAsync();
            detay.CevapIcerik = CevapIcerik;
            detay.DogruSecenek = DogruSecenek;
            detay.SoruSikki = SoruSikki;
            try
            {
                await Entity.SaveChangesAsync();

                List<int> idx = await Entity.SoruSiklaris.Where(t => t.UniteSorular_Ref == detay.UniteSorular_Ref && t.IsActive == true).Select(y => y.ID).ToListAsync();
                foreach (var item in idx.Select((r, v) => new { value = r, Index = v }))
                {
                    SoruSiklari detay2 = await Entity.SoruSiklaris.Where(t => t.ID == item.value).FirstAsync();
                    detay2.DogruSecenek = false;
                    if (detay2.ID == detay.ID)
                    {
                        detay2.DogruSecenek = DogruSecenek;
                    }
                    await Entity.SaveChangesAsync();
                }
                kaydet = true;
            }
            catch
            {
                kaydet = false;
            }

            return Json(kaydet, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SoruEkleSiklar(int UniteSorular_Ref, string SoruSikki, string CevapIcerik, bool DogruSecenek)
        {
            bool kaydet = false;
            SoruSiklari detay = new SoruSiklari();
            detay.IsActive = true;
            detay.IsDelete = false;
            detay.UniteSorular_Ref = UniteSorular_Ref;
            detay.CevapIcerik = CevapIcerik;
            detay.DogruSecenek = DogruSecenek;
            detay.SoruSikki = SoruSikki;
            try
            {
                Entity.SoruSiklaris.Add(detay);
                await Entity.SaveChangesAsync();
                kaydet = true;
            }
            catch
            {
                kaydet = false;
            }
            return Json(kaydet, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SoruSiklarSil(int ID)
        {
            string kaydet = "";
            SoruSiklari detay = await Entity.SoruSiklaris.Where(t => t.ID == ID).FirstAsync();
            detay.IsActive = false;
            detay.IsDelete = true;
            detay.DogruSecenek = false;
            try
            {
                await Entity.SaveChangesAsync();
                string[] siklar = { "a)", "b)", "c)", "d)", "e)", "f)", "g)" };
                IList<int> idx = await Entity.SoruSiklaris.Where(t => t.UniteSorular_Ref == detay.UniteSorular_Ref && t.IsActive == true && t.IsDelete == false).Select(y => y.ID).ToListAsync();
                foreach (var item in idx.Select((r, v) => new { value = r, Index = v }))
                {
                    SoruSiklari detay2 = await Entity.SoruSiklaris.Where(t => t.ID == item.value && t.IsActive == true && t.IsDelete == false).FirstOrDefaultAsync();
                    if (detay2 != null)
                    {
                        detay2.DogruSecenek = false;
                        if (item.Index == 0)
                        {
                            detay2.DogruSecenek = true;
                        }
                        await Entity.SaveChangesAsync();
                    }

                }
                foreach (var itemCevap in idx.Select((r, v) => new { value = r, Index = v }))
                {
                    SoruSiklari detay2Cevap = await Entity.SoruSiklaris.Where(t => t.ID == itemCevap.value && t.IsActive == true && t.IsDelete == false).FirstOrDefaultAsync();
                    if (detay2Cevap != null)
                    {
                        detay2Cevap.SoruSikki = siklar[itemCevap.Index];
                        await Entity.SaveChangesAsync();
                    }
                }

                kaydet = "Soru Sikki Başarı ile güncellendi";
            }
            catch
            {
                kaydet = "Güncelleme Sırasında bir hata oluştu  tekrar deneyiniz! (Soru Şıkkı)";
            }


            return Json(kaydet, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SoruEkleAltSiklar(int UniteSorular_Ref, string AltSoruSikki, string AltCevapIcerik, bool AltDogruSecenek)
        {
            bool kaydet = false;
            AltSiklar detay = new AltSiklar();
            detay.IsActive = true;
            detay.IsDelete = false;
            detay.UniteSorular_Ref = UniteSorular_Ref;
            detay.AltCevapIcerik = AltCevapIcerik;
            detay.AltDogruSecenek = AltDogruSecenek;
            detay.AltSoruSikki = AltSoruSikki;
            try
            {
                Entity.AltSiklars.Add(detay);
                await Entity.SaveChangesAsync();
                kaydet = true;
                //kaydet += detay.ID;
                //kaydet += ",Alt Soru Sikki Başarı ile Eklendi";
            }
            catch
            {
                kaydet = false;
            }


            return Json(kaydet, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SoruGuncelleAltSiklar(int ID, string AltSoruSikki, string AltCevapIcerik, bool AltDogruSecenek)
        {
            string kaydet = "";
            AltSiklar detay = await Entity.AltSiklars.Where(t => t.ID == ID).FirstAsync();
            detay.AltCevapIcerik = AltCevapIcerik;
            detay.AltDogruSecenek = AltDogruSecenek;
            detay.AltSoruSikki = AltSoruSikki;
            try
            {
                await Entity.SaveChangesAsync();

                List<int> idx = await Entity.AltSiklars.Where(t => t.UniteSorular_Ref == detay.UniteSorular_Ref).Select(y => y.ID).ToListAsync();
                foreach (var item in idx.Select((r, v) => new { value = r, Index = v }))
                {
                    AltSiklar detay2 = await Entity.AltSiklars.Where(t => t.ID == item.value).FirstAsync();
                    detay2.AltDogruSecenek = false;
                    if (detay2.ID == detay.ID)
                    {
                        detay2.AltDogruSecenek = AltDogruSecenek;
                    }
                    await Entity.SaveChangesAsync();
                }
                kaydet = "Soru Sikki Başarı ile güncellendi";
            }
            catch
            {
                kaydet = "Güncelleme Sırasında bir hata oluştu  tekrar deneyiniz! (Soru Şıkkı)";
            }


            return Json(kaydet, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AltSoruSikkiGuncelle(int ID)
        {
            bool kaydet = false;
            AltSiklar detay = Entity.AltSiklars.Where(t => t.ID == ID && t.IsActive == true && t.IsDelete == false).First();
            try
            {
                var detay2 = Entity.AltSiklars.Where(t => t.UniteSorular_Ref == detay.UniteSorular_Ref && t.IsActive == true && t.IsDelete == false).ToList();
                foreach (var item in detay2)
                {
                    item.AltDogruSecenek = false;
                    Entity.SaveChanges();
                }
                detay.AltDogruSecenek = true;
                Entity.SaveChanges();
                kaydet = true;
            }
            catch
            {
                kaydet = false;
            }
            return Json(kaydet, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SoruSikkiGuncelle(int ID)
        {
            bool kaydet = false;
            SoruSiklari detay = Entity.SoruSiklaris.Where(t => t.ID == ID).First();
            try
            {
                var detay2 = Entity.SoruSiklaris.Where(t => t.UniteSorular_Ref == detay.UniteSorular_Ref && t.IsActive == true && t.IsDelete == false).ToList();
                foreach (var item in detay2)
                {
                    item.DogruSecenek = false;
                    Entity.SaveChanges();
                }
                detay.DogruSecenek = true;
                Entity.SaveChanges();
                kaydet = true;
            }
            catch
            {
                kaydet = false;
            }
            return Json(kaydet, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SinavSilme(int sinavID)
        {
            bool durum = false;
            try
            {
                var durumKontrol = Entity.Sinavlars.Where(t => t.IsActive == true && t.IsDelete == false && t.ID == sinavID && t.Personel_Sinavlar.Count == 0).FirstOrDefault();
                if (durumKontrol != null)
                {
                    durumKontrol.IsActive = false;
                    durumKontrol.IsDelete = true;
                    Entity.SaveChanges();
                    durum = true;
                }
            }
            catch
            {
                durum = false;
            }
            return Json(durum, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SoruAltSiklarSil(int ID)
        {
            bool kaydet = false;
            AltSiklar detay = Entity.AltSiklars.Where(t => t.ID == ID).First();
            detay.IsActive = false;
            detay.IsDelete = true;
            try
            {
                Entity.SaveChanges();
                string[] siklar = { "a)", "b)", "c)", "d)", "e)", "f)", "g)" };
                List<int> idx = Entity.AltSiklars.Where(t => t.UniteSorular_Ref == detay.UniteSorular_Ref && t.IsActive == true && t.IsDelete == false).Select(y => y.ID).ToList(); ;
                foreach (var item in idx.Select((r, v) => new { value = r, Index = v }))
                {
                    AltSiklar detay2 = Entity.AltSiklars.Where(t => t.ID == item.value).First();
                    detay2.AltDogruSecenek = false;
                    if (item.Index == 0)
                    {
                        detay2.AltDogruSecenek = true;
                    }
                    detay2.AltSoruSikki = siklar[item.Index];
                    Entity.SaveChanges();
                }

                foreach (var itemCevap in idx.Select((r, v) => new { value = r, Index = v }))
                {
                    AltSiklar detay2Cevap = Entity.AltSiklars.Where(t => t.ID == itemCevap.value && t.IsActive == true && t.IsDelete == false).FirstOrDefault();
                    if (detay2Cevap != null)
                    {
                        detay2Cevap.AltSoruSikki = siklar[itemCevap.Index];
                        Entity.SaveChanges();
                    }
                }
                kaydet = true;

            }
            catch
            {
                kaydet = false;
            }


            return Json(kaydet, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TanimliSorular(int? page, int? soruTip, int? sBagajKategori, string aranan, int? soruNo)
        {
            ViewBag.SinavTipi = Entity.SinavTipis.ToList();
            ViewBag.SBagajKategori = Entity.SBagajKategoris.Where(e => e.IsActive && !e.IsDelete).ToList();

            soruTip = soruTip.HasValue ? Convert.ToInt32(soruTip) : -1;
            sBagajKategori = sBagajKategori.HasValue ? Convert.ToInt32(sBagajKategori) : 0;

            ViewBag.Sinavlar = Entity.Sinavlars.Where(t => t.IsActive == true).ToList();
            ViewBag.Uniteler = Entity.Unites.Where(r => r.IsActive == true && r.IsDelete == false).ToList();
            IPagedList<UniteSorular> list = null;
            int pageSize = 100;
            int pageIndex = 1;
            int recordNo = 0;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IQueryable<UniteSorular> listGnl = Entity.UniteSorulars.OrderBy(m => m.ID).Where(t => t.IsActive == true && t.IsDelete == false);
            if (!string.IsNullOrEmpty(aranan))
            {
                listGnl = listGnl.Where(r => r.Soru.Contains(aranan));
            }
            if (soruTip != -1)
            {
                if (sBagajKategori == 0)
                {
                    list = listGnl.Where(t => t.SinavTipi_Ref == soruTip).ToPagedList(pageIndex, pageSize);
                }
                else
                {
                    list = listGnl.Where(t => t.SinavTipi_Ref == soruTip && t.SBagajlar.SKategori_Ref == sBagajKategori).ToPagedList(pageIndex, pageSize);
                }
            }
            else
            {
                if (sBagajKategori == 0)
                {
                    list = listGnl.ToPagedList(pageIndex, pageSize);
                }
                else
                {
                    list = listGnl.Where(q => q.SBagajlar.SKategori_Ref == sBagajKategori).ToPagedList(pageIndex, pageSize);
                }
            }
            if (soruNo != null)
            {
                list = listGnl.Where(t => t.ID == soruNo).ToPagedList(pageIndex, pageSize);
            }
            if (pageIndex != 1)
            {
                recordNo = ((pageIndex - 1) * pageSize);
            }
            ViewBag.RecordNo = recordNo;
            ViewBag.SinavTipDn = soruTip;
            ViewBag.SBagajKategoriDn = sBagajKategori;
            ViewBag.ArananDn = aranan;
            ViewBag.SoruNo = soruNo;
            return View(list);
        }

        public ActionResult TSoruGuncelle(int id)
        {
            ViewBag.Uniteler = Entity.Unites.Where(r => r.IsActive == true && r.IsDelete == false).ToList();
            UniteSorular data = Entity.UniteSorulars.Where(t => t.ID == id).FirstOrDefault();
            //ViewBag.usiklar = Entity.SoruSiklaris.Where(r => r.UniteSorular_Ref == data.ID && r.IsActive == true).Select(t => new SoruSiklariLocal { ID = t.ID, SoruSikki = t.SoruSikki, CevapIcerik = t.CevapIcerik, DogruSecenek = t.DogruSecenek }).ToList();
            //ViewBag.altSiklar = Entity.AltSiklars.Where(t => t.UniteSorular_Ref == data.ID && t.IsActive == true).ToList();

            return View(data);
        }

        public ActionResult SoruIcerik(int id)
        {
            ViewBag.list = Entity.UniteSorulars.Where(t => t.Unite_Ref == id).Select(r => new { r.ID, r.IsActive, r.SinavTipi_Ref, r.Soru }).ToList();
            return View();

        }

        #region Sınav Tanımlama
        public ActionResult Sinavlar()
        {
            IQueryable<Sinavlar> data = Entity.Sinavlars.Where(r => r.IsActive == true && r.IsDelete == false);
            IQueryable<Firmalar> firma = Entity.Firmalars.Where(r => r.IsActive == true && r.IsDelete == false);
            try
            {
                if (!UserRoleName.PANEL_AdminGiris.InRole())
                {
                    if (firmaRef != null)
                    {
                        data = data.Where(r => r.Firma_Ref == firmaRef);
                        firma = firma.Where(r => r.ID == firmaRef);
                    }
                }
            }
            catch
            {
            }
            ViewBag.firma = firma.ToList();
            return View(data.ToList());
        }

        public JsonResult SinavlarGuncelle(int id, string SinavNo, string SoruSayisi, string Aciklama, byte SinavTipi, int? Firma_Ref, bool Yayinda)
        {
            bool durum = false;
            try
            {
                var data = Entity.Sinavlars.Where(t => t.ID == id).FirstOrDefault();
                data.SinavNo = SinavNo;
                data.SoruSayisi = int.Parse(SoruSayisi);
                data.Aciklama = Aciklama;
                data.SinavTipi = SinavTipi;
                data.Yayinda = Yayinda;
                try
                {
                    data.Firma_Ref = Firma_Ref;
                }
                catch
                {
                }
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SinavlarSil(int id)
        {
            bool durum = false;
            try
            {
                var data = Entity.Sinavlars.Where(t => t.ID == id).FirstOrDefault();
                data.IsActive = false;
                data.IsDelete = true;
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SinavKaydet(string SinavNo, int SoruSayisi, string Aciklama, byte SinavTipi, int? Firma_Ref, int? Yayinda)
        {
            var kontrol = Entity.Sinavlars.Where(y => y.SinavNo == SinavNo).Count();
            if (kontrol == 0)
            {
                Sinavlar data = new Sinavlar();
                data.IsActive = true;
                data.IsDelete = false;
                data.SinavNo = SinavNo;
                data.SoruSayisi = SoruSayisi;
                data.Aciklama = Aciklama;
                data.SinavTipi = SinavTipi;
                data.Yayinda = Yayinda == 1 ? true : false;
                try
                {
                    data.Firma_Ref = firmaRef;
                }
                catch
                {
                }
                Entity.Sinavlars.Add(data);
                Entity.SaveChanges();
            }
            return RedirectToAction("Sinavlar", "SinavIslemler", new { area = "Panel" });
        }
        #endregion

        public ActionResult SinavSorular(int id)
        {
            ViewData["SinavTipi"] = Entity.SinavTipis.ToList();
            ViewData["SoruKategori"] = Entity.SoruKategoris.ToList();
            var sinavlar = Entity.Sinavlars.Where(t => t.ID == id).Select(r => new { r.ID, r.SinavNo }).FirstOrDefault();
            ViewBag.SinavNo = sinavlar.SinavNo;
            ViewBag.SinavID = sinavlar.ID;
            var sinavSorular = Entity.Sinav_Sorular.Where(t => t.Sinavlar_Ref == id && t.IsDelete == false).ToList();
            return View(sinavSorular);
        }

        public ActionResult _GetSinavTipSorular(int stipiref)
        {
            var data = Entity.UniteSorulars.Where(r => r.IsActive == true && r.SinavTipi_Ref == stipiref).Select(t => t.Unite).Distinct().ToList();
            return PartialView("_SinavTipiUniteSorular", data);
        }

        public ActionResult SinavCevaplama(int? sinavID)
        {
            int sinavIndex = sinavID.HasValue ? Convert.ToInt32(sinavID) : 0;
            IQueryable<Sinavlar> sinavlar = Entity.Sinavlars.Where(t => t.IsActive == true);
            DateTime Bugun = DateTime.Now.Date;
            IQueryable<Personel_Sinavlar> data = Entity.Personel_Sinavlar.Where(t => t.IsActive == true && t.Sinavlar_Ref == sinavIndex && ((t.SinavTarihi <= Bugun && t.Bitti == 1)));
            try
            {
                if (firmaRef != null)
                {
                    if (!UserRoleName.PANEL_AdminGiris.InRole())
                    {
                        data = data.Where(r => r.Sinif_Ogrenci.Sinif.Firma_Ref == firmaRef);
                        sinavlar = sinavlar.Where(r => r.Firma_Ref == firmaRef);
                    }
                }
            }
            catch
            {
            }
            ViewBag.Sinavlar = sinavlar.ToList();
            ViewBag.SinavID = sinavIndex;
            return View(data.ToList());
        }

        public ActionResult _USorular(int? sinavtip, int id, int tip, int sinavno)
        {

            //int pageSize = 100;
            //int pageIndex = 1;
            //int recordNo = 0;
            //SinavSoruOranlama orn = new SinavSoruOranlama(Entity);
            //pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            //IQueryable<UniteSorular> data = Entity.UniteSorulars.OrderBy(m => m.ID).Where(t => t.IsActive == true && t.IsDelete == false && t.Unite_Ref==id && orn.SoruOranGetir(t.ID) < 90);
            //IQueryable<sp_UniteSorular_Result> data = Entity.sp_UniteSorular(id, 1);

            //List<UniteSorularPage> soruData = new List<UniteSorularPage>();

            //if (tip == (int)Enumlar.SoruTipleri.TEST)
            //{
            //    var lst = data.Where(t => t.SinavTipi_Ref == tip || t.SinavTipi_Ref == (int)Enumlar.SoruTipleri.TEORIK).ToList();
            //    for (int i = 0; i < lst.Count; i++)
            //    {
            //        var oranadet = orn.SoruOranAdetGetir(lst[i].ID);
            //        UniteSorularPage soru = new UniteSorularPage();
            //        soru.UniteSoru = lst[i];
            //        soru.bulunmaOran = oranadet.oran;
            //        soru.bulunmaAdet = oranadet.adet;
            //        soruData.Add(soru);
            //    }              
            //}
            //else
            //{
            IObjectContextAdapter dbcontextadapter = (IObjectContextAdapter)Entity;
            dbcontextadapter.ObjectContext.CommandTimeout = 300;
            List<sp_UniteSorular_Result> list = Entity.sp_UniteSorular(id, tip).ToList();
            //for (int i = 0; i < lst.Count; i++)
            //{
            //    var oranadet = orn.SoruOranAdetGetir(lst[i].ID);
            //    if (oranadet.oran < 90)
            //    {
            //        UniteSorularPage soru = new UniteSorularPage();
            //        soru.UniteSoru = lst[i];
            //        soru.bulunmaOran = oranadet.oran;
            //        soru.bulunmaAdet = oranadet.adet;
            //        soruData.Add(soru);
            //    }
            //}              
            //}
            //list = lst.ToPagedList(pageIndex, pageSize);
            //if (pageIndex != 1)
            //{
            //    recordNo = ((pageIndex - 1) * pageSize);
            //}
            //ViewBag.RecordNo = recordNo;

            ViewBag.Tip = tip;
            ViewBag.uniteTumSorular = Entity.UniteSorulars.Where(e => e.IsActive == true && e.IsDelete == false).ToList();
            ViewBag.sorusiklari = Entity.SoruSiklaris.Where(t => t.IsActive == true && t.IsDelete == false).ToList();
            ViewBag.altsiklar = Entity.AltSiklars.Where(t => t.IsActive == true && t.IsDelete == false).ToList();
            ViewBag.sinavsorular = Entity.Sinav_Sorular.Where(r => r.Sinavlar_Ref == sinavno && r.IsDelete == false).ToList();
            ViewBag.uniteler = Entity.Unites.ToList();
            ViewBag.sinavtipi = sinavtip ?? 0;
            return PartialView("_UniteSorular", list);
        }
        [ValidateInput(false)]
        public JsonResult SPKaydetData(List<String> SOgrenciID, int SinavID, string SinavTarih, string BaslamaSaati, string BitisSaati)
        {
            string durum = "Kayıt Eklenmedi";
            foreach (var item in SOgrenciID)
            {
                int idp = int.Parse(item);
                var controldd = Entity.Personel_Sinavlar.Where(r => r.Sinif_Ogrenci_Ref == idp && r.Sinavlar_Ref == SinavID && r.IsDelete == false && r.IsActive == true).Count();
                if (controldd == 0)
                {
                    double surex = TimeSpan.Parse(BitisSaati).TotalMinutes - TimeSpan.Parse(BaslamaSaati).TotalMinutes;
                    int zaman = Convert.ToInt32(surex);
                    var data = new Personel_Sinavlar();
                    data.IsActive = true;
                    data.IsDelete = false;
                    data.Sinif_Ogrenci_Ref = idp;
                    data.Sinavlar_Ref = SinavID;
                    data.SinavAciklama = false;
                    data.SinavNotuUygulama = 0;
                    data.SinavNotuTeorik = 0;
                    data.Durum = "AÇIKLANMADI";
                    data.DogruSayisiUygulama = 0;
                    data.YanlisSayisiTeorik = 0;
                    data.UygulamaBos = 0;
                    data.YanlisSayisiUgulama = 0;
                    data.DogruSayisiTeorik = 0;
                    data.TeorikBos = 0;
                    data.SinavTarihi = Convert.ToDateTime(SinavTarih);
                    data.BaslamaSaati = TimeSpan.Parse(BaslamaSaati);
                    data.BitisSaati = TimeSpan.Parse(BitisSaati);
                    data.SinavSuresi = zaman;
                    data.Basladi = 0;
                    data.Bitti = 0;
                    data.GirisSaati = null;
                    data.CikisSaati = null;
                    Entity.Personel_Sinavlar.Add(data);
                    Entity.SaveChanges();
                    durum = "ekleme İşlemi Gerçekleşti";
                }
            }
            return Json(durum, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SinavSoruKaydet(int SinavTipi_Ref, int UniteSorular_Ref, int Sinavlar_Ref)
        {
            bool durum = false;
            try
            {
                var kontrol = Entity.Sinav_Sorular.Where(t => t.IsActive == false && t.Sinavlar_Ref == Sinavlar_Ref && t.UniteSorular_Ref == UniteSorular_Ref).FirstOrDefault();
                if (kontrol != null)
                {
                    kontrol.IsActive = true;
                    kontrol.IsDelete = false;
                    durum = true;
                }
                else
                {
                    kontrol = new Sinav_Sorular();
                    kontrol.IsActive = true;
                    kontrol.IsDelete = false;
                    kontrol.Sinavlar_Ref = Sinavlar_Ref;
                    kontrol.SinavTipi_Ref = SinavTipi_Ref;
                    kontrol.UniteSorular_Ref = UniteSorular_Ref;
                    Entity.Sinav_Sorular.Add(kontrol);
                    Entity.SaveChanges();
                    durum = true;
                }
            }
            catch
            {
                durum = false;
            }


            return Json(durum, JsonRequestBehavior.AllowGet);

        }

        public ActionResult PersonelSinav()
        {
            IQueryable<Sinif> siniflar = Entity.Sinifs.Include("Firmalar").Where(t => t.IsActive == true && t.IsDelete == false);
            IQueryable<Sinif_Ogrenci> sinifOgrenci = Entity.Sinif_Ogrenci;
            IQueryable<Sinavlar> sinavlar = Entity.Sinavlars.Where(y => y.IsActive == true);

            if (!UserRoleName.PANEL_AdminGiris.InRole())
            {
                siniflar = siniflar.Where(t => t.Firma_Ref == firmaRef);
                sinifOgrenci = sinifOgrenci.Where(r => r.Sinif.Firma_Ref == firmaRef);
                sinavlar = sinavlar.Where(r => r.Firma_Ref == firmaRef);
            }

            ViewBag.Sinif = siniflar.ToList();
            ViewBag.SinifOgrenci = sinifOgrenci.ToList();
            ViewBag.Sinavlar = sinavlar.ToList();

            return View();
        }

        public JsonResult SPsil(int id)
        {
            bool durum = false;
            try
            {
                var data = Entity.Personel_Sinavlar.Where(t => t.ID == id).FirstOrDefault();
                bool kontrol = data.Basladi == 1 ? false : true;
                if (kontrol)
                {
                    data.IsActive = false;
                    data.IsDelete = true;
                    Entity.SaveChanges();
                    durum = true;
                }
            }
            catch
            {
                durum = false;
            }
            return Json(durum, JsonRequestBehavior.AllowGet);
        }

        public JsonResult TanimliSoruGuncelle(int id)
        {
            bool durum = false;
            try
            {
                var data = Entity.Sinav_Sorular.Where(t => t.ID == id).FirstOrDefault();
                data.IsActive = false;
                data.IsDelete = true;
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SinavSonuclandirma(List<String> postData)
        {
            bool durum = false;
            try
            {
                foreach (var item in postData)
                {
                    List<SinavSonuclari> cevaplar = new List<SinavSonuclari>();
                    int id = int.Parse(item);
                    int sinavEtiket = Entity.Personel_Sinavlar.Where(t => t.ID == id && t.IsActive == true && t.IsDelete == false).Select(y => y.Sinavlar_Ref).FirstOrDefault();
                    var sinavSorular = Entity.Sinav_Sorular.Where(r => r.Sinavlar_Ref == sinavEtiket && r.IsActive == true && r.IsDelete == false).ToList();
                    foreach (var item2 in sinavSorular)
                    {
                        var cevapkontrol = item2.SinavCevaplars.Where(r => r.Personel_Sinavlar_Ref == id).Select(t => new { t.Netice, t.Sınav_Sorular_Ref }).FirstOrDefault();

                        string cevabi = "";
                        if (cevapkontrol == null)
                        {
                            cevabi = "YOK";
                        }
                        else
                        {
                            if (cevapkontrol.Netice == true)
                            {
                                cevabi = "True";
                            }
                            else
                            {
                                cevabi = "False";
                            }
                        }
                        SinavSonuclari datax = new SinavSonuclari();
                        datax.PerID = id;
                        datax.SoruID = item2.ID;
                        datax.Cevap = cevabi;
                        datax.SoruTipi = item2.SinavTipi_Ref;
                        cevaplar.Add(datax);
                    }

                    int TeorikDogruSayisi = cevaplar.Where(t => t.SoruTipi == 2 && t.Cevap == "True").Count();
                    int TeorikYanlisSayisi = cevaplar.Where(t => t.SoruTipi == 2 && t.Cevap == "False").Count();
                    int TeorikBos = cevaplar.Where(t => t.SoruTipi == 2 && t.Cevap == "YOK").Count();

                    int UygulamaDogruSayisi = cevaplar.Where(t => t.SoruTipi == 1 && t.Cevap == "True").Count();
                    int UygulamaYanlisSayisi = cevaplar.Where(t => t.SoruTipi == 1 && t.Cevap == "False").Count();
                    int UygulamaBos = cevaplar.Where(t => t.SoruTipi == 1 && t.Cevap == "YOK").Count();

                    decimal TamPuan = 100;

                    int ToplamSoruUygulama = sinavSorular.Where(y => y.SinavTipi_Ref == 1).Count();
                    int ToplamSoruTeorik = sinavSorular.Where(y => y.SinavTipi_Ref == 2).Count();
                    decimal soruPuanUygulama = 0;
                    decimal soruPuanTeorik = 0;
                    if (ToplamSoruUygulama != 0)
                    {
                        soruPuanUygulama = TamPuan / ToplamSoruUygulama;
                    }
                    if (ToplamSoruTeorik != 0)
                    {
                        soruPuanTeorik = TamPuan / ToplamSoruTeorik;
                    }


                    decimal SinavNotuUygulama = soruPuanUygulama * UygulamaDogruSayisi;
                    decimal SinavNotuTeorik = soruPuanTeorik * TeorikDogruSayisi;

                    string Netice = "";
                    int uygulamaSoruVami = sinavSorular.Where(e => e.SinavTipi_Ref == (int)Enumlar.SoruTipleri.UYGULAMA).Count();
                    int teorikSoruVarmi = sinavSorular.Where(e => e.SinavTipi_Ref == (int)Enumlar.SoruTipleri.TEORIK).Count();

                    if (uygulamaSoruVami > 0 && teorikSoruVarmi > 0)
                    {
                        if (SinavNotuUygulama >= 75 && SinavNotuTeorik >= 75)
                        {
                            Netice = "GEÇTİ";
                        }
                        else
                        {
                            Netice = "KALDI";
                        }
                    }
                    else if (uygulamaSoruVami > 0 && teorikSoruVarmi == 0)
                    {
                        if (SinavNotuUygulama >= 75)
                        {
                            Netice = "GEÇTİ";
                        }
                        else
                        {
                            Netice = "KALDI";
                        }
                    }
                    else if (uygulamaSoruVami == 0 && teorikSoruVarmi > 0)
                    {
                        if (SinavNotuTeorik >= 75)
                        {
                            Netice = "GEÇTİ";
                        }
                        else
                        {
                            Netice = "KALDI";
                        }
                    }



                    var perSinavIsleme = Entity.Personel_Sinavlar.Where(t => t.ID == id).FirstOrDefault();
                    perSinavIsleme.DogruSayisiUygulama = UygulamaDogruSayisi;
                    perSinavIsleme.DogruSayisiTeorik = TeorikDogruSayisi;
                    perSinavIsleme.YanlisSayisiUgulama = UygulamaYanlisSayisi;
                    perSinavIsleme.YanlisSayisiTeorik = TeorikYanlisSayisi;
                    perSinavIsleme.SinavNotuUygulama = SinavNotuUygulama;
                    perSinavIsleme.SinavNotuTeorik = SinavNotuTeorik;
                    perSinavIsleme.UygulamaBos = UygulamaBos;
                    perSinavIsleme.TeorikBos = TeorikBos;
                    perSinavIsleme.Durum = Netice;
                    Entity.SaveChanges();
                    durum = true;
                }
            }
            catch
            {
                durum = false;
            }
            return Json(durum, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DuyuruGonder(int sinavID, List<String> idler)
        {
            bool durum = false;
            try
            {
                foreach (var item in idler)
                {
                    int id = int.Parse(item);
                    var sinavEtiket = Entity.Personel_Sinavlar.Where(t => t.ID == id && t.IsActive == true && t.IsDelete == false && t.Durum != "AÇIKLANMADI" && t.Sinavlar_Ref == sinavID && t.SinavAciklama == false).FirstOrDefault();
                    if (sinavEtiket != null)
                    {
                        sinavEtiket.SinavAciklama = true;
                        sinavEtiket.DuyuruTarihi = DateTime.Now.Date;
                        Entity.SaveChanges();
                    }
                }


                //List<Personel_Sinavlar> data = Entity.Personel_Sinavlar.Where(t => t.Durum != "AÇIKLANMADI" && t.Sinavlar_Ref == sinavID).ToList();
                //foreach (var item in data)
                //{

                //}
                //Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }

            return Json(durum, JsonRequestBehavior.AllowGet);
        }

        public ActionResult _SinavaAtananPersonel(int id)
        {
            var data = Entity.Personel_Sinavlar.Where(e => e.IsActive == true && e.IsDelete == false && e.Sinavlar_Ref == id).ToList();
            return PartialView("_PersonelSinavlar", data);
        }

        public ActionResult _SinifOgrenci(int sinifID)
        {
            DateTime zaman = DateTime.Now.Date;
            List<SinifPersonelView> list = Entity.Sinif_Ogrenci.Where(t => t.Sinif_Ref == sinifID && t.Bitti == false)
                .Select(w => new SinifPersonelView
                {
                    personel_id = w.Personel.ID,
                    adi = w.Personel.Adi,
                    soyadi = w.Personel.Soyadi,
                    tcno = w.Personel.TCNo,
                    resim = w.Personel.Resim ?? "RYok.jpg",
                    sinif = w.Sinif.SinifAdi,
                    sinif_ogrenci_id = w.ID
                }).ToList();


            //var sonlst = list.Where(t=>t.Personel.Kullanicis.Where(r=>r.BitisTarihi.Value)).FirstOrDefault();
            //foreach (var item in list)
            //{                
            //    int kontrol = item.Personel.Kullanicis.Where(y=>y.BitisTarihi >= zaman).Count();
            //    if (kontrol != 0)
            //    {

            //    }
            //}


            return PartialView("_SinifOgrenciler", list);
        }

        public JsonResult AtanmisPersonelCikarma(int sinavID)
        {
            var list = Entity.Personel_Sinavlar.Where(t => t.Sinavlar_Ref == sinavID && t.IsActive == true).Select(y => y.Sinif_Ogrenci.Personel_Ref).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        protected string savedocument(HttpPostedFileBase document)
        {
            if (document != null)
            {
                FileInfo f = new FileInfo(document.FileName);
                string Fname = Guid.NewGuid() + f.Name;
                string path = "Areas/Sinavlar/Images/" + Fname;
                document.SaveAs(Server.MapPath("~/" + path));
                return path;
            }
            else
            {
                return "";
            }

        }


        #region Sınav Tipi İşlemleri

        public ActionResult SinavTip()
        {
            var data = Entity.SinavTipis.ToList();
            return View(data);
        }
        [HttpPost]
        public ActionResult SinavTipiEkle(string Baslik)
        {
            SinavTipi blm = new SinavTipi();
            blm.Baslik = Baslik;
            Entity.SinavTipis.Add(blm);
            Entity.SaveChanges();
            return RedirectToAction("SinavTip", "SinavIslemler", new { area = "Panel" });
        }

        public JsonResult SinavTipiGuncelle(int id, string Baslik)
        {
            bool durum = false;
            try
            {
                SinavTipi blm = Entity.SinavTipis.Where(t => t.ID == id).FirstOrDefault();
                blm.Baslik = Baslik;
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region Test Tanımlama
        public ActionResult Testler()
        {
            ViewBag.Moduller = Entity.UniteModuls.Where(t => t.IsActive == true && t.IsDelete == false).ToList();
            var data = Entity.Testlers.Where(r => r.IsActive == true && r.IsDelete == false).ToList();
            return View(data);
        }

        public JsonResult TestlerGuncelle(int id, string TestNo, string SoruSayisi, int Sure, string Aciklama, int UniteModul_Ref)
        {
            bool durum = false;
            try
            {
                var kontrol = Entity.Testlers.Where(y => y.TestNo == TestNo && y.UniteModul_Ref == UniteModul_Ref).Count();
                if (kontrol == 0)
                {
                    var data = Entity.Testlers.Where(t => t.ID == id).FirstOrDefault();
                    data.TestNo = TestNo;
                    data.SoruSayisi = int.Parse(SoruSayisi);
                    data.Sure = Sure;
                    data.Aciklama = Aciklama;
                    data.UniteModul_Ref = UniteModul_Ref;
                    Entity.SaveChanges();
                    durum = true;
                }
            }
            catch
            {
                durum = false;
            }
            return Json(durum, JsonRequestBehavior.AllowGet);
        }

        public JsonResult TestSilme(int id)
        {
            bool durum = false;
            try
            {
                var durumKontrol = Entity.Testlers.Where(t => t.IsActive == true && t.IsDelete == false && t.ID == id && t.Personel_Testler.Count == 0).FirstOrDefault();
                if (durumKontrol != null)
                {
                    durumKontrol.IsActive = false;
                    durumKontrol.IsDelete = true;
                    Entity.SaveChanges();
                    durum = true;
                }
            }
            catch
            {
                durum = false;
            }
            return Json(durum, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult TestKaydet(string TestNo, int SoruSayisi, int Sure, string Aciklama, int UniteModul_Ref)
        {
            var kontrol = Entity.Testlers.Where(y => y.TestNo == TestNo && y.UniteModul_Ref == UniteModul_Ref).Count();
            if (kontrol == 0)
            {
                Testler data = new Testler();
                data.IsActive = true;
                data.IsDelete = false;
                data.TestNo = TestNo;
                data.SoruSayisi = SoruSayisi;
                data.Sure = Sure;
                data.Aciklama = Aciklama;
                data.UniteModul_Ref = UniteModul_Ref;
                Entity.Testlers.Add(data);
                Entity.SaveChanges();
            }
            return RedirectToAction("Testler", "SinavIslemler", new { area = "Panel" });
        }

        public ActionResult TestSorular(int id, int UniteModul_Ref)
        {
            ViewData["Unite"] = Entity.Unites.Where(r => r.IsActive == true && r.Modul_Ref == UniteModul_Ref).ToList();
            ViewBag.UniteModulRef = UniteModul_Ref;
            var data = Entity.Testlers.Where(t => t.ID == id).FirstOrDefault();
            return View(data);
        }

        public JsonResult TestKursModulUniteler(int id)
        {
            var data = Entity.Unites.Where(t => t.IsActive == true && t.IsDelete == false && t.Modul_Ref == id).Select(r => new { r.ID, r.UniteAdi }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult TestSoruKaydet(int Test_Ref, int UniteSorular_Ref)
        {
            bool durum = false;
            try
            {
                var kontrol = Entity.Test_Sorular.Where(r => r.Testler_Ref == Test_Ref && r.UniteSorular_Ref == UniteSorular_Ref).FirstOrDefault();
                if (kontrol == null)
                {
                    var data = new Test_Sorular();
                    data.IsActive = true;
                    data.IsDelete = false;
                    data.Testler_Ref = Test_Ref;
                    data.UniteSorular_Ref = UniteSorular_Ref;
                    Entity.Test_Sorular.Add(data);
                    Entity.SaveChanges();
                }
                else
                {
                    kontrol.IsActive = true;
                    kontrol.IsDelete = false;
                    Entity.SaveChanges();
                }
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, JsonRequestBehavior.AllowGet);

        }


        public JsonResult TanimliSoruGuncelleTest(int id)
        {
            bool durum = false;
            try
            {
                var data = Entity.Test_Sorular.Where(t => t.ID == id).FirstOrDefault();
                data.IsActive = false;
                data.IsDelete = true;
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, JsonRequestBehavior.AllowGet);
        }
        #endregion



        protected override void Dispose(bool disposing)
        {
            Entity.Dispose();
            base.Dispose(disposing);
        }
    }

    public class SinavSonuclari : IDisposable
    {
        public int PerID { get; set; }
        public int SoruID { get; set; }
        public string Cevap { get; set; }
        public int SoruTipi { get; set; }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }

    public class SoruSiklariLocal : IDisposable
    {

        public int ID { get; set; }
        public string SoruSikki { get; set; }
        public string CevapIcerik { get; set; }
        public bool DogruSecenek { get; set; }


        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }

    public class ResimIslem : IDisposable
    {
        public int? ID { get; set; }
        public string Baslik { get; set; }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }

}
