﻿using Haveak_Pro.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System;
using System.Threading.Tasks;
using System.Data.Entity;
using Haveak_Pro.Areas.PANEL.Models;


namespace Haveak_Pro.Areas.PANEL.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    [Authorize(Roles = UserRoleName.PANEL_AdminGiris + "," + UserRoleName.ADMIN_PersonelKullanici)]
    public class RaporlamaController : Controller
    {
        public JsonResult getPersonel(int param)
        {
            using (HaveakTurEntities db = new HaveakTurEntities())
            {
                List<sp_Personel_Result> dty = db.sp_Personel(param).ToList();
                return Json(dty, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult getPersonelSinavlar(int param)
        {
            using (HaveakTurEntities db = new HaveakTurEntities())
            {
                List<sp_PersonelSinavlar_Result> dty = db.sp_PersonelSinavlar(param).ToList();
                return Json(dty, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getPersonelTestler(int param)
        {
            using (HaveakTurEntities db = new HaveakTurEntities())
            {
                List<sp_PersonelTestler_Result> dty = db.sp_PersonelTestler(param).ToList();
                return Json(dty, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getPersonelTip(int param)
        {
            using (HaveakTurEntities db = new HaveakTurEntities())
            {
                List<sp_Personel_TIP_Result> dty = db.sp_Personel_TIP(param).ToList();
                return Json(dty, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult getPersonelToplamSurec(int param)
        {
            using (HaveakTurEntities db = new HaveakTurEntities())
            {
                List<sp_ToplamSurecler_Result> dty = db.sp_ToplamSurecler(param).ToList();
                List<TimeSpan> saatToplam = new List<TimeSpan>();
                //int bagajToplam = 0;
                //string sinifAdi = string.Empty;
                //foreach (var item in dty)
                //{                    
                //    if (item.SinifAdi == sinifAdi || sinifAdi == string.Empty)
                //    {
                //        saatToplam.Add(item.ToplamSure.Value);
                //        bagajToplam += item.ToplamBagaj.Value;
                //        sinifAdi = item.SinifAdi;
                //    }
                //    else
                //    {
                //        sp_ToplamSurecler_Result rst = new sp_ToplamSurecler_Result();
                //        rst.SinifAdi = sinifAdi;
                //        rst.SurecAdi = "";
                //        rst.ToplamBagaj = bagajToplam;
                //        rst.ToplamSure = saatToplam.;
                //        dty.Add(rst);
                //    }
                //}

                return Json(dty, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getPersonelGenelToplam(int param)
        {
            using (HaveakTurEntities db = new HaveakTurEntities())
            {
                string dty = db.sp_GenelToplamSurecler(param).FirstOrDefault() == null ? "00:00:00" : db.sp_GenelToplamSurecler(param).FirstOrDefault().Value.ToString();
                return Json(dty, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getPersonelSinifToplamSurec(int param, string sinif)
        {
            using (HaveakTurEntities db = new HaveakTurEntities())
            {
                sp_SinifToplamSurecler_Result dty = db.sp_SinifToplamSurecler(param, sinif).FirstOrDefault();
                return Json(dty, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult getMaddeSinavBasari(int param)
        {
            List<DuzenekOranlarView> oranlar = new GenelRaporlama().SinavMaddeOranlar(param);

            //using (HaveakTurEntities db = new HaveakTurEntities())
            //{
            //List<DuzenekOranlar> oranlar = new List<DuzenekOranlar>();

            //#region Uygulama Sınav ORanlar

            ////var personelSinavSorular = db.UniteSorulars.Where(e => e.STM_Ref != null && e.IsActive == true && e.IsDelete == false && e.SinavTipi_Ref == 1
            ////    && e.Sinav_Sorular.Any(q => q.IsActive == true && q.IsDelete == false
            ////    && q.Sinavlar.Personel_Sinavlar.Any(r => r.Sinif_Ogrenci.Personel_Ref == param && r.IsActive == true && r.IsDelete == false && r.SinavAciklama==true))).ToList(); // Uygulama Personelin gördüğü sorular

            //Nullable<int> genelSinavSilah = db.PersonelToplamSinavSoruMaddeDagilim(param).Where(w=>w.Isim == "Silah").Select(q => q.ToplamSoru).FirstOrDefault() ?? 0;
            //Nullable<int> genelSinavSilahDogru = db.PersonelToplamSinavSoruMaddeDagilimDogruAdet(param).Where(r=>r.Isim == "Silah").Select(q=>q.DogruCevap).FirstOrDefault() ?? 0;
            //double oran = 0;
            //try
            //{
            //    oran = ((Convert.ToDouble(genelSinavSilahDogru) / Convert.ToDouble(genelSinavSilah)) * 100);
            //    if (double.IsNaN(oran))
            //    {
            //        oran = 0;
            //    }
            //    oran = Math.Round(oran, 2);
            //}
            //catch
            //{
            //    oran = 0;
            //}
            //oranlar.Add(new DuzenekOranlar { madde = "Silah", oran = oran, sorusayisi = genelSinavSilah.Value, dogrusayisi = genelSinavSilahDogru.Value, sinavturu ="UYGULAMA Sınavlar" });

            //Nullable<int> genelSinavDuzenek = db.PersonelToplamSinavSoruMaddeDagilim(param).Where(w => w.Isim == "Düzenek").Select(q => q.ToplamSoru).FirstOrDefault() ?? 0;
            //Nullable<int> genelSinavDuzenekDogru = db.PersonelToplamSinavSoruMaddeDagilimDogruAdet(param).Where(r => r.Isim == "Düzenek").Select(q => q.DogruCevap).FirstOrDefault() ?? 0;
            //double oranDuz = 0;
            //try
            //{
            //    oranDuz = ((Convert.ToDouble(genelSinavDuzenekDogru) / Convert.ToDouble(genelSinavDuzenek)) * 100);
            //    if (double.IsNaN(oranDuz))
            //    {
            //        oranDuz = 0;
            //    }
            //    oranDuz = Math.Round(oranDuz, 2);
            //}
            //catch
            //{
            //    oranDuz = 0;
            //}
            //oranlar.Add(new DuzenekOranlar { madde = "Düzenek", oran = oranDuz, sorusayisi = genelSinavDuzenek.Value, dogrusayisi = genelSinavDuzenekDogru.Value, sinavturu=" " });

            //Nullable<int> genelSinavKesici = db.PersonelToplamSinavSoruMaddeDagilim(param).Where(w => w.Isim == "Kesici-Delici").Select(q => q.ToplamSoru).FirstOrDefault() ?? 0;
            //Nullable<int> genelSinavKesiciDogru = db.PersonelToplamSinavSoruMaddeDagilimDogruAdet(param).Where(r => r.Isim == "Kesici-Delici").Select(q => q.DogruCevap).FirstOrDefault() ?? 0;
            //double oranKes = 0;
            //try
            //{
            //    oranKes = ((Convert.ToDouble(genelSinavKesiciDogru) / Convert.ToDouble(genelSinavKesici)) * 100);
            //    if (double.IsNaN(oranKes))
            //    {
            //        oranKes = 0;
            //    }
            //    oranKes = Math.Round(oranKes, 2);
            //}
            //catch
            //{
            //    oranKes = 0;
            //}
            //oranlar.Add(new DuzenekOranlar { madde = "Kesici-Delici", oran = oranKes, sorusayisi = genelSinavKesici.Value, dogrusayisi = genelSinavKesiciDogru.Value, sinavturu= " " });

            //Nullable<int> genelSinavKargo = db.PersonelToplamSinavSoruMaddeDagilim(param).Where(w => w.Isim == "Düzenek (Kargo-EDS)").Select(q => q.ToplamSoru).FirstOrDefault() ?? 0;
            //Nullable<int> genelSinavKargoDogru = db.PersonelToplamSinavSoruMaddeDagilimDogruAdet(param).Where(r => r.Isim == "Düzenek (Kargo-EDS)").Select(q => q.DogruCevap).FirstOrDefault() ?? 0;
            //double oranKar = 0;
            //try
            //{
            //    oranKar = ((Convert.ToDouble(genelSinavKargoDogru) / Convert.ToDouble(genelSinavKargo)) * 100);
            //    if (double.IsNaN(oranKar))
            //    {
            //        oranKar = 0;
            //    }
            //    oranKar = Math.Round(oranKar, 2);
            //}
            //catch
            //{
            //    oranKar = 0;
            //}
            //oranlar.Add(new DuzenekOranlar { madde = "Düzenek (Kargo-EDS)", oran = oranKar, sorusayisi = genelSinavKargo.Value, dogrusayisi = genelSinavKargoDogru.Value, sinavturu= " " });

            //Nullable<int> genelSinavDiger = db.PersonelToplamSinavSoruMaddeDagilim(param).Where(w => w.Isim == "Diğerleri").Select(q => q.ToplamSoru).FirstOrDefault() ?? 0;
            //Nullable<int> genelSinavDigerDogru = db.PersonelToplamSinavSoruMaddeDagilimDogruAdet(param).Where(r => r.Isim == "Diğerleri").Select(q => q.DogruCevap).FirstOrDefault() ?? 0;
            //double oranDig = 0;
            //try
            //{
            //    oranDig = ((Convert.ToDouble(genelSinavDigerDogru) / Convert.ToDouble(genelSinavDiger)) * 100);
            //    if (double.IsNaN(oranDig))
            //    {
            //        oranDig = 0;
            //    }
            //    oranDig = Math.Round(oranDig, 2);
            //}
            //catch
            //{
            //    oranDig = 0;
            //}
            //oranlar.Add(new DuzenekOranlar { madde = "Diğerleri", oran = oranDig, sorusayisi = genelSinavDiger.Value, dogrusayisi = genelSinavDigerDogru.Value, sinavturu= " " }); 
            //#endregion

            //#region GİS Sınavlar
            //var personelSinavSorularTip = db.UniteSorulars.Where(e => e.STM_Ref != null && e.IsActive == true && e.IsDelete == false && e.SBMaddeResimler.SBTM_Ref == 4
            //         && e.Tip_Sorular.Any(q => q.IsActive == true && q.IsDelete == false
            //         && q.TipCevaplars.Any(r => r.Personel_TIP.Personel_Ref == param))).ToList(); //  GİS Personelin gördüğü sorular

            //var genelSinavSilahTip = personelSinavSorular.Where(w => w.SBMaddeResimler.SBTM_Ref == 1).Count();
            //var genelSinavSilahDogruTip = personelSinavSorular.Where(q => q.SBMaddeResimler.SBTM_Ref == 1 && q.Sinav_Sorular.Any(u => u.SinavCevaplars.Any(t => t.DogruCevap == true))).Count();
            //double oranTip = 0;
            //try
            //{
            //    oranTip = ((Convert.ToDouble(genelSinavSilahDogruTip) / Convert.ToDouble(genelSinavSilahTip)) * 100);
            //    if (double.IsNaN(oranTip))
            //    {
            //        oranTip = 0;
            //    }
            //    oranTip = Math.Round(oranTip, 2);
            //}
            //catch
            //{
            //    oranTip = 0;
            //}
            //oranlar.Add(new DuzenekOranlar { madde = "Silah", oran = oranTip, sorusayisi = genelSinavSilahTip, dogrusayisi = genelSinavSilahDogruTip, sinavturu = "GİS Sınavlar" });

            //var genelSinavDuzenekTip = personelSinavSorular.Where(w => w.SBMaddeResimler.SBTM_Ref == 3).Count();
            //var genelSinavDuzenekDogruTip = personelSinavSorular.Where(q => q.SBMaddeResimler.SBTM_Ref == 3 && q.Sinav_Sorular.Any(u => u.SinavCevaplars.Any(t => t.DogruCevap == true))).Count();
            //double oranDuzTip = 0;
            //try
            //{
            //    oranDuzTip = ((Convert.ToDouble(genelSinavDuzenekDogruTip) / Convert.ToDouble(genelSinavDuzenekTip)) * 100);
            //    if (double.IsNaN(oranDuzTip))
            //    {
            //        oranDuzTip = 0;
            //    }
            //    oranDuzTip = Math.Round(oranDuzTip, 2);
            //}
            //catch
            //{
            //    oranDuz = 0;
            //}
            //oranlar.Add(new DuzenekOranlar { madde = "Düzenek", oran = oranDuzTip, sorusayisi = genelSinavDuzenekTip, dogrusayisi = genelSinavDuzenekDogruTip, sinavturu = " " });

            //var genelSinavKesiciTip = personelSinavSorular.Where(w => w.SBMaddeResimler.SBTM_Ref == 2).Count();
            //var genelSinavKesiciDogruTip = personelSinavSorular.Where(q => q.SBMaddeResimler.SBTM_Ref == 2 && q.Sinav_Sorular.Any(u => u.SinavCevaplars.Any(t => t.DogruCevap == true))).Count();
            //double oranKesTip = 0;
            //try
            //{
            //    oranKesTip = ((Convert.ToDouble(genelSinavKesiciDogruTip) / Convert.ToDouble(genelSinavKesiciTip)) * 100);
            //    if (double.IsNaN(oranKesTip))
            //    {
            //        oranKesTip = 0;
            //    }
            //    oranKesTip = Math.Round(oranKesTip, 2);
            //}
            //catch
            //{
            //    oranKesTip = 0;
            //}
            //oranlar.Add(new DuzenekOranlar { madde = "Kesici-Delici", oran = oranKesTip, sorusayisi = genelSinavKesiciTip, dogrusayisi = genelSinavKesiciDogruTip });

            //var genelSinavKargoTip = personelSinavSorular.Where(w => w.SBMaddeResimler.SBTM_Ref == 6).Count();
            //var genelSinavKargoDogruTip = personelSinavSorular.Where(q => q.SBMaddeResimler.SBTM_Ref == 6 && q.Sinav_Sorular.Any(u => u.SinavCevaplars.Any(t => t.DogruCevap == true))).Count();
            //double oranKarTip = 0;
            //try
            //{
            //    oranKarTip = ((Convert.ToDouble(genelSinavKargoDogruTip) / Convert.ToDouble(genelSinavKargoTip)) * 100);
            //    if (double.IsNaN(oranKarTip))
            //    {
            //        oranKarTip = 0;
            //    }
            //    oranKarTip = Math.Round(oranKarTip, 2);
            //}
            //catch
            //{
            //    oranKarTip = 0;
            //}
            //oranlar.Add(new DuzenekOranlar { madde = "Düzenek (Kargo-EDS)", oran = oranKarTip, sorusayisi = genelSinavKargoTip, dogrusayisi = genelSinavKargoDogruTip, sinavturu = " " });

            //var genelSinavDigerTip = personelSinavSorular.Where(w => w.SBMaddeResimler.SBTM_Ref == 7).Count();
            //var genelSinavDigerDogruTip = personelSinavSorular.Where(q => q.SBMaddeResimler.SBTM_Ref == 7 && q.Sinav_Sorular.Any(u => u.SinavCevaplars.Any(t => t.DogruCevap == true))).Count();
            //double oranDigTip = 0;
            //try
            //{
            //    oranDigTip = ((Convert.ToDouble(genelSinavDigerDogruTip) / Convert.ToDouble(genelSinavDigerTip)) * 100);
            //    if (double.IsNaN(oranDigTip))
            //    {
            //        oranDigTip = 0;
            //    }
            //    oranDigTip = Math.Round(oranDigTip, 2);
            //}
            //catch
            //{
            //    oranDigTip = 0;
            //}
            //oranlar.Add(new DuzenekOranlar { madde = "Diğerleri", oran = oranDigTip, sorusayisi = genelSinavDigerTip, dogrusayisi = genelSinavDigerDogruTip, sinavturu = " " });

            //#endregion

            return Json(oranlar, JsonRequestBehavior.AllowGet);
            //}        
        }
        public async Task<ActionResult> Raporlar()
        {
            using (HaveakTurEntities db = new HaveakTurEntities())
            {
                IQueryable<Sinif> siniflar = db.Sinifs.Where(e => !e.IsDelete && e.IsActive);
                if (!UserRoleName.PANEL_AdminGiris.InRole())
                {
                    int firmaRef = AktifKullanici.Aktif.FirmaId.Value;
                    siniflar = db.Sinifs.Where(e => !e.IsDelete && e.IsActive && e.Firma_Ref == firmaRef);
                }
                return View(await siniflar.ToListAsync());
            }
        }

        public ActionResult GenelRaporlarDetay()
        {
            return View();
        }
        public JsonResult GenelToplamRaporlarDetay(string ad, string soyad, string kurumadi, string tarih, string tarih2)
        {
            DateTime? tarihb = null;
            DateTime? tarihbit = null;
            List<PersonelGenelRapor> raporData = null;

            if (!string.IsNullOrEmpty(tarih) && string.IsNullOrEmpty(tarih2))
            {
                tarihb = Convert.ToDateTime(tarih);
                tarihbit = Convert.ToDateTime(tarih);
            }
            if (!string.IsNullOrEmpty(tarih) && !string.IsNullOrEmpty(tarih2))
            {
                tarihb = Convert.ToDateTime(tarih);
                tarihbit = Convert.ToDateTime(tarih2);
            }
            int? firmaId = AktifKullanici.Aktif.FirmaId;
            raporData = new GenelRaporlama().GenelDataCiktiDetay(firmaId, ad, soyad, kurumadi, tarihb, tarihbit).ToList();
            return Json(raporData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GenelRaporlar()
        {
            return View();
        }
        public JsonResult GenelToplamRaporlar(int ktipi)
        {            
            int? firmaId = AktifKullanici.Aktif.FirmaId;
            List<PersonelGenelRapor> raporData = new GenelRaporlama().GenelDataCikti(firmaId, ktipi).ToList();
            return Json(raporData, JsonRequestBehavior.AllowGet);
        }
        public async Task<ActionResult> getSinifPersoneller(int sinif_ref)
        {
            using (HaveakTurEntities db = new HaveakTurEntities())
            {
                List<Sinif_Ogrenci> data = await db.Sinif_Ogrenci.Include(r => r.Personel_Sinavlar).Include(r => r.Personel).Include(r => r.Sinif).Include(r => r.Personel_Sinavlar.Select(q => q.Sinavlar)).Where(t => t.Sinif_Ref == sinif_ref).ToListAsync();
                //List<Personel_Sinavlar> data = db.Personel_Sinavlar.Include("Sinavlar").Include(r=>r.Sinif_Ogrenci.Personel).Where(t => t.IsActive == true && t.Sinif_Ogrenci.Sinif_Ref == sinif_ref && t.Bitti == 1).ToList();
                return PartialView("_sinifOgrenciListe", data);
            }
        }

        public async Task<ActionResult> GenelSinifRaporlar()
        {
            using (HaveakTurEntities db = new HaveakTurEntities())
            {
                IQueryable<Sinif> siniflar = db.Sinifs.Where(e => !e.IsDelete && e.IsActive);
                if (!UserRoleName.PANEL_AdminGiris.InRole())
                {
                    int firmaRef = AktifKullanici.Aktif.FirmaId.Value;
                    siniflar = db.Sinifs.Where(e => !e.IsDelete && e.IsActive && e.Firma_Ref == firmaRef);
                }
                return View(await siniflar.ToListAsync());
            }
        }

        public ActionResult getSinifDetayRapor(string siniflar)
        {
            List<PersonelGenelRapor> raporData = new GenelRaporlama().GenelSinifDataCiktiDetay(siniflar).ToList();
            return PartialView("_sinifGenelOgrenciRaporListe",raporData);
        }

        public ActionResult GenelSinifRaporlarDetay()
        {
            return View();
        }

        public JsonResult GenelSinifToplamRaporlarDetay(string ad, string soyad, string kurumadi, string tarih, string tarih2)
        {
            DateTime? tarihb = null;
            DateTime? tarihbit = null;
            List<PersonelGenelRapor> raporData = null;

            if (!string.IsNullOrEmpty(tarih) && string.IsNullOrEmpty(tarih2))
            {
                tarihb = Convert.ToDateTime(tarih);
                tarihbit = Convert.ToDateTime(tarih);
            }
            if (!string.IsNullOrEmpty(tarih) && !string.IsNullOrEmpty(tarih2))
            {
                tarihb = Convert.ToDateTime(tarih);
                tarihbit = Convert.ToDateTime(tarih2);
            }
            int? firmaId = AktifKullanici.Aktif.FirmaId;
            raporData = new GenelRaporlama().GenelSinifDataCiktiDetay(firmaId, ad, soyad, kurumadi, tarihb, tarihbit).ToList();
            return Json(raporData, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GenelDagilimlarToplam()
        {
            HaveakTurEntities db = new HaveakTurEntities();
            List<KategoriDagilimlar> dglList = new List<KategoriDagilimlar>();
            KategoriDagilimlar dgl = new KategoriDagilimlar();
            KursKategoriler ktg = new KursKategoriler();
            dgl.Baslik = "Toplam Görüntü Sayısı";
            var kategoriler = await db.SBagajKategoris.Where(t => t.IsActive == true && t.IsDelete == false).ToListAsync();
            foreach (var item in kategoriler)
            {
                int data = await db.SBagajlars.Where(t => t.SKategori_Ref == item.ID && t.IsActive == true && t.IsDelete == false).CountAsync();
                switch (item.Isim)
                {
                    case "TERMİNAL BAGAJLARI":
                        ktg.Kurs3 = data;
                        ktg.Kurs4 = data;
                        break;
                    case "KARGO BAGAJLAR":
                        ktg.Kurs11 = data;
                        break;
                    case "TEDARİK BAGAJLAR":
                        ktg.Kurs17 = data;
                        break;
                    default:
                        break;
                }
            }
            dgl.Detaylar = ktg;
            dglList.Add(dgl);
            ViewBag.BosBagajSorlar = await db.UniteSorulars.Where(r => r.IsActive && !r.IsDelete && r.STM_Ref == null && r.SinavTipi_Ref == 1).CountAsync();
            ViewBag.Madde = await db.Maddelers.Where(t => t.IsActive == true && t.IsDelete == false).ToListAsync();
            ViewBag.TMaddeler = await db.SBMaddeResimlers.Where(t => t.IsActive == true && t.IsDelete == false).ToListAsync();
            ViewBag.TMKategori = await db.TehlikeliMaddeKategoris.Where(e => e.IsActive == true && e.IsDelete == false).ToListAsync();
            ViewBag.STMKategori = await db.SBTehlikeliMKategoris.Where(e => e.IsActive == true && e.IsDelete == false).ToListAsync();
            return View(dglList);
        }

        public async Task<ActionResult> GenelDagilimlarDetay()
        {
            List<UniteSorular> list = null;
            using (HaveakTurEntities db = new HaveakTurEntities())
            {
                list = await db.Sinav_Sorular.Where(t => t.IsActive == true && t.IsDelete == false && t.UniteSorular.STM_Ref != null &&  t.SinavTipi.Baslik == "UYGULAMA").Select(w=>w.UniteSorular).Include(d=>d.SBMaddeResimler.SBTehlikeliMKategori).Include(r=>r.SBagajlar.SBagajKategori).Include(s=>s.Unite).ToListAsync(); 
            }
            return View(list);
        }



        //public List<Personel_Sinavlar> getSinifData(int sinif_ref)
        //{
        //    using (HaveakTurEntities db = new HaveakTurEntities())
        //    {
        //        List<SinifData> data = db.Personel_Sinavlar.Include("Sinavlar").Include(r => r.Sinif_Ogrenci.Personel).Where(t => t.IsActive == true && t.Sinif_Ogrenci.Sinif_Ref == sinif_ref && t.Bitti == 1).Select(e=>new SinifData {
        //             tcno = e.Sinif_Ogrenci.Personel.TCNo,
        //             sno = e.Sinavlar.SinavNo,
        //              adi = e.Sinif_Ogrenci.Personel.Adi,
        //               soyadi = e.Sinif_Ogrenci.Personel.Soyadi,
        //                siniflar = e.Sinif_Ogrenci.Sinif.SinifAdi,
        //                 teorik = e.SinavNotuTeorik.ToString(),
        //                   uygulama  =e.SinavNotuUygulama.ToString(),
        //                    durum = e.Durum
        //                     //sonuc = e.
        //        }).ToList();
        //        return data;
        //    }
        //}

        //public ActionResult getGlobalSinifPersoneller(int sinif_ref)
        //{
        //    using (HaveakTurEntities db = new HaveakTurEntities())
        //    {
        //        List<Personel> data = db.Personels.Select.ToList();
        //        return PartialView("_sinifOgrenciListe", data);
        //    }
        //}

        //[HttpPost]
        //[ValidateInput(false)]
        //public ActionResult ExportPdf()
        //{
        //    using (MemoryStream stream = new System.IO.MemoryStream())
        //    {
        //        string gridHtml = System.IO.File.ReadAllText(Server.MapPath("~/Shared/ReportViews/PRapor.html"), Encoding.UTF8);
        //        StringReader sr = new StringReader(gridHtml);
        //        Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);

        //        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
        //        pdfDoc.Open();
        //        XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
        //        pdfDoc.Close();
        //        return File(stream.ToArray(), "application/pdf", "Grid.pdf");
        //    }
        //}

    }

    public class KursKategoriler
    {
        public int Kurs3 { get; set; }
        public int Kurs4 { get; set; }
        public int Kurs11 { get; set; }
        public int Kurs17 { get; set; }
    }

    public class KategoriDagilimlar
    {
        public string Baslik { get; set; }
        public KursKategoriler Detaylar { get; set; }
    }

}
