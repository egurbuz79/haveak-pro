﻿using Haveak_Pro.Areas.PANEL.Models;
using Haveak_Pro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace Haveak_Pro.Areas.PANEL.Controllers
{
    public class TipIslemlerController : Controller
    {
        //
        // GET: /PANEL/TipIslemler/
        HaveakTurEntities Entity = new HaveakTurEntities();
        public ActionResult Index()
        {
            var data = Entity.TipSinavlars.Where(r => r.IsActive == true && r.IsDelete == false).ToList();
            return View(data);
        }

        public JsonResult TipGuncelle(int id, string TipNo, string SoruSayisi, int Sure, string Aciklama, bool Yayin)
        {
            bool durum = false;
            try
            {
                var data = Entity.TipSinavlars.Where(y => y.ID == id).FirstOrDefault();
                if (data != null)
                {                 
                    data.TipNo = TipNo;
                    data.SoruSayisi = int.Parse(SoruSayisi);
                    data.Sure = Sure;
                    data.Aciklama = Aciklama;
                    data.Yayinda = Yayin;
                    Entity.SaveChanges();
                    durum = true;
                }
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult TipSilme(int id)
        {
            bool durum = false;
            try
            {
                var durumKontrol = Entity.TipSinavlars.Where(t => t.IsActive == true && t.IsDelete == false && t.ID == id).FirstOrDefault();
                if (durumKontrol != null)
                {
                    durumKontrol.IsActive = false;
                    durumKontrol.IsDelete = true;
                    Entity.SaveChanges();
                    durum = true;
                }
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult TipKaydet(string TipNo, int SoruSayisi, int Sure, string Aciklama, string Yayin)
        {
            var kontrol = Entity.TipSinavlars.Where(y => y.TipNo == TipNo && y.IsDelete==false && y.IsActive==true).Count();
            if (kontrol == 0)
            {
                TipSinavlar data = new TipSinavlar();
                data.IsActive = true;
                data.IsDelete = false;
                data.TipNo = TipNo;
                data.SoruSayisi = SoruSayisi;
                data.Sure = Sure;
                data.Aciklama = Aciklama;
                data.Yayinda = Yayin == "on" ? true: false;
                Entity.TipSinavlars.Add(data);
                Entity.SaveChanges();
            }
            return RedirectToAction("Index", "TipIslemler", new { area = "Panel" });
        }

        public async Task<ActionResult> TipSorular(int id)
        {
            //ViewData["Unite"] = Entity.Unites.Where(r => r.IsActive == true).ToList();
            //yukarıda kurs 3 ve kurs 11 ya otomatik seçilecek yada başka bi şekilde çağrılacak!
            ViewBag.TipSorular = await Entity.UniteSorulars.Where(t => t.SinavTipi_Ref == (int)Enumlar.SoruTipleri.TIP && t.IsActive==true && t.IsDelete==false).ToListAsync();
            var data = await Entity.TipSinavlars.Where(t => t.ID == id && t.IsActive==true && t.IsDelete==false).FirstOrDefaultAsync();
            return View(data);
        }

        //public JsonResult TestKursModulUniteler(int id)
        //{
        //    var data = Entity.Unites.Where(t => t.IsActive == true && t.IsDelete == false && t.Modul_Ref == id).Select(r => new { r.ID, r.UniteAdi }).ToList();
        //    return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        //}

        public JsonResult TipSoruKaydet(int TipSinavlar_Ref, int UniteSorular_Ref)
        {
            bool durum = false;
            try
            {
                var kontrol = Entity.Tip_Sorular.Where(r => r.TipSinavlar_Ref == TipSinavlar_Ref && r.UniteSorular_Ref == UniteSorular_Ref && r.IsActive==true && r.IsDelete==false).FirstOrDefault();
                if (kontrol == null)
                {
                    var data = new Tip_Sorular();
                    data.IsActive = true;
                    data.IsDelete = false;
                    data.TipSinavlar_Ref = TipSinavlar_Ref;
                    data.UniteSorular_Ref = UniteSorular_Ref;
                    Entity.Tip_Sorular.Add(data);
                    Entity.SaveChanges();                   
                }
                else
                {
                    kontrol.IsActive = true;
                    kontrol.IsDelete = false;
                    Entity.SaveChanges();  
                }
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);

        }


        public JsonResult TanimliSoruGuncelleTip(int id)
        {
            bool durum = false;
            try
            {
                var data = Entity.Tip_Sorular.Where(t => t.ID == id).FirstOrDefault();
                data.IsActive = false;
                data.IsDelete = true;
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

       

    }
}
