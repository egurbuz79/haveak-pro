﻿using Haveak_Pro.Areas.PANEL.Models;
using Haveak_Pro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Haveak_Pro.Areas.PANEL.Controllers
{
    public class TipEgitimIslemlerController : Controller
    {
        //
        // GET: /PANEL/TipEgitimIslemler/

        HaveakTurEntities Entity = new HaveakTurEntities();
        public ActionResult Index()
        {
            var data = Entity.TipEgitimSinavlars.Where(r => r.IsActive == true && r.IsDelete == false).ToList();
            return View(data);
        }

        public JsonResult TipEgitimGuncelle(int id, string TipEgitimNo, string SoruSayisi, int Sure, string Aciklama, bool Yayin)
        {
            bool durum = false;
            try
            {
                var data = Entity.TipEgitimSinavlars.Where(y => y.ID == id).FirstOrDefault();
                if (data != null)
                {
                    data.TipEgitimNo = TipEgitimNo;
                    data.SoruSayisi = int.Parse(SoruSayisi);
                    data.Sure = Sure;
                    data.Aciklama = Aciklama;
                    data.Yayinda = Yayin;
                    Entity.SaveChanges();
                    durum = true;
                }
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult TipEgitimSilme(int id)
        {
            bool durum = false;
            try
            {
                var durumKontrol = Entity.TipEgitimSinavlars.Where(t => t.IsActive == true && t.IsDelete == false && t.ID == id).FirstOrDefault();
                if (durumKontrol != null)
                {
                    durumKontrol.IsActive = false;
                    durumKontrol.IsDelete = true;
                    Entity.SaveChanges();
                    durum = true;
                }
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult TipEgitimKaydet(string TipEgitimNo, int SoruSayisi, int Sure, string Aciklama, string Yayin)
        {
            var kontrol = Entity.TipEgitimSinavlars.Where(y => y.TipEgitimNo == TipEgitimNo && y.IsDelete == false && y.IsActive == true).Count();
            if (kontrol == 0)
            {
                TipEgitimSinavlar data = new TipEgitimSinavlar();
                data.IsActive = true;
                data.IsDelete = false;
                data.TipEgitimNo = TipEgitimNo;
                data.SoruSayisi = SoruSayisi;
                data.Sure = Sure;
                data.Aciklama = Aciklama;
                data.Yayinda = Yayin == "on" ? true : false;
                Entity.TipEgitimSinavlars.Add(data);
                Entity.SaveChanges();
            }
            return RedirectToAction("Index", "TipEgitimIslemler", new { area = "Panel" });
        }

        public ActionResult TipEgitimSorular(int id)
        {
            //ViewData["Unite"] = Entity.Unites.Where(r => r.IsActive == true).ToList();
            //yukarıda kurs 3 ve kurs 11 ya otomatik seçilecek yada başka bi şekilde çağrılacak!

            //Not: Enumlar DB deki ID ye göre numara almalı!
            ViewBag.TipEgitimSorular = Entity.UniteSorulars.Where(t => t.SinavTipi_Ref == (int)Enumlar.SoruTipleri.TIP_EGITIM && t.IsActive == true && t.IsDelete == false).ToList();
            var data = Entity.TipEgitimSinavlars.Where(t => t.ID == id && t.IsActive == true && t.IsDelete == false).FirstOrDefault();
            return View(data);
        }

        public JsonResult TipEgitimSoruKaydet(int TipEgitimSinavlar_Ref, int Unite_Sorular_Ref)
        {
            bool durum = false;
            try
            {
                var kontrol = Entity.TipEgitim_Sorular.Where(r => r.TipEgitSinavlar_Ref == TipEgitimSinavlar_Ref && r.Unite_Sorular_Ref == Unite_Sorular_Ref && r.IsActive == true && r.IsDelete == false).FirstOrDefault();
                if (kontrol == null)
                {
                    var data = new TipEgitim_Sorular();
                    data.IsActive = true;
                    data.IsDelete = false;
                    data.TipEgitSinavlar_Ref = TipEgitimSinavlar_Ref;
                    data.Unite_Sorular_Ref = Unite_Sorular_Ref;
                    Entity.TipEgitim_Sorular.Add(data);
                    Entity.SaveChanges();
                }
                else
                {
                    kontrol.IsActive = true;
                    kontrol.IsDelete = false;
                    Entity.SaveChanges();
                }
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);

        }


        public JsonResult TanimliEgitimSoruGuncelleTip(int id)
        {
            bool durum = false;
            try
            {
                var data = Entity.TipEgitim_Sorular.Where(t => t.ID == id).FirstOrDefault();
                data.IsActive = false;
                data.IsDelete = true;
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

    }
}
