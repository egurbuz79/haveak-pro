﻿using Haveak_Pro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Data.Entity;

namespace Haveak_Pro.Areas.PANEL.Models
{
    public static class SinavMaddeOranlama
    {      
        public static decimal MaddeOranGetir(int resimID)
        {
            HaveakTurEntities db = new HaveakTurEntities();
            decimal oran = 0;
            IQueryable<SinavCevaplar> girilencevaplar = db.SinavCevaplars.Where(r => r.Sinav_Sorular.UniteSorular.STM_Ref == resimID);
            decimal toplamCevapAdet =  girilencevaplar.Count();
            decimal dogruCevaplar =  girilencevaplar.Where(w => w.Netice == true).Count();
            try
            {
                oran = (dogruCevaplar / toplamCevapAdet) * 100;
            }
            catch
            {
                oran = 0;
            }
            return Math.Round(oran, 2);
        }       
    }
}