﻿using Haveak_Pro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;

namespace Haveak_Pro.Areas.PANEL.Models
{
    public class turkish
    {

        #region Türkçe
        HaveakTurEntities db;
        public turkish(HaveakTurEntities _db)
        {
            db = _db;
        }


        public void PersonelKaydet(List<PersonelTopluView> model, int? firmaRef, int sinifRef)
        {
            foreach (var collection in model)
            {

                try
                {
                    Personel detay = db.Personels.Where(q => q.TCNo.Trim() == collection.TC_Kimlik_No.Trim()).FirstOrDefault();
                    if (detay == null)
                    {
                        detay = new Personel();
                        detay.IsActive = true;
                        detay.IsDelete = false;
                        detay.Adi = collection.Adi;
                        detay.Soyadi = collection.Soyadi;
                        detay.TCNo = collection.TC_Kimlik_No;
                        detay.CepTelefon = collection.Cep_Tel ?? "";
                        detay.Email = collection.EPosta ?? "";
                        detay.Adresi = "-";
                        detay.Cinsiyet = "-";
                        detay.DogumTarihi = DateTime.Now;
                        if (firmaRef != null)
                        {
                            detay.Firma_Ref = firmaRef;
                        }
                        else
                        {
                            detay.Firma_Ref = 2;
                        }
                        detay.IsTelefon = "-";
                        detay.EvTelefon = "-";
                        //if (collection.resim != null)
                        //{
                        //detay.ServiceImage = collection.resim;
                        //}
                        db.Personels.Add(detay);
                        db.SaveChanges();

                        Kullanici kontrol = new Kullanici();
                        kontrol.IsActive = true;
                        kontrol.IsDelete = false;
                        kontrol.KullaniciAdi = collection.TC_Kimlik_No;
                        kontrol.Sifre = collection.Adi.Substring(0, 1) + collection.Soyadi.Substring(0, 1) + collection.TC_Kimlik_No.Substring(0, 4);

                        kontrol.BaslangicTarihi = DateTime.Now;
                        kontrol.BitisTarihi = DateTime.Now.AddYears(1);
                        kontrol.Personel_Ref = detay.ID;
                        kontrol.Sistemde = false;
                        kontrol.Songiristarihi = DateTime.Now.Date;
                        db.Kullanicis.Add(kontrol);
                        db.SaveChanges();

                        for (int i = 2; i < 5; i++)
                        {
                            Kull_Rolleri rolKontrol = new Kull_Rolleri()
                            {
                                IsActive = true,
                                IsDelete = false,
                                User_Role_Name_Ref = i,
                                User_Ref = kontrol.ID
                            };
                            db.Kull_Rolleri.Add(rolKontrol);
                        }
                        db.SaveChanges();

                        Sinif_Ogrenci ogr = new Sinif_Ogrenci()
                        {
                            Personel_Ref = detay.ID,
                            Sinif_Ref = sinifRef,
                            Basladi = true,
                            Bitti = false,
                            BaslamaTarihi = DateTime.Now,
                            BitisTarihi = DateTime.Now.AddYears(1)
                        };
                        db.Sinif_Ogrenci.Add(ogr);
                        db.SaveChanges();

                        AtananBolumler blm = new AtananBolumler();
                        blm.Sinif_Ogrenci_Ref = ogr.ID;
                        if (firmaRef != null)
                        {
                            var dic = new Models.Dictionary().FirmaKurslar().Where(r => r.Key == firmaRef).FirstOrDefault();
                            string[] personelBirimler = collection.Calistigi_Birimler.Split(',');
                            var bolumIds = dic.Value.bolumlerId.Where(y => personelBirimler.Contains(y.harf)).Select(r => r.bolumId).ToList();

                            foreach (var itemBlm in bolumIds)
                            {
                                blm.Bolum_Ref = itemBlm;  //Kurs bilgi                               
                                db.AtananBolumlers.Add(blm);
                            }
                        }
                        else
                        {
                            blm.Bolum_Ref = 6;  //Kontrol Uygulamaları Kurs3
                            db.AtananBolumlers.Add(blm);
                        }
                        db.SaveChanges();
                    }
                    else
                    {
                        detay.CepTelefon = collection.Cep_Tel ?? "";
                        detay.Email = collection.EPosta ?? "";
                        //if (collection.resim != null)
                        //{
                        //detay.ServiceImage = collection.resim;
                        //}
                        db.SaveChanges();
                        var kullanici = detay.Kullanicis.FirstOrDefault();
                        kullanici.BitisTarihi = DateTime.Now.AddYears(1);
                        db.SaveChanges();

                        List<int> rolKontrol = db.Kull_Rolleri.Where(t => t.User_Ref == kullanici.ID).Select(t => t.User_Role_Name_Ref).ToList();
                        for (int i = 2; i < 5; i++)
                        {
                            if (!rolKontrol.Contains(i))
                            {
                                Kull_Rolleri rolx = new Kull_Rolleri();
                                rolx.IsActive = true;
                                rolx.IsDelete = false;
                                rolx.User_Role_Name_Ref = i;
                                rolx.User_Ref = kullanici.ID;
                                db.Kull_Rolleri.Add(rolx);
                                db.SaveChanges();
                            }
                        }
                        int sinif_ogr_id = db.Sinif_Ogrenci.Where(t => t.Personel_Ref == detay.ID && t.Basladi == true && t.Bitti == false).Select(s => s.ID).FirstOrDefault();

                        var dic = new Models.Dictionary().FirmaKurslar().Where(r => r.Key == firmaRef).FirstOrDefault();
                        string[] personelBirimler = collection.Calistigi_Birimler.Split(',');
                        var bolumIds = dic.Value.bolumlerId.Where(y => personelBirimler.Contains(y.harf)).Select(r => r.bolumId).ToList();
                        List<int> atananBolumKontrol = db.AtananBolumlers.Where(r => r.Sinif_Ogrenci_Ref == sinif_ogr_id).Select(t => t.Bolum_Ref).ToList();
                        var atanmayaBolumler = bolumIds.Except(atananBolumKontrol);

                        if (atanmayaBolumler.Count() != 0)
                        {
                            AtananBolumler blm = new AtananBolumler();
                            blm.Sinif_Ogrenci_Ref = sinif_ogr_id;
                            foreach (var itemBlm in atanmayaBolumler)
                            {
                                blm.Bolum_Ref = itemBlm;  //Kurs bilgi                               
                                db.AtananBolumlers.Add(blm);
                            }
                            db.SaveChanges();
                        }
                    }

                }
                catch
                {
                    //durum = "İşlem sırasında hata oluştu ! kontrol edip bilgileri tekrar deneyiniz.";
                }
            }

            //return durum;
        }

        public void PersonelTopluKaydet(List<PersonelTopluView> model, int? firmaRef, string sinifadi)
        {
            foreach (var collection in model)
            {
                try
                {
                    int sinifRef = 0;
                    var sinifKontrol = db.Sinifs.OrderByDescending(r => r.ID).Where(r => r.SinifAdi.Contains(sinifadi)).FirstOrDefault();
                    if (sinifKontrol == null)
                    {
                        Sinif snf = new Sinif()
                        {
                            SinifAdi = sinifadi + "-1",
                            Firma_Ref = firmaRef,
                            IsActive = true,
                            IsDelete = false
                        };
                        db.Sinifs.Add(snf);
                        db.SaveChanges();
                        sinifRef = snf.ID;
                    }
                    else
                    {
                        int ogrencisayikontrol = db.Sinif_Ogrenci.Where(r => r.Sinif_Ref == sinifKontrol.ID).Count();
                        if (ogrencisayikontrol <= 250)
                        {
                            sinifRef = sinifKontrol.ID;
                        }
                        else
                        {
                            int sonsinifno = int.Parse(sinifKontrol.SinifAdi.Split('-')[1]);
                            sonsinifno++;
                            Sinif snf = new Sinif()
                            {
                                SinifAdi = sinifadi + "-" + sonsinifno,
                                Firma_Ref = firmaRef,
                                IsActive = true,
                                IsDelete = false
                            };
                            db.Sinifs.Add(snf);
                            db.SaveChanges();
                            sinifRef = snf.ID;
                        }
                    }

                    Personel detay = db.Personels.Where(q => q.TCNo.Trim() == collection.TC_Kimlik_No.Trim()).FirstOrDefault();
                    if (detay == null)
                    {
                        detay = new Personel();
                        detay.IsActive = true;
                        detay.IsDelete = false;
                        detay.Adi = collection.Adi;
                        detay.Soyadi = collection.Soyadi;
                        detay.TCNo = collection.TC_Kimlik_No;
                        detay.CepTelefon = collection.Cep_Tel ?? "";
                        detay.Email = collection.EPosta ?? "";
                        detay.Adresi = "-";
                        if (!string.IsNullOrEmpty(collection.Cinsiyet))
                        {
                            detay.Cinsiyet = collection.Cinsiyet;
                        }
                        else
                        {
                            detay.Cinsiyet = "-";
                        }
                        if (!string.IsNullOrEmpty(collection.DogumTarihi))
                        {
                            detay.DogumTarihi = Convert.ToDateTime(collection.DogumTarihi);
                        }
                        else
                        {
                            detay.DogumTarihi = DateTime.Now;
                        }
                        if (firmaRef != null)
                        {
                            detay.Firma_Ref = firmaRef;
                        }
                        else
                        {
                            detay.Firma_Ref = 2;
                        }
                        detay.IsTelefon = "-";
                        detay.EvTelefon = "-";
                        if (!string.IsNullOrEmpty(collection.Resim))
                        {
                            detay.Resim = collection.Resim;
                        }
                        db.Personels.Add(detay);
                        db.SaveChanges();

                        Kullanici kontrol = new Kullanici();
                        kontrol.IsActive = true;
                        kontrol.IsDelete = false;
                        kontrol.KullaniciAdi = collection.TC_Kimlik_No;
                        kontrol.Sifre = collection.Adi.Substring(0, 1) + collection.Soyadi.Substring(0, 1) + collection.TC_Kimlik_No.Substring(0, 4);

                        kontrol.BaslangicTarihi = DateTime.Now;
                        kontrol.BitisTarihi = DateTime.Now.AddYears(1);
                        kontrol.Personel_Ref = detay.ID;
                        kontrol.Sistemde = false;
                        kontrol.Songiristarihi = DateTime.Now.Date;
                        db.Kullanicis.Add(kontrol);
                        db.SaveChanges();

                        for (int i = 2; i < 5; i++)
                        {
                            Kull_Rolleri rolKontrol = new Kull_Rolleri()
                            {
                                IsActive = true,
                                IsDelete = false,
                                User_Role_Name_Ref = i,
                                User_Ref = kontrol.ID
                            };
                            db.Kull_Rolleri.Add(rolKontrol);
                        }
                        db.SaveChanges();
                        
                        Sinif_Ogrenci ogr = new Sinif_Ogrenci()
                        {
                            Personel_Ref = detay.ID,
                            Sinif_Ref = sinifRef,
                            Basladi = true,
                            Bitti = false,
                            BaslamaTarihi = DateTime.Now,
                            BitisTarihi = DateTime.Now.AddYears(1)
                        };
                        db.Sinif_Ogrenci.Add(ogr);
                        db.SaveChanges();

                        AtananBolumler blm = new AtananBolumler();
                        blm.Sinif_Ogrenci_Ref = ogr.ID;
                        if (firmaRef != null)
                        {
                            var dic = new Models.Dictionary().FirmaKurslar().Where(r => r.Key == firmaRef).FirstOrDefault();
                            string[] personelBirimler = collection.Calistigi_Birimler.Split(',');
                            var bolumIds = dic.Value.bolumlerId.Where(y => personelBirimler.Contains(y.harf)).Select(r => r.bolumId).ToList();

                            foreach (var itemBlm in bolumIds)
                            {
                                blm.Bolum_Ref = itemBlm;  //Kurs bilgi                               
                                db.AtananBolumlers.Add(blm);
                            }
                        }
                        else
                        {
                            blm.Bolum_Ref = 6;  //Kontrol Uygulamaları Kurs3
                            db.AtananBolumlers.Add(blm);
                        }
                        db.SaveChanges();
                    }
                    else
                    {
                        detay.CepTelefon = collection.Cep_Tel ?? "";
                        detay.Email = collection.EPosta ?? "";
                        //if (collection.resim != null)
                        //{
                        //detay.ServiceImage = collection.resim;
                        //}
                        db.SaveChanges();
                        var kullanici = detay.Kullanicis.FirstOrDefault();
                        kullanici.BitisTarihi = DateTime.Now.AddYears(1);
                        db.SaveChanges();

                        List<int> rolKontrol = db.Kull_Rolleri.Where(t => t.User_Ref == kullanici.ID).Select(t => t.User_Role_Name_Ref).ToList();
                        for (int i = 2; i < 5; i++)
                        {
                            if (!rolKontrol.Contains(i))
                            {
                                Kull_Rolleri rolx = new Kull_Rolleri();
                                rolx.IsActive = true;
                                rolx.IsDelete = false;
                                rolx.User_Role_Name_Ref = i;
                                rolx.User_Ref = kullanici.ID;
                                db.Kull_Rolleri.Add(rolx);
                                db.SaveChanges();
                            }
                        }

                        var dic = new Models.Dictionary().FirmaKurslar().Where(r => r.Key == firmaRef).FirstOrDefault();
                        string[] personelBirimler = collection.Calistigi_Birimler.Split(',');
                        var bolumIds = dic.Value.bolumlerId.Where(y => personelBirimler.Contains(y.harf)).Select(r => r.bolumId).ToList();
                        //List<int> atananBolumKontrol = db.AtananBolumlers.Where(r => r.Sinif_Ogrenci.Personel_Ref == detay.ID).Select(t => t.Bolum_Ref).ToList();
                        //var atanmayaBolumler = bolumIds.Except(atananBolumKontrol);

                        //if (atanmayaBolumler.Count() != 0)
                        //{
                        Sinif_Ogrenci ogr = new Sinif_Ogrenci()
                        {
                            Personel_Ref = detay.ID,
                            Sinif_Ref = sinifRef,
                            Basladi = true,
                            Bitti = false,
                            BaslamaTarihi = DateTime.Now,
                            BitisTarihi = DateTime.Now.AddYears(1)
                        };
                        db.Sinif_Ogrenci.Add(ogr);
                        db.SaveChanges();
                        AtananBolumler blm = new AtananBolumler();
                        blm.Sinif_Ogrenci_Ref = ogr.ID;
                        foreach (var itemBlm in bolumIds)
                        {
                            blm.Bolum_Ref = itemBlm;  //Kurs bilgi                               
                            db.AtananBolumlers.Add(blm);
                        }
                        db.SaveChanges();
                        //}
                    }

                }
                catch
                {
                    //durum = "İşlem sırasında hata oluştu ! kontrol edip bilgileri tekrar deneyiniz.";
                }
            }

            //return durum;
        }

        //bool SinavKontrol(int personelRef)
        //{
        //    bool durum = true;
        //    using (HaveakTurEntities db = new HaveakTurEntities())
        //    {
        //        DateTime bugun = DateTime.Now.Date;
        //        int kontrol = db.Personel_Sinavlar.Where(d => d.SinavTarihi == null && d.Sinif_Ogrenci.Personel_Ref == personelRef).Count();
        //        if (kontrol > 0)
        //        {
        //            durum = false;
        //        }
        //    }
        //    return durum;
        //}

        //int SivanNoAta(int personelRef)
        //{
        //    int? sinavNo = 0;
        //    using (HaveakTurEntities db = new HaveakTurEntities())
        //    {
        //        int[] sinavlar = db.Personel_Sinavlar.Where(w => w.Sinif_Ogrenci.Personel_Ref == personelRef).Select(r => r.Sinavlar_Ref).ToArray();
        //        sinavNo = (from c in db.Sinavlars where !sinavlar.Contains(c.ID) select c.ID).OrderBy(q => Guid.NewGuid()).FirstOrDefault();
        //        if (sinavNo == 0 || sinavNo == null)
        //        {
        //            sinavNo = (from c in db.Sinavlars where sinavlar.Contains(c.ID) select c.ID).OrderBy(q => Guid.NewGuid()).FirstOrDefault();
        //        }
        //    }
        //    return sinavNo.Value;
        //}
        #endregion

    }
}