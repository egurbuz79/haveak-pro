﻿using System;
using System.Net;
using System.Web;

namespace Haveak_Pro.Areas.PANEL.Models
{
    public class PersonelData
    {
        public int? id { get; set; }
        public string aktifid { get; set; }
        public string adi { get; set; }
        public string soyadi { get; set; }
        public string tcno { get; set; }
        public string eposta { get; set; }
        public string ceptel { get; set; }
        public string resim { get; set; }
        //public string Dil { get; set; }
        public string sifre { get; set; }
        public string firmaadi { get; set; }
    } 
    
}