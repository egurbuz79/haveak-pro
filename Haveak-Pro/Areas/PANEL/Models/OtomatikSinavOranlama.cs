﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Haveak_Pro.Areas.PANEL.Models
{
    public class OtomatikSinavOranlama : IDisposable
    {

        private MaddeOranlarView getSinavSoruMaddeOranHesaplama(int soruUygAdet, int kategoriId, int uniteId)
        {
            decimal adet = 0;
            decimal yuzde = 0;
            string madde = string.Empty;
            if (uniteId == 25) // Terminal Unitesi
            {
                switch (kategoriId)
                {
                    case 1: //Silah
                        yuzde = 10;
                        madde = "Silah";
                        break;
                    case 2: //Kesici-Delici
                        yuzde = 15;
                        madde = "Kesici-Delici";
                        break;
                    case 3:
                        yuzde = 65;
                        madde = "Düzenek";
                        break;
                    case 0: //Temiz
                        yuzde = 5;
                        madde = "Temiz";
                        break;
                    case 7: //Diğerleri
                        yuzde = 5;
                        madde = "Diğerleri";
                        break;
                    default:
                        break;
                }
            }
            else
            {
                switch (kategoriId)
                {
                    case 3:
                        yuzde = 60;
                        madde = "Düzenek";
                        break;
                    case 0: //Temiz
                        yuzde = 5;
                        madde = "Temiz";
                        break;
                    case 6: //Düzenek EDS
                        yuzde = 20;
                        madde = "Düzenek-EDS";
                        break;
                    case 7: //Diğerleri
                        yuzde = 15;
                        madde = "Diğerleri";
                        break;
                    default:
                        break;
                }
            }


            adet = Math.Round((soruUygAdet * yuzde) / 100);
            MaddeOranlarView orn = new MaddeOranlarView()
            {
                adet = adet,
                yuzde = yuzde,
                madde = madde
            };
            return orn;
        }

        public List<MaddeOranlarView> OtomatikUygSinavOlustur(int soruUygAdet, List<int> mkategoriIds, int uniteId)
        {
            List<MaddeOranlarView> m_oran = new List<MaddeOranlarView>();
            foreach (var item in mkategoriIds)
            {
                MaddeOranlarView orn = getSinavSoruMaddeOranHesaplama(soruUygAdet, item, uniteId);
                orn.madde_Id = item;
                if (orn.yuzde != 0)
                {
                    m_oran.Add(orn);
                }
            }
            decimal toplamsoru = m_oran.Sum(r => r.adet);
            decimal duzenek = 0;
            if (soruUygAdet != toplamsoru)
            {
                decimal fark = soruUygAdet - toplamsoru;
                if (fark < 0)
                {
                    duzenek = m_oran.Where(t => t.madde == "Düzenek").Select(u => u.adet).FirstOrDefault();
                    duzenek = duzenek - fark;
                }
                else
                {
                    duzenek = m_oran.Where(t => t.madde == "Düzenek").Select(u => u.adet).FirstOrDefault();
                    duzenek = duzenek + fark;
                }
                m_oran.Where(t => t.madde == "Düzenek").First().adet = duzenek;
            }
            return m_oran;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }

    public class MaddeOranlarView
    {
        public decimal adet { get; set; }
        public decimal yuzde { get; set; }
        public string madde { get; set; }
        public int madde_Id { get; set; }

    }
}