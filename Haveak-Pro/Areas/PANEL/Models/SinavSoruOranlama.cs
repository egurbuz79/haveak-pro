﻿using Haveak_Pro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Haveak_Pro.Areas.PANEL.Models
{
    public class SinavSoruOranlama : IDisposable
    {
        HaveakTurEntities db = new HaveakTurEntities();

        public decimal SoruOranGetir(int soruId)
        {
            decimal oran = 0;

            IQueryable<SinavCevaplar> girilencevaplar = db.SinavCevaplars.Where(r => r.Sinav_Sorular.UniteSorular_Ref == soruId);
            decimal toplamCevapAdet = girilencevaplar.Count();
            decimal dogruCevaplar = girilencevaplar.Where(w => w.Netice == true).Count();
            try
            {
                oran = (dogruCevaplar / (toplamCevapAdet * 100)) * 100;
            }
            catch
            {
                oran = 0;
            }
            return Math.Round(oran, 2);
        }

        public int SoruKullanimAdet(int soruId)
        {
            int adet = 0;
            adet = db.Sinav_Sorular.Where(t => t.UniteSorular_Ref == soruId && t.IsActive == true && t.IsDelete == false).Select(t => t.ID).Count();
            return adet;
        }
        public void Dispose()
        {
            db.Dispose();
            GC.SuppressFinalize(this);
        }
    }

}