﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Haveak_Pro.Areas.PANEL.Models
{
    public class FirmaSinif
    {
        public int ID { get; set; }
        public string Sinif { get; set; }
    }
}