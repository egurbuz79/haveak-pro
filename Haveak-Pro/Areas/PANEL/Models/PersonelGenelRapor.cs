﻿using Haveak_Pro.Models;
using System;
using System.Collections.Generic;
using System.Linq;



namespace Haveak_Pro.Areas.PANEL.Models
{
    public class PersonelGenelRapor
    {
        public int perID { get; set; }
        public string TcNo { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Sinif { get; set; }
        public int SinifID { get; set; }
        public int? SinifAdet { get; set; }
        public int? GisSinavAdet { get; set; }
        public int? H_SinavAdet { get; set; }
        public int? S_SinavAdet { get; set; }
        public decimal? GisOrtalama { get; set; }
        public decimal? H_SinavUygOrtalama { get; set; }
        public decimal? S_SinavUygOrtalama { get; set; }
        public decimal? H_SinavTeoOrtalama { get; set; }
        public decimal? S_SinavTeoOrtalama { get; set; }
        public decimal? deliciYuzde { get; set; }
        public decimal? silahYuzde { get; set; }
        public decimal? duzenekYuzde { get; set; }
        public decimal? kargoYuzde { get; set; }
        public decimal? digerleriYuzde { get; set; }

        public string BaslamaTarihi { get; set; }
        public string BitisTarihi { get; set; }
        public List<sp_EgitimBolumKonuDagilimlari_Result> Konular { get; set; }
        public List<sp_EgitimCalimaKontrol_Result> KonularCalismaDurum { get; set; }
    }

    public class GenelRaporlama
    {
        public List<PersonelGenelRapor> GenelDataCikti(int? firmaRef, int ktipi)
        {
            try
            {
                List<PersonelGenelRapor> datas = null;
                using (HaveakTurEntities db = new HaveakTurEntities())
                {
                    //if (!UserRoleName.PANEL_AdminGiris.InRole())
                    //{
                    datas = new List<PersonelGenelRapor>();
                    var data = db.sp_GenelPersonelDurumDegerlendirmeFirma(firmaRef, "", "", null, null);
                    foreach (var item in data)
                    {
                        if (ktipi == 2)
                        {
                            if (item.GisAdet == 0 && item.PersonelSinavHazirlik == 0 && item.PersonelSinavSertifika == 0)
                            {
                                PersonelGenelRapor per = new PersonelGenelRapor();
                                per.perID = item.ID;
                                per.Ad = item.Adi.ToUpper();
                                per.Soyad = item.Soyadi.ToUpper();
                                per.TcNo = item.TCNo;
                                per.SinifAdet = item.SinifAdet ?? 0;
                                per.GisSinavAdet = item.GisAdet ?? 0;
                                per.H_SinavAdet = item.PersonelSinavHazirlik ?? 0;
                                per.S_SinavAdet = item.PersonelSinavSertifika ?? 0;
                                per.GisOrtalama = item.GisNotOrtalama ?? 0;
                                per.H_SinavTeoOrtalama = item.HazirlikTeoOrtalama ?? 0;
                                per.H_SinavUygOrtalama = item.HazirlikUygOrtalama ?? 0;
                                per.S_SinavTeoOrtalama = item.SertifikaTeoOrtalama ?? 0;
                                per.S_SinavUygOrtalama = item.SertifikaUygOrtalama ?? 0;
                                per.deliciYuzde = item.Kesici_Delici ?? 0;
                                per.silahYuzde = item.Silah ?? 0;
                                per.duzenekYuzde = item.Duzenek ?? 0;
                                // per.kargoYuzde = item.Kargo_EDS;
                                per.digerleriYuzde = item.Digerleri ?? 0;
                                datas.Add(per);
                            }
                        }
                        if (ktipi == 1)
                        {
                            if (item.GisAdet == 0 && item.PersonelSinavHazirlik == 0 && item.PersonelSinavSertifika == 0)
                            {
                            }
                            else
                            {
                                PersonelGenelRapor per = new PersonelGenelRapor();
                                per.perID = item.ID;
                                per.Ad = item.Adi.ToUpper();
                                per.Soyad = item.Soyadi.ToUpper();
                                per.TcNo = item.TCNo;
                                per.SinifAdet = item.SinifAdet ?? 0;
                                per.GisSinavAdet = item.GisAdet ?? 0;
                                per.H_SinavAdet = item.PersonelSinavHazirlik ?? 0;
                                per.S_SinavAdet = item.PersonelSinavSertifika ?? 0;
                                per.GisOrtalama = item.GisNotOrtalama ?? 0;
                                per.H_SinavTeoOrtalama = item.HazirlikTeoOrtalama ?? 0;
                                per.H_SinavUygOrtalama = item.HazirlikUygOrtalama ?? 0;
                                per.S_SinavTeoOrtalama = item.SertifikaTeoOrtalama ?? 0;
                                per.S_SinavUygOrtalama = item.SertifikaUygOrtalama ?? 0;
                                per.deliciYuzde = item.Kesici_Delici ?? 0;
                                per.silahYuzde = item.Silah ?? 0;
                                per.duzenekYuzde = item.Duzenek ?? 0;
                                // per.kargoYuzde = item.Kargo_EDS;
                                per.digerleriYuzde = item.Digerleri ?? 0;
                                datas.Add(per);
                            }
                        }
                    }
                    //}
                    //else
                    //{
                    //    datas = new List<PersonelGenelRapor>();
                    //    var data = db.sp_GenelPersonelDurumDegerlendirme();
                    //    foreach (var item in data)
                    //    {
                    //        PersonelGenelRapor per = new PersonelGenelRapor();
                    //        per.perID = item.ID;
                    //        per.AdSoyad = item.Adi + " " + item.Soyadi;
                    //        per.TcNo = item.TCNo;
                    //        per.SinifAdet = item.SinifAdet ?? 0;
                    //        per.GisSinavAdet = item.GisAdet ?? 0;
                    //        per.H_SinavAdet = item.PersonelSinavHazirlik ?? 0;
                    //        per.S_SinavAdet = item.PersonelSinavSertifika ?? 0;
                    //        per.GisOrtalama = item.GisNotOrtalama ?? 0;
                    //        per.H_SinavTeoOrtalama = item.HazirlikTeoOrtalama ?? 0;
                    //        per.H_SinavUygOrtalama = item.HazirlikUygOrtalama ?? 0;
                    //        per.S_SinavTeoOrtalama = item.SertifikaTeoOrtalama ?? 0;
                    //        per.S_SinavUygOrtalama = item.SertifikaUygOrtalama ?? 0;
                    //        per.deliciYuzde = item.Kesici_Delici ?? 0;
                    //        per.silahYuzde = item.Silah ?? 0;
                    //        per.duzenekYuzde = item.Duzenek ?? 0;
                    //        // per.kargoYuzde = item.Kargo_EDS;
                    //        per.digerleriYuzde = item.Digerleri ?? 0;
                    //        datas.Add(per);
                    //    }
                    //}   
                }

                return datas;
            }
            catch
            {
                return new List<PersonelGenelRapor>();
            }

        }

        public List<PersonelGenelRapor> GenelDataCiktiDetay(int? firmaRef, string ad, string soyad, string kurumadi, DateTime? baslangict, DateTime? bitist)
        {
            try
            {
                List<PersonelGenelRapor> datas = null;
                using (HaveakTurEntities db = new HaveakTurEntities())
                {
                    //if (!UserRoleName.PANEL_AdminGiris.InRole())
                    //{
                    datas = new List<PersonelGenelRapor>();
                    var data = db.sp_GenelPersonelDurumDegerlendirmeFirmaListe(firmaRef, ad, soyad, baslangict, bitist);
                    foreach (var item in data)
                    {
                        if (item.GisAdet != 0)
                        {
                            PersonelGenelRapor per = new PersonelGenelRapor();
                            per.perID = item.ID;
                            per.Ad = item.Adi.ToUpper();
                            per.Soyad = item.Soyadi.ToUpper();
                            per.TcNo = item.TCNo;
                            per.SinifAdet = item.SinifAdet ?? 0;
                            per.GisSinavAdet = item.GisAdet ?? 0;
                            per.H_SinavAdet = item.PersonelSinavHazirlik ?? 0;
                            per.S_SinavAdet = item.PersonelSinavSertifika ?? 0;
                            per.GisOrtalama = item.GisNotOrtalama ?? 0;
                            per.H_SinavTeoOrtalama = item.HazirlikTeoOrtalama ?? 0;
                            per.H_SinavUygOrtalama = item.HazirlikUygOrtalama ?? 0;
                            per.S_SinavTeoOrtalama = item.SertifikaTeoOrtalama ?? 0;
                            per.S_SinavUygOrtalama = item.SertifikaUygOrtalama ?? 0;
                            per.deliciYuzde = item.Kesici_Delici ?? 0;
                            per.silahYuzde = item.Silah ?? 0;
                            per.duzenekYuzde = item.Duzenek ?? 0;
                            // per.kargoYuzde = item.Kargo_EDS;
                            per.digerleriYuzde = item.Digerleri ?? 0;
                            datas.Add(per);
                        }
                    }

                }

                return datas;
            }
            catch
            {
                return new List<PersonelGenelRapor>();
            }

        }

        public List<PersonelGenelRapor> GenelSinifDataCiktiDetay(int? firmaRef, string ad, string soyad, string kurumadi, DateTime? baslangict, DateTime? bitist)
        {
            try
            {
                List<PersonelGenelRapor> datas = null;
                using (HaveakTurEntities db = new HaveakTurEntities())
                {
                    datas = new List<PersonelGenelRapor>();
                    var data = db.sp_GenelSinifDegerlendirme(firmaRef, ad, soyad, baslangict, bitist);
                    foreach (var item in data)
                    {
                        PersonelGenelRapor per = new PersonelGenelRapor();
                        per.Ad = item.Adi.ToUpper();
                        per.Soyad = item.Soyadi.ToUpper();
                        per.TcNo = item.TCNo;
                        per.Sinif = item.SinifAdi;
                        per.BaslamaTarihi = item.BaslamaTarihi.ToShortDateString();
                        per.BitisTarihi = item.BitisTarihi.ToShortDateString();
                        per.GisSinavAdet = item.GisAdet ?? 0;
                        per.H_SinavAdet = item.HazirlikSinavSayi ?? 0;
                        per.S_SinavAdet = item.SetifikaSinavSayi ?? 0;
                        per.GisOrtalama = item.GisNotOrtalama ?? 0;
                        per.H_SinavTeoOrtalama = item.HazirlikTeoOrtalama ?? 0;
                        per.H_SinavUygOrtalama = item.HazirlikUygOrtalama ?? 0;
                        per.S_SinavTeoOrtalama = item.SertifikaTeoOrtalama ?? 0;
                        per.S_SinavUygOrtalama = item.SertifikaUygOrtalama ?? 0;
                        per.deliciYuzde = item.Kesici_Delici ?? 0;
                        per.silahYuzde = item.Silah ?? 0;
                        per.duzenekYuzde = item.Duzenek ?? 0;
                        per.kargoYuzde = item.Kargo_EDS;
                        per.digerleriYuzde = item.Digerleri ?? 0;
                        datas.Add(per);
                    }
                }

                return datas;
            }
            catch
            {
                return new List<PersonelGenelRapor>();
            }

        }

        public List<DuzenekOranlarView> SinavMaddeOranlar(int prs_Id)
        {
            using (HaveakTurEntities db = new HaveakTurEntities())
            {
                List<DuzenekOranlarView> oranlar = new List<DuzenekOranlarView>();

                #region Uygulama Sınav ORanlar

                Nullable<int> genelSinavSilah = db.PersonelToplamSinavSoruMaddeDagilim(prs_Id).Where(w => w.Isim == "Silah").Select(q => q.ToplamSoru).FirstOrDefault() ?? 0;
                Nullable<int> genelSinavSilahDogru = db.PersonelToplamSinavSoruMaddeDagilimDogruAdet(prs_Id).Where(r => r.Isim == "Silah").Select(q => q.DogruCevap).FirstOrDefault() ?? 0;
                double oran = 0;
                try
                {
                    oran = ((Convert.ToDouble(genelSinavSilahDogru) / Convert.ToDouble(genelSinavSilah)) * 100);
                    if (double.IsNaN(oran))
                    {
                        oran = 0;
                    }
                    oran = Math.Round(oran, 2);
                }
                catch
                {
                    oran = 0;
                }
                oranlar.Add(new DuzenekOranlarView { madde = "Silah", oran = oran, sorusayisi = genelSinavSilah.Value, dogrusayisi = genelSinavSilahDogru.Value, sinavturu = "UYGULAMA Sınavlar" });

                Nullable<int> genelSinavDuzenek = db.PersonelToplamSinavSoruMaddeDagilim(prs_Id).Where(w => w.Isim == "Düzenek").Select(q => q.ToplamSoru).FirstOrDefault() ?? 0;
                Nullable<int> genelSinavDuzenekDogru = db.PersonelToplamSinavSoruMaddeDagilimDogruAdet(prs_Id).Where(r => r.Isim == "Düzenek").Select(q => q.DogruCevap).FirstOrDefault() ?? 0;
                double oranDuz = 0;
                try
                {
                    oranDuz = ((Convert.ToDouble(genelSinavDuzenekDogru) / Convert.ToDouble(genelSinavDuzenek)) * 100);
                    if (double.IsNaN(oranDuz))
                    {
                        oranDuz = 0;
                    }
                    oranDuz = Math.Round(oranDuz, 2);
                }
                catch
                {
                    oranDuz = 0;
                }
                oranlar.Add(new DuzenekOranlarView { madde = "Düzenek", oran = oranDuz, sorusayisi = genelSinavDuzenek.Value, dogrusayisi = genelSinavDuzenekDogru.Value, sinavturu = " " });

                Nullable<int> genelSinavKesici = db.PersonelToplamSinavSoruMaddeDagilim(prs_Id).Where(w => w.Isim == "Kesici-Delici").Select(q => q.ToplamSoru).FirstOrDefault() ?? 0;
                Nullable<int> genelSinavKesiciDogru = db.PersonelToplamSinavSoruMaddeDagilimDogruAdet(prs_Id).Where(r => r.Isim == "Kesici-Delici").Select(q => q.DogruCevap).FirstOrDefault() ?? 0;
                double oranKes = 0;
                try
                {
                    oranKes = ((Convert.ToDouble(genelSinavKesiciDogru) / Convert.ToDouble(genelSinavKesici)) * 100);
                    if (double.IsNaN(oranKes))
                    {
                        oranKes = 0;
                    }
                    oranKes = Math.Round(oranKes, 2);
                }
                catch
                {
                    oranKes = 0;
                }
                oranlar.Add(new DuzenekOranlarView { madde = "Kesici-Delici", oran = oranKes, sorusayisi = genelSinavKesici.Value, dogrusayisi = genelSinavKesiciDogru.Value, sinavturu = " " });

                Nullable<int> genelSinavKargo = db.PersonelToplamSinavSoruMaddeDagilim(prs_Id).Where(w => w.Isim == "Düzenek (Kargo-EDS)").Select(q => q.ToplamSoru).FirstOrDefault() ?? 0;
                Nullable<int> genelSinavKargoDogru = db.PersonelToplamSinavSoruMaddeDagilimDogruAdet(prs_Id).Where(r => r.Isim == "Düzenek (Kargo-EDS)").Select(q => q.DogruCevap).FirstOrDefault() ?? 0;
                double oranKar = 0;
                try
                {
                    oranKar = ((Convert.ToDouble(genelSinavKargoDogru) / Convert.ToDouble(genelSinavKargo)) * 100);
                    if (double.IsNaN(oranKar))
                    {
                        oranKar = 0;
                    }
                    oranKar = Math.Round(oranKar, 2);
                }
                catch
                {
                    oranKar = 0;
                }
                oranlar.Add(new DuzenekOranlarView { madde = "Düzenek (Kargo-EDS)", oran = oranKar, sorusayisi = genelSinavKargo.Value, dogrusayisi = genelSinavKargoDogru.Value, sinavturu = " " });

                Nullable<int> genelSinavDiger = db.PersonelToplamSinavSoruMaddeDagilim(prs_Id).Where(w => w.Isim == "Diğerleri").Select(q => q.ToplamSoru).FirstOrDefault() ?? 0;
                Nullable<int> genelSinavDigerDogru = db.PersonelToplamSinavSoruMaddeDagilimDogruAdet(prs_Id).Where(r => r.Isim == "Diğerleri").Select(q => q.DogruCevap).FirstOrDefault() ?? 0;
                double oranDig = 0;
                try
                {
                    oranDig = ((Convert.ToDouble(genelSinavDigerDogru) / Convert.ToDouble(genelSinavDiger)) * 100);
                    if (double.IsNaN(oranDig))
                    {
                        oranDig = 0;
                    }
                    oranDig = Math.Round(oranDig, 2);
                }
                catch
                {
                    oranDig = 0;
                }
                oranlar.Add(new DuzenekOranlarView { madde = "Diğerleri", oran = oranDig, sorusayisi = genelSinavDiger.Value, dogrusayisi = genelSinavDigerDogru.Value, sinavturu = " " });
                #endregion
                return oranlar;
            }
        }

        public List<PersonelGenelRapor> GenelSinifDataCiktiDetay(string siniflar)
        {
            try
            {
                List<PersonelGenelRapor> datas = null;
                using (HaveakTurEntities db = new HaveakTurEntities())
                {
                    datas = new List<PersonelGenelRapor>();
                    IEnumerable<sp_GenelSinifPersonelDurumDegerlendirme_Result> data = db.sp_GenelSinifPersonelDurumDegerlendirme(siniflar);
                    foreach (var item in data)
                    {
                        PersonelGenelRapor per = new PersonelGenelRapor();
                        per.perID = item.ID;
                        per.Ad = item.Adi.ToUpper();
                        per.Soyad = item.Soyadi.ToUpper();
                        per.TcNo = item.TCNo;
                        per.Sinif = item.SinifAdi;
                        per.SinifID = item.SOID;
                        per.GisSinavAdet = item.GisAdet ?? 0;
                        per.GisOrtalama = item.GisNotOrtalama ?? 0;
                        per.H_SinavTeoOrtalama = item.HazirlikTeoOrtalama ?? 0;
                        per.H_SinavUygOrtalama = item.HazirlikUygOrtalama ?? 0;
                        per.S_SinavTeoOrtalama = item.SertifikaTeoOrtalama ?? 0;
                        per.S_SinavUygOrtalama = item.SertifikaUygOrtalama ?? 0;
                        per.deliciYuzde = item.Kesici_Delici ?? 0;
                        per.silahYuzde = item.Silah ?? 0;
                        per.duzenekYuzde = item.Duzenek ?? 0;
                        per.kargoYuzde = item.Kargo_EDS;
                        per.digerleriYuzde = item.Digerleri ?? 0;
                        per.Konular = db.sp_EgitimBolumKonuDagilimlari(item.SOID).ToList();
                        per.KonularCalismaDurum = db.sp_EgitimCalimaKontrol(item.SOID).ToList();
                        datas.Add(per);
                    }
                }
                return datas;
            }
            catch
            {
                return new List<PersonelGenelRapor>();
            }

        }
    }
}