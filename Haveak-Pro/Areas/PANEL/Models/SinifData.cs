﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Haveak_Pro.Areas.PANEL.Models
{
    public class SinifData
    {
        public string sno { get; set; }
        public string tcno { get; set; }
        public string adi { get; set; }
        public string soyadi { get; set; }
        public string siniflar { get; set; }
        public string uygulama { get; set; }
        public string teorik { get; set; }
        public string durum { get; set; }
        public string sonuc { get; set; }
    }
}