﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Haveak_Pro.Areas.PANEL.Models
{
    public class DuzenekOranlarView
    {
        public string sinavturu { get; set; }
        public string madde { get; set; }
        public double oran { get; set; }
        public int sorusayisi { get; set; }
        public int dogrusayisi { get; set; }
    }
}