﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Haveak_Pro.Areas.PANEL.Models
{
    public class SinifPersonelView : IDisposable
    {
        public int personel_id { get; set; }
        public int sinif_ogrenci_id { get; set; }
        public string resim { get; set; }
        public string adi { get; set; }
        public string soyadi { get; set; }
        public string tcno { get; set; }
        public string sinif { get; set; }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}