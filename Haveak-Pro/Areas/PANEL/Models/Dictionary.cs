﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Haveak_Pro.Areas.PANEL.Models
{
    public class Dictionary
    {

        public Dictionary<int, string> DuyuruTipi()
        {
            Dictionary<int, string> list = new Dictionary<int, string>();
            list.Add(1, "Genel Duyuru");
            list.Add(2, "Sınav Duyuru");
            return list;
        }

        public Dictionary<string, string> RolAdi()
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            list.Add("PANEL/AdminGirisi", "ADMİN");
            list.Add("ADMIN/BagajTehlikelimadde", " - Bagaj Madde");
            list.Add("ADMIN/Egitim", " - Eğitim");
            list.Add("ADMIN/Sinav", " - Sınav İşlem");
            list.Add("ADMIN/PersonelKullanici", " - Personel Kullanıcı");
            list.Add("GUVENLIK/OgrenciGirisi", "GÜVENLİK");
            list.Add("EGITIM/OgrenciGirisi", "EĞİTİM");
            list.Add("SINAVLAR/OgrenciGirisi", "SINAV");
            return list;
        }

        public Dictionary<int, string> SoruKategori()
        {
            Dictionary<int, string> list = new Dictionary<int, string>();
            list.Add(0, "Soru Kategorisi Seçiniz");
            list.Add(1, "UYGULAMA");
            list.Add(2, "TEORİK");
            return list;
        }

        public Dictionary<int, string> DuyuruOncelik()
        {
            Dictionary<int, string> list = new Dictionary<int, string>();
            list.Add(1, "Normal");
            list.Add(2, "Önemli");
            list.Add(3, "Acil");
            return list;
        }

        public Dictionary<int, FirmaDetay> FirmaKurslar()
        {
            var gozenKurslar = new List<KursAlfabe> { new KursAlfabe { bolumId = 6, harf = "A" }, new KursAlfabe { bolumId = 14, harf = "C" }, new KursAlfabe { bolumId = 20, harf = "D" } };
            var gozen = new FirmaDetay { bolumlerId = gozenKurslar, firma = "GÖZEN GÜVENLİK HİZMETLERİ", kurslar = "Kurs3,Kurs11,Kurs17" };
            Dictionary<int, FirmaDetay> firmaKurslar = new Dictionary<int, FirmaDetay>();
            firmaKurslar.Add(7, gozen);
            var sistemKurslar = new List<KursAlfabe> { new KursAlfabe { bolumId = 6, harf = "A" }, new KursAlfabe { bolumId = 14, harf = "C" } };
            var sistem = new FirmaDetay { bolumlerId = sistemKurslar, firma = "SİSTEM GÜVENLİK HİZMETLERİ", kurslar = "Kurs3,Kurs11" };
            firmaKurslar.Add(4, sistem);

            var igaKurslar = new List<KursAlfabe>
            {
                new KursAlfabe { bolumId = 6, harf = "A" },
                new KursAlfabe { bolumId = 5, harf = "B" },
                new KursAlfabe { bolumId = 14, harf = "C" },
                new KursAlfabe { bolumId = 21, harf = "D" },
                new KursAlfabe { bolumId = 7, harf = "E" },
                new KursAlfabe { bolumId = 3, harf = "F" },
                new KursAlfabe { bolumId = 24, harf = "G" },
                new KursAlfabe { bolumId = 25, harf = "H" }
            };
            var iga = new FirmaDetay { bolumlerId = igaKurslar, firma = "İGA GÜVENLİK HİZMETLERİ", kurslar = "Kurs1,Kurs2,Kurs3,Kurs4,Kurs11,Kurs17" };
            firmaKurslar.Add(1, iga);
            var heasKurslar = new List<KursAlfabe>
            {
                new KursAlfabe { bolumId = 6, harf = "A" },
                new KursAlfabe { bolumId = 5, harf = "B" },
                new KursAlfabe { bolumId = 14, harf = "C" },
                new KursAlfabe { bolumId = 16, harf = "D" },
                new KursAlfabe { bolumId = 7, harf = "E" },
                new KursAlfabe { bolumId = 3, harf = "F" },
                new KursAlfabe { bolumId = 17, harf = "G" },
                new KursAlfabe { bolumId = 20, harf = "H" }
            };
            var heas = new FirmaDetay { bolumlerId = heasKurslar, firma = "HEAŞ", kurslar = "Kurs1,Kurs2,Kurs3,Kurs4,Kurs11,Kurs15,Kurs17" };
            firmaKurslar.Add(8, heas);
            return firmaKurslar;
        }

    }
    public class FirmaDetay
    {
        public string firma { get; set; }
        public string kurslar { get; set; }
        public List<KursAlfabe> bolumlerId { get; set; }
    }

    public class KursAlfabe
    {
        public int bolumId { get; set; }
        public string harf { get; set; }
    }
}