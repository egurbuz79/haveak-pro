﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Haveak_Pro.Areas.PANEL.Models
{
    public class VEgitimOkunmaListe
    {
        public int bolumId { get; set; }
        public string bolumAdi { get; set; }
        public int kursId { get; set; }
        public string kursAdi { get; set; }
        public int? toplamUniteSayi { get; set; }
        public int? okunanUniteSayi { get; set; }
        public int? kalanUniteSayi { get; set; }      

    }
}