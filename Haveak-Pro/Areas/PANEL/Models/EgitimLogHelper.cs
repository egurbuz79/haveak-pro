﻿using Haveak_Pro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Haveak_Pro.Areas.PANEL.Models 
{
    public class EgitimLogHelper : IDisposable
    {
        HaveakTurEntities entity = new HaveakTurEntities();

        public bool EgitimLog(int sinif_ogrenci_ref, DateTime tarih, TimeSpan giriszamani, TimeSpan cikiszamani, int uniteid)
        {
            bool sonuc = false;
            try
            {
                var kontrol = entity.Egitim_Log.Where(r => r.Unite_Ref == uniteid && r.Sinif_Ogrenci_Ref == sinif_ogrenci_ref).Count();
                if (kontrol == 0)
                {
                    Egitim_Log log = new Egitim_Log();
                    log.IsActive = true;
                    log.IsDelete = false;
                    log.Sinif_Ogrenci_Ref = sinif_ogrenci_ref;
                    log.Unite_Ref = uniteid;
                    log.Tarih = tarih;
                    log.GirisZamani = giriszamani;
                    log.CikisZamani = cikiszamani;
                    entity.Egitim_Log.Add(log);
                    sonuc = entity.SaveChanges() > 0 ? true : false;
                }
            }
            catch
            {
                sonuc = false;
            }
            return sonuc;
        }

        public string ToplamUniteSuresi(int uniteomodul_id, int sinif_ogrenci_ref)
        {
            long toplamsure = 0;
            var uniteIDs = entity.Unites.Where(t => t.Modul_Ref == uniteomodul_id).Select(r => r.ID).ToList();
            foreach (int item in uniteIDs)
            {
                var kontrol = entity.Egitim_Log.Where(r => r.Unite_Ref == item && r.Sinif_Ogrenci_Ref == sinif_ogrenci_ref).First();
                TimeSpan fark = kontrol.CikisZamani - kontrol.GirisZamani;
                toplamsure += fark.Ticks;
            }
            TimeSpan sonuc = TimeSpan.FromTicks(toplamsure);
            return sonuc.ToString();
        }

        public List<VEgitimOkunmaListe> EgitimOkumaDurum(int sinif_ogrenci_ref)
        {
            var kursdagilimlar = entity.sp_EgitimBolumKonuDagilimlari(sinif_ogrenci_ref);
            var okunanbolumler = entity.sp_EgitimCalimaKontrol(sinif_ogrenci_ref);
            List<VEgitimOkunmaListe> data = new List<VEgitimOkunmaListe>();
            foreach (var item in kursdagilimlar)
            {
                var okunmadetay = okunanbolumler.Where(e => e.ID == item.KursID).FirstOrDefault();
                int okunanasayi = 0;
                int kalanUniteSayi = 0;
                if (okunmadetay != null)
                {
                    okunanasayi = okunmadetay.OkunanUniteSayi.Value;
                    kalanUniteSayi = okunmadetay.KalanUnite.Value;
                }
                VEgitimOkunmaListe bilgi = new VEgitimOkunmaListe();
                bilgi.bolumId = item.ID;
                bilgi.bolumAdi = item.BolumAdi;
                bilgi.kursId = item.KursID;
                bilgi.kursAdi = item.Kurs;
                bilgi.toplamUniteSayi = item.ToplamUniteSayi ?? 0;
                bilgi.okunanUniteSayi = okunanasayi;
                bilgi.kalanUniteSayi = kalanUniteSayi;
                data.Add(bilgi);
            }
            return data;
        }

        public void Dispose()
        {
            entity.Dispose();
            GC.SuppressFinalize(this);
        }

    }
}