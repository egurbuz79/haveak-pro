﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Haveak_Pro
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {

            //routes.RouteExistingFiles = true;

            //routes.IgnoreRoute("Views/Shared/ReportViews/PRapor.html");

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute("Rapor", "Views/Shared/ReportViews/PRapor.html", new { action = "Rapor", conroller = "Personel", idz = UrlParameter.Optional });
            
            routes.MapRoute(
               "Default",
               "{controller}/{action}/{id}",
               new { controller = "Home", action = "Index", id = UrlParameter.Optional },
               new[] { "Haveak_Pro.Controllers" }            
            );         
             
        }
    }
}